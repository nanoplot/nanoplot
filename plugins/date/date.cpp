/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "date.h"
#include "options.h"
#include "gettext.h"
#include <stdio.h>

#define _(String) gettext (String)
#define N_(String) String

#define LEAP(n)	((((n) % 400 == 0) || (((n) % 4 == 0) && !((n) % 100 == 0)))? 1 : 0)

static const int days[4][12] = {
	{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
	{31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
	{31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365},
	{31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366}
};

static Variant date_fromString(const char *str)
{
	int year, mon, day, y;
	sscanf(str, "%d%*1c%2u%*1c%2u", &year, &mon, &day);
	int d = 0;
	if (year >= 1970) {
		y = 1970;
		while (y < year) {
			d += days[LEAP(y) + 2][11];
			y++;
		}
	} else {
		y = 1969;
		do {
			d -= days[LEAP(y) + 2][11];
			y--;
		} while (y >= year);
	}
	mon = (mon - 1) % 12;
	if (mon > 0)
		d += days[LEAP(year) + 2][mon - 1];
	day = (day - 1) % days[LEAP(year)][mon];
	d += day;
	return Variant(d);
}

static void date_toString(Variant v, char *buf, int buf_len)
{
	int year, mon, leap;
	int dt = v.toInt();
	if (dt >= 0) {
		year = 1970;
		leap = LEAP(year);
		while (dt >= days[leap + 2][11]) {
			dt -= days[leap + 2][11];
			year++;
			leap = LEAP(year);
		}
		mon = 0;
		while (dt >= days[leap][mon])
			dt -= days[leap][mon++];
	} else {
		year = 1969;
		leap = LEAP(year);
		while (dt < -days[leap + 2][11]) {
			dt += days[leap + 2][11];
			year--;
			leap = LEAP(year);
		}
		mon = 11;
		while (dt < -days[leap][mon])
			dt += days[leap][mon--];
		dt += days[leap][mon];
	}
	snprintf(buf, 16, "%d-%02d-%02d", year, mon + 1, dt + 1);
}

void init()
{
}

plugin_info info()
{
	plugin_info info = {"date", _("Date data type"),
	                    _("Provides data type date.")};
	return info;
}

datatype_info date()
{
	datatype_info info = {&date_fromString, &date_toString, type_int,
	                      "date", _("date")};
	return info;
}
