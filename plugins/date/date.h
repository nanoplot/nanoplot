/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DATE_H
#define DATE_H

#include <plugin.h>

#define init date_LTX_init
#define info date_LTX_info
#define symbols date_LTX_symbols
#define date date_LTX_date

BEGIN_C_DECLS

void init();

plugin_info info();

exported_symbol symbols[] = {
	{"datatype", "date"},
	{0, 0}
};

datatype_info date();

END_C_DECLS

#endif /* date_H */
