/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pdf.h"
#include "pdfexport.h"
#include "options.h"
#include "gettext.h"
#include <iostream>
#include <napp.h>
#define _(String) gettext (String)
#define N_(String) String


using namespace std;

static Export* pdf_instantiate(const char *fname, int width, int height)
{
	return new PDFExport(fname);
}

void init()
{
}

plugin_info info()
{
	plugin_info info = {"pdf", _("PDF export"), _("Provides PDF export.")};
	return info;
}

export_info pdf()
{
	export_info info = {&pdf_instantiate, false, _("PDF"),
	                    _("Export Portable Document Format"), "pdf"};
	return info;
}
