/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pngexport.h"
#include <iostream>

using namespace std;

PNGExport::PNGExport(const char *_filename, int width, int height)
 : CairoExport(), filename(_filename), w(width), h(height)
{
	surface = cairo_image_surface_create(CAIRO_FORMAT_RGB24, w, h);
	canvas  = cairo_create(surface);
	
	cairo_scale(canvas, (double) w / IMG_WIDTH, (double) h / IMG_HEIGHT);
	
	cairo_set_line_cap(canvas, CAIRO_LINE_CAP_SQUARE);
	cairo_select_font_face(canvas, "Serif", CAIRO_FONT_SLANT_NORMAL,
	                       CAIRO_FONT_WEIGHT_NORMAL);

	cairo_set_source_rgb(canvas, 1, 1 , 1);
	cairo_rectangle(canvas, 0, 0, w, h);
	cairo_fill(canvas);
	cairo_set_source_rgb(canvas, 0, 0, 0);
}

PNGExport::~PNGExport()
{
	if (canvas) {
		cairo_show_page(canvas);
		cairo_surface_write_to_png(surface, filename.c_str());
		cairo_destroy(canvas);
	}
	if (surface)
		cairo_surface_destroy(surface);
}

