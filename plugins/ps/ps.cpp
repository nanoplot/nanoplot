/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ps.h"
#include "psexport.h"
#include "epsexport.h"
#include "gettext.h"
#include <iostream>
#include <napp.h>
#include <options.h>
#define _(String) gettext (String)
#define N_(String) String

using namespace std;

static Export* ps_instantiate(const char *fname, int width, int height)
{
	return new PSExport(fname);
}

#ifdef NANOPLOT_WITH_EPS
static Export* eps_instantiate(const char *fname, int width, int height)
{
	return new EPSExport(fname);
}
#endif /* NANOPLOT_WITH_EPS */

void init()
{
}

plugin_info info()
{
	plugin_info info = {"ps", _("PS export"), _("Provides PS export.")};
	return info;
}

export_info ps()
{
	export_info info = {&ps_instantiate, false, _("PS"),
	                    _("Export Post Script"), "ps"};
	return info;
}

#ifdef NANOPLOT_WITH_EPS
export_info eps()
{
	export_info info = {&eps_instantiate, false, _("EPS"),
	                    _("Export Encapsulated Post Script"), "eps"};
	return info;
}
#endif /* NANOPLOT_WITH_EPS */	

