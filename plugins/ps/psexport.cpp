/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "psexport.h"

PSExport::PSExport(const char *filename): CairoExport() 
{
	surface = cairo_ps_surface_create(filename, IMG_WIDTH, IMG_HEIGHT);
	canvas  = cairo_create(surface);

	cairo_set_line_cap(canvas, CAIRO_LINE_CAP_SQUARE);
	cairo_select_font_face(canvas, "Serif", CAIRO_FONT_SLANT_NORMAL,
	                       CAIRO_FONT_WEIGHT_NORMAL);

	translate(0, 0);
}

PSExport::~PSExport()
{
	if (canvas) {
		cairo_show_page(canvas);
		cairo_destroy(canvas);
	}
	if (surface)
		cairo_surface_destroy(surface);
}

