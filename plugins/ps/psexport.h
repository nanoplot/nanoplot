/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PSEXPORT_H
#define PSEXPORT_H

#include <cairoexport.h>
#include <cairo-ps.h>

/**
 *   Export graphics to PS via cairo
 */
class PSExport: public CairoExport
{
public:
	/**
	 *   A constructor.
	 *   \param filename filename of the PS image to be written
	 */
	PSExport(const char *filename);

	/**  A destructor */
	~PSExport();
protected:
	cairo_surface_t *surface;  /**< Cairo PS surface */
};

#endif /* PSEXPORT_H */
