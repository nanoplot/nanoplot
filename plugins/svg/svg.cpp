/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "svg.h"
#include "svgexport.h"
#include "options.h"
#include "gettext.h"
#include <iostream>
#include <napp.h>
#define _(String) gettext (String)
#define N_(String) String

using namespace std;

static Export* svg_instantiate(const char *fname, int width, int height)
{
	return new SVGExport(fname);
}

void init()
{
}

plugin_info info()
{
	plugin_info info = {"svg", _("SVG export"), _("Provides SVG export.")};
	return info;
}

export_info svg()
{
	export_info info = {&svg_instantiate, false, _("SVG"),
	                    _("Export Scalable Vector Graphics"), "svg"};
	return info;
}

