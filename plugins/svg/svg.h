/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SVG_H
#define SVG_H

#include <plugin.h>

#define init svg_LTX_init
#define info svg_LTX_info
#define symbols svg_LTX_symbols
#define svg svg_LTX_svg

BEGIN_C_DECLS

void init();

plugin_info info();

exported_symbol symbols[] = {
	{"export", "svg"},
	{0, 0}
};

export_info svg();

END_C_DECLS

#endif /* SVG_H */
