/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SVGEXPORT_H
#define SVGEXPORT_H

#include <cairoexport.h>
#include <cairo-svg.h>

/**
 *   Export graphics to SVG via cairo
 */
class SVGExport: public CairoExport
{
public:
	/**
	 *   A constructor.
	 *   \param filename filename of the SVG image to be written
	 */
	SVGExport(const char *filename);

	/**  A destructor */
	~SVGExport();
private:
	cairo_surface_t *surface;  /**< Cairo SVG surface */
};

#endif /* SVGEXPORT_H */
