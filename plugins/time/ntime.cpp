/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ntime.h"
#include "options.h"
#include "gettext.h"
#include <stdio.h>

#define _(String) gettext (String)
#define N_(String) String

static Variant time_fromString(const char *str)
{
	int hour, min, sec, t;
	sscanf(str, "%d%*1c%2u%*1c%2u", &hour, &min, &sec);

	t  = hour * 3600 + min * 60 + sec;
	t %= 24 * 3600;

	return Variant(t);
}

static void time_toString(Variant v, char *buf, int buf_len)
{
	int hour, min;
	int sec = v.toInt();

	while (sec < 0)
		sec += 24 * 3600;
	
	sec %= 24 * 3600;
	hour = sec / 3600;
	sec -= hour * 3600;
	min  = sec / 60;
	sec -= min * 60;

	snprintf(buf, 16, "%d:%02d:%02d", hour, min, sec);
}

void init()
{
}

plugin_info info()
{
	plugin_info info = {"time", _("Time data type"),
	                    _("Provides data type time.")};
	return info;
}

datatype_info ntime()
{
	datatype_info info = {&time_fromString, &time_toString, type_int,
	                      "time", _("time")};
	return info;
}

