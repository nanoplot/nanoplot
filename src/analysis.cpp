/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2008, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "analysis.h"

fit_err Analysis::linearFitting(const vector<double> &x,
                                const vector<double> &y,
                                vector<double> *f,
                                string *info)
{
	unsigned int i;
	unsigned int n = x.size();
	double sumx  = 0;               /* Computing needed sums */
	double sumx2 = 0;
	double sumy  = 0;
	double sumy2 = 0;
	double sumxy = 0;
	double r = 0;
	ostringstream status;

	if (n < 2)
		return ferr_data;

	for (i = 0; i < n; ++i) {
		sumx  += x[i];
		sumx2 += x[i] * x[i];
		sumy  += y[i];
		sumy2 += y[i] * y[i];
		sumxy += x[i] * y[i];
	}
	
	double gamma = n * sumx2 - sumx * sumx;
	double a = (n * sumxy - sumx * sumy) / gamma;
	double b = (sumx2 * sumy - sumx * sumxy) / gamma;

	double sigma = 0;
	if (n > 2) {
		for (i = 0; i < n; ++i) {
			double t = y[i] - a * (x[i]) - b;
			sigma += t * t;
		}
		sigma = sqrt(sigma / (n - 2));
	} else {
		sigma = 0;
	}
	double da = sigma * sqrt(n / gamma);         /* Computing deltas */
	double db = sigma * sqrt(sumx2 / gamma);

	sumx2 = 0;
	sumy2 = 0;
	sumx = sumx / n;                      /* Mean x */
	sumy = sumy / n;                      /* Mean y */
	for (i = 0; i < n; ++i) {
		sumx2 += (x[i] - sumx) * (x[i] - sumx);
		sumy2 += (y[i] - sumy) * (y[i] - sumy); 
	}
	
	sumx2 = sqrt(sumx2 / (n - 1));
	sumy2 = sqrt(sumy2 / (n - 1));
	r = a * sumx2 / sumy2;
	
	status.setf(ios_base::scientific);
	status << "Y=aX+b:<br>a=" << a << " &plusmn; " << da << "<br>b=" << b 
	       << " &plusmn; " << db << "<br>r=" 
	       << resetiosflags(ios_base::scientific) << r;
	
	*info = status.str();

	/* 
	 *   Writing npl procedure:
	 *   a varx * b +
	 */
	f->push_back(npl_push);
	f->push_back(a);
	f->push_back(npl_varx);
	f->push_back(npl_mul);
	f->push_back(npl_push);
	f->push_back(b);
	f->push_back(npl_add);
	f->push_back(npl_end);
	return ferr_ok;
}

fit_err Analysis::quadFitting(const vector<double> &x,
                              const vector<double> &y,
                              vector<double> *f,
                              string *info)
{
	int i;
	int n = x.size();
	double sumx   = 0;                   /* Computing needed sums */
	double sumx2  = 0;
	double sumx3  = 0;
	double sumx4  = 0;
	double sumy   = 0;
	double sumxy  = 0;
	double sumx2y = 0;
	ostringstream status;

	if (n < 3)
		return ferr_data;

	for (i = 0; i < n; ++i) {
		sumx   += x[i];
		sumx2  += x[i] * x[i];
		sumx3  += x[i] * x[i] * x[i];
		sumx4  += x[i] * x[i] * x[i] * x[i];
		sumy   += y[i];
		sumxy  += x[i] * y[i];
		sumx2y += x[i] * x[i] * y[i];
	}
	
	double gamma = n * sumx4 * sumx2 + 2 * sumx3 * sumx2 * sumx
	             - sumx2 * sumx2 * sumx2 - sumx4 * sumx * sumx
	             - n * sumx3 * sumx3;
	
	double a = (n * sumx2 * sumx2y + sumx3 * sumx * sumy
	            + sumx2 * sumx * sumxy - sumx2 * sumx2 * sumy
		    - sumx * sumx * sumx2y - n * sumx3 * sumxy) / gamma;
	
	double b = (n * sumx4 * sumxy + sumx2 * sumx * sumx2y
	            + sumx3 * sumx2 * sumy - sumx2 * sumx2 * sumxy
		    - sumx4 * sumx * sumy - n * sumx3 * sumx2y) / gamma;
	
	double c = (sumx4 * sumx2 * sumy + sumx3 * sumx2 * sumxy
	            + sumx3 * sumx * sumx2y - sumx2 * sumx2 * sumx2y
		    - sumx4 * sumx * sumxy - sumx3 * sumx3 * sumy) / gamma;

	status.setf(ios_base::scientific);
	status << "Y=aX<sup>2</sup>+bX+c:<br>a=" << a << "<br>b=" << b
	       << "<br>c=" << c;
	
	*info = status.str();

	/*
	 *   Writing npl procedure:
	 *   a varx varx * * b varx * + c +
	 */
	f->push_back(npl_push);
	f->push_back(a);
	f->push_back(npl_varx);
	f->push_back(npl_varx);
	f->push_back(npl_mul);
	f->push_back(npl_mul);
	f->push_back(npl_push);
	f->push_back(b);
	f->push_back(npl_varx);
	f->push_back(npl_mul);
	f->push_back(npl_add);
	f->push_back(npl_push);
	f->push_back(c);
	f->push_back(npl_add);
	f->push_back(npl_end);
	return ferr_ok;
}

fit_err Analysis::cubicFitting(const vector<double> &x,
                               const vector<double> &y,
                               vector<double> *f,
                               string *info)
{
	int i;
	int n = x.size();
	double sumx   = 0;                /* Computing needed sums */
	double sumx2  = 0;
	double sumx3  = 0;
	double sumx4  = 0;
	double sumx5  = 0;
	double sumx6  = 0;
	double sumy   = 0;
	double sumxy  = 0;
	double sumx2y = 0;
	double sumx3y = 0;
	ostringstream status;

	if (n < 4)
		return ferr_data;

	for (i = 0; i < n; ++i) {
		sumx   += x[i];
		sumx2  += x[i] * x[i];
		sumx3  += x[i] * x[i] * x[i];
		sumx4  += x[i] * x[i] * x[i] * x[i];
		sumx5  += pow(x[i], 5);
		sumx6  += pow(x[i], 6);
		sumy   += y[i];
		sumxy  += x[i] * y[i];
		sumx2y += x[i] * x[i] * y[i];
		sumx3y += x[i] * x[i] * x[i] * y[i];
	}

	Matrix mtrx(4, 4);
	Matrix mtrx2;
	mtrx.at(0, 0) = sumx6;
	mtrx.at(0, 1) = sumx5;
	mtrx.at(0, 2) = sumx4;
	mtrx.at(0, 3) = sumx3;
	mtrx.at(1, 0) = sumx5;
	mtrx.at(1, 1) = sumx4;
	mtrx.at(1, 2) = sumx3;
	mtrx.at(1, 3) = sumx2;
	mtrx.at(2, 0) = sumx4;
	mtrx.at(2, 1) = sumx3;
	mtrx.at(2, 2) = sumx2;
	mtrx.at(2, 3) = sumx;
	mtrx.at(3, 0) = sumx3;
	mtrx.at(3, 1) = sumx2;
	mtrx.at(3, 2) = sumx;
	mtrx.at(3, 3) = n;
	double gamma = mtrx.det();
	mtrx2 = mtrx;
	
	mtrx.at(0, 0) = sumx3y;
	mtrx.at(1, 0) = sumx2y;
	mtrx.at(2, 0) = sumxy;
	mtrx.at(3, 0) = sumy;
	double a = mtrx.det() / gamma;

	mtrx = mtrx2;
	mtrx.at(0, 1) = sumx3y;
	mtrx.at(1, 1) = sumx2y;
	mtrx.at(2, 1) = sumxy;
	mtrx.at(3, 1) = sumy;
	double b = mtrx.det() / gamma;
	
	mtrx = mtrx2;
	mtrx.at(0, 2) = sumx3y;
	mtrx.at(1, 2) = sumx2y;
	mtrx.at(2, 2) = sumxy;
	mtrx.at(3, 2) = sumy;
	double c = mtrx.det() / gamma;
	
	mtrx = mtrx2;
	mtrx.at(0, 3) = sumx3y;
	mtrx.at(1, 3) = sumx2y;
	mtrx.at(2, 3) = sumxy;
	mtrx.at(3, 3) = sumy;
	double d = mtrx.det() / gamma;

	status.setf(ios_base::scientific);
	status << "Y=aX<sup>3</sup>+bX<sup>2</sup>+cX+d:<br>a=" << a
	       << "<br>b=" << b << "<br>c=" << c << "<br>d=" << d;
	*info = status.str();
	
	/* 
	 *   Writing npl procedure:
	 *   a varx 2 pow * b varx varx * * + c varx * + d + 
	 */
	f->push_back(npl_push);
	f->push_back(a);
	f->push_back(npl_varx);
	f->push_back(npl_push);
	f->push_back(3);
	f->push_back(npl_pow);
	f->push_back(npl_mul);
	f->push_back(npl_push);
	f->push_back(b);
	f->push_back(npl_varx);
	f->push_back(npl_varx);
	f->push_back(npl_mul);
	f->push_back(npl_mul);
	f->push_back(npl_add);
	f->push_back(npl_push);
	f->push_back(c);
	f->push_back(npl_varx); 
	f->push_back(npl_mul);
	f->push_back(npl_add);
	f->push_back(npl_push);
	f->push_back(d);
	f->push_back(npl_add);
	f->push_back(npl_end);
	return ferr_ok;
}

fit_err Analysis::quartFitting(const vector<double> &x,
                               const vector<double> &y,
                               vector<double> *f,
                               string *info)
{

	int i;
	int n = x.size();
	double sumx   = 0;                  /* Computing needed sums */
	double sumx2  = 0;
	double sumx3  = 0;
	double sumx4  = 0;
	double sumx5  = 0;
	double sumx6  = 0;
	double sumx7  = 0;
	double sumx8  = 0;
	double sumy   = 0;
	double sumxy  = 0;
	double sumx2y = 0;
	double sumx3y = 0;
	double sumx4y = 0;
	ostringstream status;

	if (n < 5)
		return ferr_data;

	for (i = 0; i < n; ++i) {
		sumx   += x[i];
		sumx2  += x[i] * x[i];
		sumx3  += x[i] * x[i] * x[i];
		sumx4  += x[i] * x[i] * x[i] * x[i];
		sumx5  += pow(x[i], 5);
		sumx6  += pow(x[i], 6);
		sumx7  += pow(x[i], 7);
		sumx8  += pow(x[i], 8);
		sumy   += y[i];
		sumxy  += x[i] * y[i];
		sumx2y += x[i] * x[i] * y[i];
		sumx3y += x[i] * x[i] * x[i] * y[i];
		sumx4y += pow(x[i], 4) * y[i];
	}

	Matrix mtrx(5, 5);
	Matrix mtrx2;
	mtrx.at(0, 0) = sumx8;
	mtrx.at(0, 1) = sumx7;
	mtrx.at(0, 2) = sumx6;
	mtrx.at(0, 3) = sumx5;
	mtrx.at(0, 4) = sumx4;
	mtrx.at(1, 0) = sumx7;
	mtrx.at(1, 1) = sumx6;
	mtrx.at(1, 2) = sumx5;
	mtrx.at(1, 3) = sumx4;
	mtrx.at(1, 4) = sumx3;
	mtrx.at(2, 0) = sumx6;
	mtrx.at(2, 1) = sumx5;
	mtrx.at(2, 2) = sumx4;
	mtrx.at(2, 3) = sumx3;
	mtrx.at(2, 4) = sumx2;
	mtrx.at(3, 0) = sumx5;
	mtrx.at(3, 1) = sumx4;
	mtrx.at(3, 2) = sumx3;
	mtrx.at(3, 3) = sumx2;
	mtrx.at(3, 4) = sumx;
	mtrx.at(4, 0) = sumx4;
	mtrx.at(4, 1) = sumx3;
	mtrx.at(4, 2) = sumx2;
	mtrx.at(4, 3) = sumx;
	mtrx.at(4, 4) = n;
	double gamma = mtrx.det();
	mtrx2 = mtrx;
	
	mtrx.at(0, 0) = sumx4y;
	mtrx.at(1, 0) = sumx3y;
	mtrx.at(2, 0) = sumx2y;
	mtrx.at(3, 0) = sumxy;
	mtrx.at(4, 0) = sumy;
	double a = mtrx.det() / gamma;

	mtrx = mtrx2;
	mtrx.at(0, 1) = sumx4y;
	mtrx.at(1, 1) = sumx3y;
	mtrx.at(2, 1) = sumx2y;
	mtrx.at(3, 1) = sumxy;
	mtrx.at(4, 1) = sumy;
	double b = mtrx.det() / gamma;
	
	mtrx = mtrx2;
	mtrx.at(0, 2) = sumx4y;
	mtrx.at(1, 2) = sumx3y;
	mtrx.at(2, 2) = sumx2y;
	mtrx.at(3, 2) = sumxy;
	mtrx.at(4, 2) = sumy;
	double c = mtrx.det() / gamma;
	
	mtrx = mtrx2;
	mtrx.at(0, 3) = sumx4y;
	mtrx.at(1, 3) = sumx3y;
	mtrx.at(2, 3) = sumx2y;
	mtrx.at(3, 3) = sumxy;
	mtrx.at(4, 3) = sumy;
	double d = mtrx.det() / gamma;

	mtrx = mtrx2;
	mtrx.at(0, 4) = sumx4y;
	mtrx.at(1, 4) = sumx3y;
	mtrx.at(2, 4) = sumx2y;
	mtrx.at(3, 4) = sumxy;
	mtrx.at(4, 4) = sumy;
	double e = mtrx.det() / gamma;
	
	status.setf(ios_base::scientific);
	status << "Y=aX<sup>4</sup>+bX<sup>3</sup>+cX<sup>2</sup>+dX+e:<br>a="
	       << a << "<br>b=" << b << "<br>c=" << c << "<br>d=" << d
	       << "<br>e=" << e;
	*info = status.str();
	
	/* 
	 *   Writing npl procedure:
	 *   a varx 4 pow * b varx 3 pow * + c varx varx * * + d varx * + e +
	 */
	f->push_back(npl_push);
	f->push_back(a);
	f->push_back(npl_varx);
	f->push_back(npl_push);
	f->push_back(4);
	f->push_back(npl_pow);
	f->push_back(npl_mul);
	f->push_back(npl_push);
	f->push_back(b);
	f->push_back(npl_varx);
	f->push_back(npl_push);
	f->push_back(3);
	f->push_back(npl_pow);
	f->push_back(npl_mul);
	f->push_back(npl_add);
	f->push_back(npl_push);
	f->push_back(c);
	f->push_back(npl_varx);
	f->push_back(npl_varx);
	f->push_back(npl_mul);
	f->push_back(npl_mul);
	f->push_back(npl_add);
	f->push_back(npl_push);
	f->push_back(d);
	f->push_back(npl_varx);
	f->push_back(npl_mul);
	f->push_back(npl_add);
	f->push_back(npl_push);
	f->push_back(e);
	f->push_back(npl_add);
	f->push_back(npl_end);
	return ferr_ok;
}

fit_err Analysis::polyFitting(const vector<double> &x,
                              const vector<double> &y,
                              int degree,
                              vector<double> *f,
                              string *info)
{
	/*
	 *   This is a generalization of fittings above to an arbitrary
	 *   polynomial degree.
	 */

	int i, j;
	int n = x.size();

	if (n < degree + 1)
		return ferr_data;

	ostringstream status;
	vector<double> sumx(degree + degree + 1); /* Computing needed sums */
	vector<double> sumy(degree + 1);

	sumx[0] = n;
	for (i = 0; i < n; ++i) {
		for (j = 1; j <= degree + degree; ++j) {
			sumx[j] += pow(x[i],j);
		}
		for (j = 0; j <= degree; ++j) {
			sumy[j] += pow(x[i], j) * y[i];
		}
	}
	
	Matrix mtrx(degree + 1, degree + 1);
	Matrix mtrx2;
	for (i = 0; i <= degree; ++i) {
		for (j = 0; j <= degree; ++j)
			mtrx.at(i, j) = sumx[degree + degree - j - i];
	}
	double gamma = mtrx.det();
	mtrx2 = mtrx;

	vector<double> a(degree + 1);

	for (i = 0; i <= degree; ++i) {
		mtrx = mtrx2;
		for (j = 0; j <= degree; ++j) {
			mtrx.at(j, i) = sumy[degree - j];
		}
		a[i] = mtrx.det() / gamma;
	}

	status.setf(ios_base::scientific);
	status << "Y=";
	for (i = 0; i < degree; ++i) {
		status << (char)('a' + i) << "X";
		if (degree - i != 1) 
			status << "<sup>" << degree - i << "</sup>";
		status << "+";
	}
	status << (char)('a' + degree) << ":";
	for (i = 0; i <= degree; ++i)
		status << "<br>" << (char)('a' + i) << "=" << a[i];
	*info = status.str();
	
	/*
	 *   Writing npl procedure
	 */
	f->push_back(npl_push);
	f->push_back(a[degree]);
	for (i = 0; i < degree; ++i) {
		f->push_back(npl_push);
		f->push_back(a[i]);
		f->push_back(npl_varx);
		f->push_back(npl_push);
		f->push_back((double)(degree - i));
		f->push_back(npl_pow);
		f->push_back(npl_mul);
		f->push_back(npl_add);
	}
	f->push_back(npl_end);
	return ferr_ok;
}

fit_err Analysis::expFitting(const vector<double> &x,
                             const vector<double> &y,
                             vector<double> *f,
                             string *info)
{
	/* 
	 *   We are using here a change of variables: y=ab^x => lny=x*lnb+lna.
	 *   Now, coefficients lnb=A and lna=B can be easily computed by linear 
	 *   regression; this yields a=exb(B) and b=exp(A)
	 */

	int i;
	int n = x.size();
	double sumx  = 0;				/* Computing needed sums */
	double sumx2 = 0;
	double sumy  = 0;
	double sumy2 = 0;
	double sumxy = 0;
	ostringstream status;
	vector<double> ly = y;

	if (n < 2)
		return ferr_data;

	for (i = 0; i < n; ++i) {
		if (y[i] > 0) {
			ly[i] = log(y[i]);
		} else {
			*info = "ERROR: DOMAIN";
			return ferr_domain;
		}
	}

	for (i = 0; i < n; ++i) {
		sumx  += x[i];
		sumx2 += x[i] * x[i];
		sumy  += ly[i];
		sumy2 += ly[i] * ly[i];
		sumxy += x[i] * ly[i];
	}
	
	double gamma = n * sumx2 - sumx * sumx;
	double b = (n * sumxy - sumx * sumy) / gamma;       /* our A=lnb */
	double a = (sumx2 * sumy - sumx * sumxy) / gamma;   /* our B=lna */

	a = exp(a);                        /* returning to old variables */
	b = exp(b);

	status.setf(ios_base::scientific);
	status << "Y=ab^X:<br>a=" << a << "<br>b=" << b;
	*info = status.str();

	/* 
	 *   Writing npl procedure:
	 *   a b varx pow *
	 */
	f->push_back(npl_push);
	f->push_back(a);
	f->push_back(npl_push);
	f->push_back(b);
	f->push_back(npl_varx);
	f->push_back(npl_pow);
	f->push_back(npl_mul);
	f->push_back(npl_end);
	return ferr_ok;
}

fit_err Analysis::logFitting(const vector<double> &x,
                             const vector<double> &y,
                             vector<double> *f,
                             string *info)
{
	/* 
	 *   Given the regression equation y=a+blnx, by doing substitution x:=lnx 
	 *   we're able to obtain a simple problem solvable by linear regression
	 */

	int i;
	int n = x.size();
	double sumx  = 0;                    /* Computing needed sums */
	double sumx2 = 0;
	double sumy  = 0;
	double sumy2 = 0;
	double sumxy = 0;
	ostringstream status;
	vector<double> lx = x;

	if (n < 2)
		return ferr_data;

	for (i = 0; i < n; ++i) {
		if (x[i] > 0) {
			lx[i] = log(x[i]);   /* Above mentioned substitution */
		} else {
			*info = "ERROR: DOMAIN";
			return ferr_domain;
		}
	}

	for (i = 0; i < n; ++i) {
		sumx  += lx[i];
		sumx2 += lx[i] * lx[i];
		sumy  += y[i];
		sumy2 += y[i] * y[i];
		sumxy += x[i] * y[i];
	}
	
	double gamma = n * sumx2 - sumx * sumx;
	double b = (n * sumxy - sumx * sumy) / gamma;
	double a = (sumx2 * sumy - sumx * sumxy) / gamma;

	status.setf(ios_base::scientific);
	status << "Y=a+bln(X):<br>a=" << a << "<br>b=" << b;
	*info = status.str();

	/*
	 *   Writing npl procedure:
	 *   a b varx ln mul +
	 */
	f->push_back(npl_push);
	f->push_back(a);
	f->push_back(npl_push);
	f->push_back(b);
	f->push_back(npl_varx);
	f->push_back(npl_ln);
	f->push_back(npl_mul);
	f->push_back(npl_add);
	f->push_back(npl_end);
	return ferr_ok;
}

fit_err Analysis::powerFitting(const vector<double> &x,
                               const vector<double> &y,
                               vector<double> *f,
                               string *info)
{
	/* 
	 *   Given the regression equation y=ax^b, by logarithming both sides
	 *   we obtain lny=lna+blnx. Now, by doing substitution lny:=y, lnx:=x,
	 *   we are able to compute coefficients a and b by linear regression.
	 */
	
	int i;
	int n = x.size();
	double sumx  = 0;             /* Computing needed sums */
	double sumx2 = 0;
	double sumy  = 0;
	double sumy2 = 0;
	double sumxy = 0;
	ostringstream status;
	vector<double> lx = x;
	vector<double> ly = y;

	if (n < 2)
		return ferr_data;

	for (i = 0; i < n; ++i) {
		/* Doing above mentioned substitutions */
		if (x[i] > 0 && y[i] > 0) {
			lx[i] = log(x[i]);
			ly[i] = log(y[i]);
		} else {
			*info = "ERROR: DOMAIN";
			return ferr_domain;
		}
	}

	for (i = 0; i < n; ++i) {
		sumx  += lx[i];
		sumx2 += lx[i] * lx[i];
		sumy  += ly[i];
		sumy2 += ly[i] * ly[i];
		sumxy += lx[i] * ly[i];
	}
	
	double gamma = n * sumx2 - sumx * sumx;
	double b = (n * sumxy - sumx * sumy) / gamma;
	double a = (sumx2 * sumy - sumx * sumxy) / gamma;
	a = exp(a);

	status.setf(ios_base::scientific);
	status << "Y=ax^b:<br>a=" << a << "<br>b=" << b;
	*info = status.str();

	/* 
	 *   Writing npl procedure:
	 *   a varx b pow *
	 */
	f->push_back(npl_push);
	f->push_back(a);
	f->push_back(npl_varx);
	f->push_back(npl_push);
	f->push_back(b);
	f->push_back(npl_pow);
	f->push_back(npl_mul);
	f->push_back(npl_end);
	return ferr_ok;
}

fit_err Analysis::gaussFitting(const vector<double> &x,
                                      const vector<double> &y,
                                      vector<double> *f,
                                      string *info)
{
	/* 
	 *   Given the regression equation y=a*exp(-b(x-c)^2), by logarithming
	 *   both sides, we obtain lny=-bx^2+2bcx-bc^2+lna. Now, by doing
	 *   substitution lny:=y, we are able to compute coefficients A=-b,
	 *   B=2bc and C=-bc^2+lna by quadratic regression. This yields b=-A,
	 *   c=B/2b and a=exp(C+bc^2).
	 */
	
	int i;
	int n = x.size();
	double sumx   = 0;                   /* Computing needed sums */
	double sumx2  = 0;
	double sumx3  = 0;
	double sumx4  = 0;
	double sumy   = 0;
	double sumxy  = 0;
	double sumx2y = 0;
	ostringstream status;
	vector<double> ly = y;

	if (n < 3)
		return ferr_data;

	for (i = 0; i < n; ++i) {
		/* Doing above mentioned substitutions */
		if (y[i] > 0) {
			ly[i] = log(y[i]);
		} else {
			*info = "ERROR: DOMAIN";
			return ferr_domain;
		}
	}

	for (i = 0; i < n; ++i) {
		sumx   += x[i];
		sumx2  += x[i] * x[i];
		sumx3  += x[i] * x[i] * x[i];
		sumx4  += x[i] * x[i] * x[i] * x[i];
		sumy   += ly[i];
		sumxy  += x[i] * ly[i];
		sumx2y += x[i] * x[i] * ly[i];
	}
	
	double gamma = n * sumx4 * sumx2 + 2 * sumx3 * sumx2 * sumx
	             - sumx2 * sumx2 * sumx2 - sumx4 * sumx * sumx
		     - n * sumx3 * sumx3;
	
	double A = (n * sumx2 * sumx2y + sumx3 * sumx * sumy
	            + sumx2 * sumx * sumxy - sumx2 * sumx2 * sumy
		    - sumx * sumx * sumx2y - n * sumx3 * sumxy) / gamma;
	
	double B = (n * sumx4 * sumxy + sumx2 * sumx * sumx2y
	            + sumx3 * sumx2 * sumy - sumx2 * sumx2 * sumxy
		    - sumx4 * sumx * sumy - n * sumx3 * sumx2y) / gamma;
	
	double C = (sumx4 * sumx2 * sumy + sumx3 * sumx2 * sumxy
	            + sumx3 * sumx * sumx2y - sumx2 * sumx2 * sumx2y
		    - sumx4 * sumx * sumxy - sumx3 * sumx3 * sumy) / gamma;

	double b = -A;
	double c = B / (b + b);
	double a = exp(C + b * c * c);
	double var = 1 / sqrt(2 * b);

	status.setf(ios_base::scientific);
	status << "Y=aexp(-b(x-c)<sup>2</sup>)<br>a=" << a << "<br>b=" << b 
	       << "<br>c=" << c << "<br>sigma=" << var << "<br>" 
	       << "FWHM" << "=" << 2.355*var;
	*info = status.str();

	/*
	 *   Writing npl procedure:
	 *   a -b varx c - dup * * exp *
	 */
	f->push_back(npl_push);
	f->push_back(a);
	f->push_back(npl_push);
	f->push_back(-b);
	f->push_back(npl_varx);
	f->push_back(npl_push);
	f->push_back(c);
	f->push_back(npl_sub);
	f->push_back(npl_dup);
	f->push_back(npl_mul);
	f->push_back(npl_mul);
	f->push_back(npl_exp);
	f->push_back(npl_mul);
	f->push_back(npl_end);
	return ferr_ok;
}

void Analysis::findPeaks(const vector<double> &x,
                         const vector<double> &y,
                         double sensit,
                         int type,
                         vector< vector<double> > *peaks)
{
	int n = x.size();
	double d;
	double ad = 0;
	double h;
	double ymax = y[0], ymin = y[0];

	for (int i = 0; i < n; ++i) {
		if (ymax < y[i])
			ymax = y[i];
		else if (ymin > y[i])
			ymin = y[i];
	}
	if (ymin == ymax)
		return;

	vector<double> px;
	vector<double> py;

	for (int i = 2; i < n - 2; ++i) {
		d = (-y[i - 2] - 2 * y[i - 1] + 6 * y[i] - 2 * y[i + 1]
		  - y[i + 2]);
		h = x[i + 1] - x[i - 1];
		d /= h * h;
		ad += abs(d);
	}
	ad /= n - 4;

	for (int i = 2; i < n - 2; ++i) {
		d = (-y[i - 2] - 2 * y[i - 1] + 6 * y[i] - 2 * y[i + 1]
		  - y[i + 2]);
		h = x[i + 1] - x[i - 1];
		d /= h * h * ad;
		if (type == 0) {
			if (d > sensit && y[i] >= y[i - 1]
			    && y[i] >= y[i + 1])
			{
				px.push_back(x[i]);
				py.push_back(y[i]);
			}
		} else if (type == 1) {
			if (-d > sensit && y[i] <= y[i - 1]
			    && y[i] <= y[i + 1])
			{
				px.push_back(x[i]);
				py.push_back(y[i]);
			}
		} else {
			if (abs(d) > sensit && ((y[i] >= y[i - 1]
			    && y[i] >= y[i + 1]) || (y[i] <= y[i - 1]
			    && y[i] <= y[i + 1])))
			{
				px.push_back(x[i]);
				py.push_back(y[i]);
			}
		}
	}
	
	if (px.size() == 0)
		return;

	if (!px.empty()) {
		peaks->push_back(px);
		peaks->push_back(py);
	}
}

