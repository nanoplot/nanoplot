/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2008, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ANALYSIS_H
#define ANALYSIS_H

#include <vector>
#include <string>
#include <sstream>
#include <iomanip>
#include <cmath>
#include "nplotnamespace.h"
#include "types.h"

using namespace std;
using namespace nplot;

/**
 *   A collection of analysis functions.
 */
class Analysis
{
public:
	/**  Do a linear fitting.
	 *   \param x x coordinates
	 *   \param y y coordinates
	 *   \param func fitted function will be returned here
	 *   \param info information about fitting parameters
	 */
	static fit_err linearFitting(const vector<double> &x,
	                             const vector<double> &y,
	                             vector<double> *f,
	                             string *info);

	/**  Do a quad fitting.
	 *   \param x x coordinates
	 *   \param y y coordinates
	 *   \param func fitted function will be returned here
	 *   \param info information about fitting parameters
	 */
	static fit_err quadFitting(const vector<double> &x,
	                           const vector<double> &y,
	                           vector<double> *f,
	                           string *info);
	
	/**  Do a cubic fitting.
	 *   \param x x coordinates
	 *   \param y y coordinates
	 *   \param func fitted function will be returned here
	 *   \param info information about fitting parameters
	 */
	static fit_err cubicFitting(const vector<double> &x,
	                            const vector<double> &y,
	                            vector<double> *f,
	                            string *info);
	
	/**  Do a quart fitting.
	 *   \param x x coordinates
	 *   \param y y coordinates
	 *   \param func fitted function will be returned here
	 *   \param info information about fitting parameters
	 */
	static fit_err quartFitting(const vector<double> &x,
	                            const vector<double> &y,
	                            vector<double> *f,
	                            string *info);

	/**  Do a polynomial fitting.
	 *   \param x x coordinates
	 *   \param y y coordinates
	 *   \param func fitted function will be returned here
	 *   \param info information about fitting parameters
	 */
	static fit_err polyFitting(const vector<double> &x,
	                           const vector<double> &y,
	                           int degree,
	                           vector<double> *f,
	                           string *info);
	
	/**  Do an exponential fitting.
	 *   \param x x coordinates
	 *   \param y y coordinates
	 *   \param func fitted function will be returned here
	 *   \param info information about fitting parameters
	 */
	static fit_err expFitting(const vector<double> &x,
	                          const vector<double> &y,
	                          vector<double> *f,
	                          string *info);
	
	/**  Do a logarithmic fitting.
	 *   \param x x coordinates
	 *   \param y y coordinates
	 *   \param func fitted function will be returned here
	 *   \param info information about fitting parameters
	 */
	static fit_err logFitting(const vector<double> &x,
	                          const vector<double> &y,
	                          vector<double> *f,
	                          string *info);
	
	/**  Do a power fitting.
	 *   \param x x coordinates
	 *   \param y y coordinates
	 *   \param func fitted function will be returned here
	 *   \param info information about fitting parameters
	 */
	static fit_err powerFitting(const vector<double> &x,
	                            const vector<double> &y,
	                            vector<double> *f,
	                            string *info);
	
	/**  Do a gaussian fitting.
	 *   \param x x coordinates
	 *   \param y y coordinates
	 *   \param func fitted function will be returned here
	 *   \param info information about fitting parameters
	 */
	static fit_err gaussFitting(const vector<double> &x,
	                            const vector<double> &y,
	                            vector<double> *f,
	                            string *info);
	
	/**  Find peaks.
	 *   \param x x coordinates
	 *   \param y y coordinates
	 *   \param func fitted function will be returned here
	 *   \param info information about fitting parameters
	 */
	static void findPeaks(const vector<double> &x,
	                      const vector<double> &y,
	                      double sensit,
	                      int type,
	                      vector< vector<double> > *peaks);
};

#endif /* ANALYSIS_H */
