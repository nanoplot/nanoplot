/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "array.h"
#include "napp.h"
#include <fstream>
#include <string.h>

Array::Array(int _cols, int _rows)
 : vcols(_cols), vrows(_rows)
{
	virtcols = vcols;
	virtrows = vrows;
	data = new Variant*[vcols];
	types = new int[vcols];
	for (int i = 0; i < vcols; ++i) {
		data[i] = new Variant[vrows];
		types[i] = type_unknown;
	}
}

Array::Array()
 : data(NULL), types(NULL), vcols(0), vrows(0), virtcols(0), virtrows(0)
{
}

Array::Array(const Array &rhs)
 : data(NULL), types(NULL), vcols(0), vrows(0), virtcols(0), virtrows(0)
{
	setCols(rhs.cols());
	setRows(rhs.rows());
	for (int i = 0; i < vcols; ++i) {
		for (int j = 0; j < vrows; ++j)
			data[i][j] = rhs[i][j];
		types[i] = rhs.types[i];
	}
}

Array::~Array()
{
	if (data) {
		for (int i = 0; i < vcols; ++i)
			delete[] data[i];
		delete[] data;
	}
	if (types)
		delete[] types;
}

void Array::load(SessionReader *reader)
{
	setCols(reader->getPropertyInt("cols"));
	setRows(reader->getPropertyInt("rows"));

	reader->processChildren();
	
	vector<datatype_info> typesvec = nApp::instance()->types();

	int i = 0;
	for (const char *name = reader->nextNode(); name;
	     name = reader->nextNode())
	{
		if (strcmp(name, "column") == 0) {
			string type = reader->getProperty("type");
			types[i] = type_unknown;
			unsigned int t = 0;
			for (; t < typesvec.size(); ++t) {
				if (type != typesvec[t].name_generic)
					continue;
				types[i] = t;
				break;
			}
			string content = reader->getValue();
			string token;
			int j = 0;
			for (string::iterator it = content.begin();
			     it != content.end(); ++it)
			{
				if (*it != ' ') {
					token += *it;
					continue;
				}
				fromString(i, j++, token.c_str());
				token.clear();
			}
			i++;
		}
	}
}

void Array::save(SessionWriter *writer) const
{
	string s;
	writer->startElement("Array");
	writer->setProperty("rows", vrows);
	writer->setProperty("cols", vcols);

	for (int i = 0; i < vcols; ++i) {
		writer->startElement("column");
		datatype_info info = nApp::instance()->types()[types[i]];
		writer->setProperty("type", info.name_generic);
		s = "";
		for (int j = 0; j < vrows; ++j)
			s += toString(i, j) + " ";
		writer->setValue(s.c_str());
		writer->endElement();
	}
	writer->endElement();
}

const Variant* Array::operator[](int i) const
{
	return data[i];
}

Variant* Array::operator[](int i)
{
	return data[i];
}

const Array& Array::operator=(const Array &rhs)
{
	setCols(rhs.cols());
	setRows(rhs.rows());
	for (int i = 0; i < vcols; ++i) {
		for (int j = 0; j < vrows; ++j)
			data[i][j] = rhs[i][j];
		types[i] = rhs.types[i];
	}
	return rhs;
}

void Array::setCols(int n)
{
	if (n == vcols) {
		return;
	} else if (n < vcols) {
		for (int i = n; i < vcols; ++i)
			delete[] data[i];
	} else { /* n > vcols */
		if (n > virtcols) {
			virtcols = n;
			Variant  **tmpdata  = new Variant*[virtcols];
			int *tmptypes = new int[virtcols];
			for (int i = 0; i < vcols; ++i) {
				tmpdata[i]  = data[i];
				tmptypes[i] = types[i];
			}
			if (data)
				delete[] data;
			if (types)
				delete[] types;
			data  = tmpdata;
			types = tmptypes;
		}
		if (virtrows) {
			for (int i = vcols; i < n; ++i)
				data[i] = new Variant[virtrows];
		} else {
			for (int i = vcols; i < n; ++i)
				data[i] = NULL;
		}
		for (int i = vcols; i < n; ++i)
			types[i] = type_unknown;
	}
	vcols = n;
}

void Array::setRows(int n)
{
	if (n == vrows) {
		return;
	} else if (n > vrows) {
		if (n > virtrows) {
			virtrows = n;
			for (int i = 0; i < vcols; ++i) {
				Variant *tmp = new Variant[virtrows];
				for (int j = 0; j < vrows; ++j)
					tmp[j] = data[i][j];
				if (data[i] != NULL)
					delete[] data[i];
				data[i] = tmp;
			}
		}
	}
	vrows = n;
}

void Array::sortAscending(int c)
{
	if (!data)
		return;

	int n = 0;
	int last_change = vrows;
	int refs[vrows];
	for (int i = 0; i < vrows; ++i)
		refs[i] = i;

	Variant *col = data[c];
	datatype_info info = nApp::instance()->types()[types[c]];
	int type = info.primitive;
	do {
		n = last_change;
		last_change = 0;
		for (int i = 1; i < n; ++i) {
			if ((type == type_double
			     && col[i - 1].toDouble() > col[i].toDouble())
			    || ((type == type_int)
			        && col[i - 1].toInt() > col[i].toInt()))
			{
				Variant t = col[i];
				col[i] = col[i - 1];
				col[i - 1] = t;
				int t2 = refs[i];
				refs[i] = refs[i - 1];
				refs[i - 1] = t2;
				last_change = i;
			}
		}
	} while (last_change != 0);

	Variant tmp[vrows];
	for (int i = 0; i < vcols; ++i) {
		if (i == c)
			continue;
		for (int j = 0; j < vrows; ++j)
			tmp[j] = data[i][refs[j]];
		for (int j = 0; j < vrows; ++j)
			data[i][j] = tmp[j];
	}
}

void Array::sortDescending(int c)
{
	if (!data)
		return;

	int n = 0;
	int last_change = vrows;

	int refs[vrows];
	for (int i = 0; i < vrows; ++i)
		refs[i] = i;

	Variant *col = data[c];
	datatype_info info = nApp::instance()->types()[types[c]];
	int type = info.primitive;
	do {
		n = last_change;
		last_change = 0;
		for (int i = 1; i < n; ++i) {
			if ((type == type_double
			     && col[i - 1].toDouble() < col[i].toDouble())
			    || ((type == type_int)
			        && col[i - 1].toInt() < col[i].toInt()))
			{
				Variant t = col[i];
				col[i] = col[i - 1];
				col[i - 1] = t;
				int t2 = refs[i];
				refs[i] = refs[i - 1];
				refs[i - 1] = t2;
				last_change = i;
			}
		}
	} while (last_change != 0);

	Variant tmp[vrows];
	for (int i = 0; i < vcols; ++i) {
		if (i == c)
			continue;
		for (int j = 0; j < vrows; ++j)
			tmp[j] = data[i][refs[j]];
		for (int j = 0; j < vrows; ++j)
			data[i][j] = tmp[j];
	}
}

bool Array::good() const
{
	if (!data)
		return false;
	
	bool good;
	for (int r = 0; r < vrows; ++r) {
		good = true;
		for (int c = 0; c < vcols; ++c) {
			if (data[c][r].good())
				continue;
			good = false;
		}
		if (good)
			return true;
	}
	
	return false;
}

string Array::toString(int c, int r) const
{
	if (!data[c][r].good() || types[c] == type_unknown)
		return "";
	char buf[16];
	datatype_info info = nApp::instance()->types()[types[c]];
	info.toString(data[c][r], buf, 16);
		
	return string(buf);
}

void Array::fromString(int c, int r, const char *str)
{
	if (types[c] == type_unknown)
		return;
	
	if (*str == 0) {
		data[c][r] = Variant();
		return;
	}

	datatype_info info = nApp::instance()->types()[types[c]];
	data[c][r] = info.fromString(str);
}

double Array::toDouble(int c, int r) const
{
	if (!data[c][r].good() || types[c] == type_unknown)
		return 0;
	
	datatype_info info = nApp::instance()->types()[types[c]];
	switch (info.primitive) {
	case type_double:
		return data[c][r].toDouble();
	case type_int:
		return (double) data[c][r].toInt();
	default:
		return 0;
	}
}

vector<double> Array::columnDouble(int n) const
{
	if (n > vcols || types[n] == type_unknown)
		return vector<double>(0);

	vector<double> ret;
	Variant *col = data[n];

	datatype_info info = nApp::instance()->types()[types[n]];
	switch (info.primitive) {
	case type_double:
		for (int i = 0; i < vrows; ++i) {
			if (!col[i].good())
				continue;
			ret.push_back(col[i].toDouble());
		}
		break;
	case type_int:
		for (int i = 0; i < vrows; ++i) {
			if (!col[i].good())
				continue;
			ret.push_back((double) col[i].toInt());
		}
		break;
	default:
		break;
	}

	return ret;
}

int Array::columnType(int n) const
{
	if (n > vcols)
		return type_unknown;

	return types[n];
}

void Array::setColumnType(int n, int type)
{
	if (n > vcols || types[n] == type || type == type_unknown)
		return;
	
	datatype_info newinfo = nApp::instance()->types()[type];
	datatype_info oldinfo = nApp::instance()->types()[types[n]];

	switch (newinfo.primitive) {
	case type_double:
		if (oldinfo.primitive == type_int) {
			for (int i = 0; i < vrows; ++i) {
				if (!data[n][i].good())
					continue;
				data[n][i] = (double) data[n][i].toInt();
			}
		}
		break;
	case type_int:
		if (oldinfo.primitive == type_double) {
			for (int i = 0; i < vrows; ++i) {
				if (!data[n][i].good())
					continue;
				data[n][i] = (int) data[n][i].toDouble();
			}
		}
		break;
	default:
		break;
	}

	types[n] = type;
}

Array Array::columns(int n, int *c) const
{
	Array ret(n, vrows);
	int row = 0;
	bool complete;
	for (int i = 0; i < n; ++i) {
		if (c[i] < 0)
			ret.setColumnType(i, type_int);
		else
			ret.setColumnType(i, types[c[i]]);
	}
	for (int j = 0; j < vrows; ++j) {
		complete = true;
		for (int i = 0; i < n; ++i) {
			if (c[i] < 0) {
				ret[i][row] = j + 1;
			} else {
				if (!data[c[i]][j].good()) {
					complete = false;
					break;
				}
				ret[i][row] = data[c[i]][j];
			}
		}
		if (complete)
			row++;
	}
	ret.setRows(row);

	return ret;
}

bool Array::exportAscii(const char *fname) const
{
	ofstream outfile(fname);

	if (!outfile.good())
		return false;
	
	for (int j = 0; j < vrows; ++j) {
		for (int i = 0; i < vcols - 1; ++i)
			outfile << toString(i, j) << "\t";
		outfile << toString(vcols - 1, j) << endl;
	}

	return true;
}

bool Array::exportLatexTabular(const char *fname) const
{
	ofstream outfile(fname);

	if (!outfile.good())
		return false;

	outfile << "\\begin{tabular}{|";
	for (int i = 0; i < vcols; ++i)
		outfile << "c|";
	outfile << "}\n\\hline" << endl;
	for (int j = 0; j < vrows; j++) {
		for (int i = 0; i < vcols - 1; ++i) {
			outfile << toString(i, j) << " & ";
		}
		outfile << toString(vcols - 1, j) << "\\\\\n\\hline"
			<< endl;
	}
	outfile << "\\end{tabular}" << endl;

	return true;
}

