/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARRAY_H
#define ARRAY_H

#include "sessionreader.h"
#include "sessionwriter.h"
#include "types.h"
#include "nplotnamespace.h"
#include <string>
#include <vector>

using namespace nplot;
using namespace std;

class Array {
public:
	Array(int cols, int rows);
	Array();
	Array(const Array &rhs);
	~Array();
	void load(SessionReader *reader);
	void save(SessionWriter *writer) const;
	const Variant* operator[](int i) const;
	Variant* operator[](int i);
	const Array& operator=(const Array &rhs);
	int cols() const { return vcols; };
	int rows() const { return vrows; };
	void setCols(int n);
	void setRows(int n);
	void sortAscending(int col);
	void sortDescending(int col);
	bool good() const;
	string toString(int c, int r) const;
	void fromString(int c, int r, const char *str);
	double toDouble(int c, int t) const;
	vector<double> columnDouble(int n) const;
	int columnType(int n) const;
	void setColumnType(int n, int);
	Array columns(int n, int *c) const;
	bool exportAscii(const char *fname) const;
	bool exportLatexTabular(const char *fname) const;
private:
	Variant **data;
	int *types;
	int vcols;
	int vrows;
	int virtcols;
	int virtrows;
};

#endif /* ARRAY_H */
