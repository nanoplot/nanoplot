/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "axes.h"
#include "config.h"
#include "napp.h"
#include "nplotnamespace.h"
#include "tex.h"

using namespace nplot;

/******** A B S T R A C T A X I S *********/

AbstractAxis::AbstractAxis()
{
	vmin          = 0;
	vmax          = 1;
	vscale        = scale_linear;
	vnminor       = 4;
	vmajorinc     = 0.1;
	vmajorh       = 6;
	vminorh       = 3;
	vticsType     = tics_out;
	vlabel        = "";
	vlabelSize    = 18;
	vlabelCenter  = true;
	vgridMaj      = false;
	vgridMin      = false;
	vzeroline     = false;
	vgridMajThick = 1;
	vgridMinThick = 1;
	vzeroThick    = 1;
	vgridMajDash  = 2;
	vgridMinDash  = 3;
	vzeroDash     = 1;
	vdataType     = type_double;
}

string AbstractAxis::toString(double d) const
{
	datatype_info info = nApp::instance()->types()[vdataType];
	char buf[16];
	Variant v;
	if (info.primitive == type_double)
		v = d;
	else
		v = (int) d;
	
	info.toString(v, buf, 16);
	return string(buf);
}

/************* X A X I S 2 D **************/

XAxis2D::XAxis2D(XY *_xychart): xychart(_xychart)
{
}

void XAxis2D::load(SessionReader *reader)
{
	vector<datatype_info> typesvec = nApp::instance()->types();

	vmin          = reader->getPropertyDouble("vmin");
	vmax          = reader->getPropertyDouble("vmax");
	vscale        = (ScaleType)reader->getPropertyInt("vscale");
	vmajorinc     = reader->getPropertyDouble("vmajorinc");
	vnminor       = reader->getPropertyInt("vnminor");
	vmajorh       = reader->getPropertyInt("vmajorh");
	vminorh       = reader->getPropertyInt("vminorh");
	vticsType     = (TicsType)reader->getPropertyInt("vticsType");
	vlabel        = reader->getProperty("vlabel");
	vlabelSize    = reader->getPropertyInt("vlabelSize");
	vlabelCenter  = reader->getPropertyBool("vlabelCenter");
	vgridMaj      = reader->getPropertyBool("vgridMaj");
	vgridMin      = reader->getPropertyBool("vgridMin");
	vgridMajThick = reader->getPropertyDouble("vgridMajThick");
	vgridMinThick = reader->getPropertyDouble("vgridMinThick");
	vgridMajDash  = reader->getPropertyInt("vgridMajDash");
	vgridMinDash  = reader->getPropertyInt("vgridMinDash");
	vzeroline     = reader->getPropertyBool("vzeroline");
	vzeroThick    = reader->getPropertyDouble("vzeroThick");
	vzeroDash     = reader->getPropertyInt("vzeroDash");
}

void XAxis2D::save(SessionWriter *writer) const
{
	writer->startElement("XAxis2D");
	writer->setProperty("vmin", vmin);
	writer->setProperty("vmax", vmax);
	writer->setProperty("vscale", vscale);
	writer->setProperty("vmajorinc", vmajorinc);
	writer->setProperty("vnminor", vnminor);
	writer->setProperty("vmajorh", vmajorh);
	writer->setProperty("vminorh", vminorh);
	writer->setProperty("vticsType", vticsType);
	writer->setProperty("vlabel", vlabel.c_str());
	writer->setProperty("vlabelSize", vlabelSize);
	writer->setProperty("vlabelCenter", vlabelCenter);
	writer->setProperty("vgridMaj", vgridMaj);
	writer->setProperty("vgridMin", vgridMin);
	writer->setProperty("vgridMajThick", vgridMajThick);
	writer->setProperty("vgridMinThick", vgridMinThick);
	writer->setProperty("vgridMajDash", vgridMajDash);
	writer->setProperty("vgridMinDash", vgridMinDash);
	writer->setProperty("vzeroline", vzeroline);
	writer->setProperty("vzeroThick", vzeroThick);
	writer->setProperty("vzeroDash", vzeroDash);
	writer->endElement();
}

void XAxis2D::repaint(Export *exp) const
{
	int    leftmgn  = xychart->leftmgn;
	int    rightmgn = xychart->rightmgn;
	int    btmmgn   = xychart->btmmgn;
	int    majy1    = exp->height() - btmmgn;
	int    majy2    = majy1;
	int    miny1    = majy1;
	int    miny2    = majy1;
	int    cw       = exp->width() - leftmgn - rightmgn; /* Canvas width */
	double trXmin   = trX(vmin);
	double rw       = trX(vmax) - trXmin;                /* "Real" width */
	double min, max, inc, rx, d, eps;
	int j;
	string ticlabel;
	TeX tex(exp);

	switch ((int) vticsType) {
	case tics_in:
		majy1 -= vmajorh;
		miny1 -= vminorh;
		break;
	case tics_out:
		majy2 += vmajorh;
		miny2 += vminorh;
		break;
	default: /* Tics in/out */
		majy1 -= vmajorh;
		miny1 -= vminorh;
		majy2 += vmajorh;
		miny2 += vminorh;
	}

	exp->setColor(0, 0, 0);
	exp->drawLine(leftmgn, exp->height() - btmmgn,
	              exp->width() - rightmgn, exp->height() - btmmgn);

	inc = vmajorinc;
	if (vmin < vmax) {
		min = vmin;
		max = vmax;
	} else {
		min = vmax;
		max = vmin;
	}
	eps  = epsilon * fabs(max);
	max += eps;                 /* Adding small epsilon to max */
	
	for (j = 0; (d = incrementX(min, inc, j)) <= max; ++j) {
		rx = leftmgn + cw * (trX(d) - trXmin) / rw;
		if (rx >= leftmgn && rx <= exp->width() - rightmgn) {
			exp->drawLine(rx, majy1, rx, majy2);
			exp->setFont(Export::times, 12);
			double fh = exp->getFontHeight();
			if (vdataType == type_double)
				ticlabel = tex.sci2pwr(d);
			else
				ticlabel = toString(d);
			rx -= (int) (0.5 * tex.textWidth(ticlabel.c_str(),
			                                  12));
			tex.textInterpret(rx, majy2 + fh, ticlabel.c_str(),
			                   12, 0);
		}
		for (int n = 1; n <= vnminor; n++) {
			double mininc = minincX(d, n * inc / (vnminor + 1));
			rx = leftmgn + cw * (trX(mininc) - trXmin) / rw;
			if (rx >= leftmgn && rx <= exp->width() - rightmgn)
				exp->drawLine(rx, miny1, rx, miny2);
		}
	}
	
	if (vlabel != "") {
		d = tex.textWidth(vlabel.c_str(), vlabelSize);
		rx = vlabelCenter? (cw - d) * 0.5 + leftmgn 
		                 : exp->width() - d - rightmgn;
		tex.textInterpret(rx, exp->height() - exp->getFontDescent(),
		                   vlabel.c_str(), (double) vlabelSize, 0);
	}

}

void XAxis2D::drawGrid(Export *exp) const
{
	if (!vgridMaj && !vgridMin && !vzeroline)
		return;

	int    leftmgn  = xychart->leftmgn;
	int    rightmgn = xychart->rightmgn;
	int    btmmgn   = xychart->btmmgn;
	int    topmgn   = xychart->topmgn;
	int    cw       = exp->width() - leftmgn - rightmgn; /* Canvas width */
	double trXmin   = trX(vmin);
	double rw       = trX(vmax) - trXmin;                /* "Real" width */
	double inc      = vmajorinc;
	double min, max, rx, d;

	if (vmin < vmax) {
		min = vmin;
		max = vmax;
	} else {
		min = vmax;
		max = vmin;
	}
	
	for (int j = 0; (d = incrementX(min, inc, j)) <= max; ++j) {
		rx = leftmgn + cw * (trX(d) - trXmin) / rw;
		if (d == 0 && vzeroline) {
			exp->drawLine(rx, exp->height() - btmmgn, rx, topmgn,
			              vzeroThick,
			              (line_dash) vzeroDash); 
		} else if (vgridMaj && rx >= leftmgn 
		           && rx <= exp->width() - rightmgn)
		{
			exp->drawLine(rx, exp->height() - btmmgn, rx,
			              topmgn, vgridMajThick,
				      (line_dash) vgridMajDash);
		}
		if (!vgridMin)
			continue;
		for (int n = 1; n <= vnminor; n++) {
			double mininc = minincX(d, n * inc / (vnminor + 1));
			rx = leftmgn + cw * (trX(mininc) - trXmin) / rw;
			if (rx >= leftmgn && rx <= exp->width() - rightmgn) {
				exp->drawLine(rx, exp->height() - btmmgn, rx,
				              topmgn, vgridMinThick,
				              (line_dash) vgridMinDash);
			}
		}
	}
}

double XAxis2D::height(Export *exp) const
{
	double ret = 0;
	exp->setFont(Export::times, 12);
	ret += exp->getFontHeight();
	if (vticsType != tics_in)
		ret += vmajorh;
	if (vlabel != "") {
		exp->setFont(Export::times, vlabelSize);
		ret += exp->getFontHeight();
	}
	return ret;
}

double XAxis2D::rightMargin(Export *exp) const
{
	double min, max, d, d2, rx;
	double inc     = vmajorinc;
	int    leftmgn = xychart->leftmgn;
	string ticlabel;
	TeX    tex(exp);

	if (vmin < vmax) {
		min = vmin;
		max = vmax;
	} else {
		min = vmax;
		max = vmin;
	}
	max += epsilon * fabs(max);

	/* Taking last tick */
	for (int j = 0; (d2 = incrementX(min, inc, j)) < max; ++j)
		d = d2;

	if (vdataType == type_double)
		ticlabel = tex.sci2pwr(d);
	else
		ticlabel = toString(d);

	exp->setFont(Export::times, 12);
	double labelw = 0.5 * exp->stringWidth(ticlabel.c_str());
	rx = leftmgn + (exp->width() - leftmgn)
	     * (trX(d) - trX(vmin)) / (trX(vmax) - trX(vmin));
	int t = (int) (labelw + rx - exp->width());
	if (t > labelw)
		return (int) labelw;
	else if (t > 0)
		return t;

	return 0;
}

double XAxis2D::trX(double x) const
{
	switch ((int) vscale) {
	case scale_log10:
		return (x > 0)? log10(x) : log10(1.0e-50);
	case scale_ln:
		return (x > 0)? log(1.0e-50) : log(x);
	case scale_sqrt:
		return (x > 0)? sqrt(x) : -sqrt(x);
	case scale_recip:
		return 1 / x;
	default: /* scale_linear */
		return x;
	}
}

double XAxis2D::atrX(double x) const
{
	switch ((int) vscale) {
	case scale_log10:
		return pow(10, x);;
	case scale_ln:
		return pow(M_E, x);
	case scale_sqrt:
		return x * x;
	case scale_recip:
		return 1 / x;
	default: /* scale_linear */
		return x;
	}
}

double XAxis2D::incrementX(double min, double inc, int j) const
{
	switch ((int) vscale) {
	case scale_log10: 
		if (inc <= 1)
			inc = 10;
		return (min > 0)? pow(inc, (int) (floor(log10(min)
		                  / log10(inc)))) * pow(inc, j)
		                : (1.0e-50)*pow(inc,j);
	case scale_ln:
		return (min > 0)? min * pow(M_E, j)
		                : (1.0e-50) * pow(M_E, j);
	default: /* scale_linear */
		return ((int) floor(min / inc) + j) * inc;
	}
}

double XAxis2D::minincX(double x, double inc) const
{
	switch ((int) vscale) {
	case scale_log10:
		if (inc <= 1)
			inc = 10;
		return x * inc;
	case scale_ln:
		return x;
	}
	return x + inc; /* scale_linear */
}

/************* Y A X I S 2 D **************/

YAxis2D::YAxis2D(XY *_xychart): xychart(_xychart)
{
}

void YAxis2D::load(SessionReader *reader)
{
	vmin          = reader->getPropertyDouble("vmin");
	vmax          = reader->getPropertyDouble("vmax");
	vscale        = (ScaleType)reader->getPropertyInt("vscale");
	vmajorinc     = reader->getPropertyDouble("vmajorinc");
	vnminor       = reader->getPropertyInt("vnminor");
	vmajorh       = reader->getPropertyInt("vmajorh");
	vminorh       = reader->getPropertyInt("vminorh");
	vticsType     = (TicsType)reader->getPropertyInt("vticsType");
	vlabel        = reader->getProperty("vlabel");
	vlabelSize    = reader->getPropertyInt("vlabelSize");
	vlabelCenter  = reader->getPropertyBool("vlabelCenter");
	vgridMaj      = reader->getPropertyBool("vgridMaj");
	vgridMin      = reader->getPropertyBool("vgridMin");
	vgridMajThick = reader->getPropertyDouble("vgridMajThick");
	vgridMinThick = reader->getPropertyDouble("vgridMinThick");
	vgridMajDash  = reader->getPropertyInt("vgridMajDash");
	vgridMinDash  = reader->getPropertyInt("vgridMinDash");
	vzeroline     = reader->getPropertyBool("vzeroline");
	vzeroThick    = reader->getPropertyDouble("vzeroThick");
	vzeroDash     = reader->getPropertyInt("vzeroDash");
}

void YAxis2D::save(SessionWriter *writer) const
{
	writer->startElement("YAxis2D");
	writer->setProperty("vmin", vmin);
	writer->setProperty("vmax", vmax);
	writer->setProperty("vscale", vscale);
	writer->setProperty("vmajorinc", vmajorinc);
	writer->setProperty("vnminor", vnminor);
	writer->setProperty("vmajorh", vmajorh);
	writer->setProperty("vminorh", vminorh);
	writer->setProperty("vticsType", vticsType);
	writer->setProperty("vlabel", vlabel.c_str());
	writer->setProperty("vlabelSize", vlabelSize);
	writer->setProperty("vlabelCenter", vlabelCenter);
	writer->setProperty("vgridMaj", vgridMaj);
	writer->setProperty("vgridMin", vgridMin);
	writer->setProperty("vgridMajThick", vgridMajThick);
	writer->setProperty("vgridMinThick", vgridMinThick);
	writer->setProperty("vgridMajDash", vgridMajDash);
	writer->setProperty("vgridMinDash", vgridMinDash);
	writer->setProperty("vzeroline", vzeroline);
	writer->setProperty("vzeroThick", vzeroThick);
	writer->setProperty("vzeroDash", vzeroDash);
	writer->endElement();
}

void YAxis2D::repaint(Export *exp) const
{
	int    leftmgn = xychart->leftmgn;
	int    topmgn  = xychart->topmgn;
	int    btmmgn  = xychart->btmmgn;
	int    majy1   = leftmgn;
	int    majy2   = majy1;
	int    miny1   = majy1;
	int    miny2   = majy1;
	int    ch      = exp->height() - topmgn - btmmgn; /* Canvas height */
	double trYmin  = trY(vmin);
	double rh      = trY(vmax) - trY(vmin);           /* "Real" height */
	double min, max, inc, ry, d, eps;
	string ticlabel;
	int j;
	TeX tex(exp);

	switch (vticsType) {
	case tics_in:
		majy1 += vmajorh;
		miny1 += vminorh;
		break;
	case tics_out:
		majy2 -= vmajorh;
		miny2 -= vminorh;
		break;
	default: /* Tics in/out */
		majy1 += vmajorh;
		miny1 += vminorh;
		majy2 -= vmajorh;
		miny2 -= vminorh;
	}

	exp->setColor(0, 0, 0);
	exp->drawLine(leftmgn, exp->height() - btmmgn, leftmgn, topmgn);

	inc = vmajorinc;
	if (vmin < vmax) {
		min = vmin;
		max = vmax;
	} else {
		min = vmax;
		max = vmin;
	}
	eps  = epsilon * fabs(max);
	max += eps;

	for (j = 0; (d = incrementY(min, inc, j)) <= max; ++j) {
		ry = topmgn + ch * (1 - (trY(d) - trYmin) / rh);
		if (ry >= topmgn && ry <= exp->height() - btmmgn) {
			exp->drawLine(majy1, ry, majy2, ry);
			exp->setFont(Export::times, 12);
			double fd = exp->getFontDescent();
			ry += 0.5 * exp->getFontAscent();
			if (vdataType == type_double)
				ticlabel = tex.sci2pwr(d);
			else
				ticlabel = toString(d);
			tex.textInterpret(majy2
			                  - tex.textWidth(ticlabel.c_str(),
			                                  12)
			                  - fd, ry, ticlabel.c_str(), 12, 0);
		}
		for (int n = 1; n <= vnminor; n++) {
			double mininc = minincY(d, n * inc / (vnminor + 1));
			ry = topmgn + ch * (1 - (trY(mininc) - trYmin) / rh);
			if (ry >= topmgn && ry <= exp->height() - btmmgn)
				exp->drawLine(miny1, ry, miny2, ry);
		}
	}
	
	if (vlabel != "") {
		d = tex.textWidth(vlabel.c_str(), vlabelSize),
		ry = vlabelCenter? (ch + d) * 0.5 + topmgn : d + topmgn;
		exp->setFont(Export::times, vlabelSize);
		double ascent = exp->getFontAscent();
		tex.textInterpret(ascent, ry, vlabel.c_str(),
		                  (double) vlabelSize, -90);
	}
}

void YAxis2D::drawGrid(Export *exp) const
{
	if (!vgridMaj && !vgridMin && !vzeroline)
		return;

	int    leftmgn  = xychart->leftmgn;
	int    rightmgn = xychart->rightmgn;
	int    btmmgn   = xychart->btmmgn;
	int    topmgn   = xychart->topmgn;
	int    ch       = exp->height() - topmgn - btmmgn; /* Canvas height */
	double trYmin   = trY(vmin);
	double rh       = trY(vmax) - trY(vmin);           /* "Real" height */
	double inc      = vmajorinc;
	double min, max, ry, d;
	
	if (vmin < vmax) {
		min = vmin;
		max = vmax;
	} else {
		min = vmax;
		max = vmin;
	}
	for (int j = 0; (d = incrementY(min, inc, j)) <= max; ++j) {
		ry = topmgn + ch * (1 - (trY(d) - trYmin) / rh);
		if (d == 0 && vzeroline) {
			exp->drawLine(leftmgn, ry, exp->width() - rightmgn,
			              ry, vzeroThick,
			              (line_dash) vzeroDash);
		} else if (vgridMaj && ry >= topmgn
		           && ry <= exp->height() - btmmgn)
		{
			exp->drawLine(leftmgn, ry, exp->width() - rightmgn,
			              ry, vgridMajThick,
			              (line_dash) vgridMajDash);
		}
		if (!vgridMin)
			continue;
		for (int n = 1; n <= vnminor; n++) {
			double mininc = minincY(d, n * inc / (vnminor + 1));
			ry = topmgn + ch * (1 - (trY(mininc) - trYmin) / rh);
			if (ry >= topmgn && ry <= exp->height() - btmmgn) {
				exp->drawLine(leftmgn, ry, exp->width()
				              - rightmgn, ry, vgridMinThick,
				            (line_dash) vgridMinDash);
			}
		}
	}

}

double YAxis2D::width(Export *exp) const
{
	double ret = 0;
	double inc = vmajorinc;
	double min, max, d, w;
	string ticlabel;
	TeX tex(exp);

	if (vmin < vmax) {
		min = vmin;
		max = vmax;
	} else {
		min = vmax;
		max = vmin;
	}
	for (int j = 0; (d = incrementY(min, inc, j)) <= max; ++j) {
		if (d <= min || d >= max)
			continue;
		if (vdataType == type_double)
			ticlabel = tex.sci2pwr(d);
		else
			ticlabel = toString(d);
		w = tex.textWidth(ticlabel.c_str(), 12);
		if (w > ret)
			ret = w;
	}
	
	ret += exp->getFontDescent();
	
	if (vticsType != tics_in)
		ret += vmajorh;
	
	if (vlabel != "") {
		exp->setFont(Export::times, vlabelSize);
		ret += exp->getFontHeight();
	}

	return ret;
}

double YAxis2D::trY(double y) const
{
	switch ((int) vscale) {
	case scale_linear:
		return y;
	case scale_log10:
		return (y > 0)? log10(y) : log10(1.0e-50);
	case scale_ln:
		return (y > 0)? log(y) : log(1.0e-50);
	case scale_sqrt:
		return (y > 0)? sqrt(y) : -sqrt(-y);
	case scale_recip:
		return 1 / y;
	default: /* scale_linear */
		return y;
	}
}

double YAxis2D::atrY(double y) const
{
	switch ((int) vscale) {
	case scale_linear:
		return y;
	case scale_log10:
		return pow(10, y);
	case scale_ln:
		return pow(M_E, y);
	case scale_sqrt:
		return y * y;
	case scale_recip:
		return 1 / y;
	default: /* scale_linear */
		return y;
	}
}

double YAxis2D::incrementY(double min, double inc, int j) const
{
	switch ((int) vscale) {
	case scale_log10:
		if (inc <= 1)
			inc = 10;
		return (min > 0)? pow(inc, (int) (floor(log10(min)
		                  / log10(inc)))) * pow(inc, j)
		                : (1.0e-50) * pow(inc, j);
	case scale_ln:
		return (min > 0)? min * pow(M_E, j)
		                : (1.0e-50) * pow(M_E, j);
	default: /* scale_linear */
		return ((int) floor(min / inc)) * inc + j * inc;
	}
}

double YAxis2D::minincY(double y, double inc) const
{
	switch ((int) vscale) {
	case scale_log10:
		if (inc <= 1)
			inc = 10;	
		return y * inc;
	case scale_ln:
		return y;
	default: /* scale_linear */
		return y + inc;
	}
}

