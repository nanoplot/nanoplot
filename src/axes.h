/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2008, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AXES_H
#define AXES_H

#include <sstream>
#include <string>
#include "chartelement.h"
#include "export.h"
#include "nplotnamespace.h"
#include "sessionreader.h"
#include "sessionwriter.h"
#include "xy.h"

using namespace std;
using namespace nplot;

class XY;

/**
 *   Base class for all the axes.
 */
class AbstractAxis: public ChartElement
{
public:
	/**  Axis scale type enum. */
	enum ScaleType {
		scale_linear = 0,
		scale_log10,
		scale_ln,
		scale_sqrt,
		scale_recip
	};

	/**  Axis tics type enum. */
	enum TicsType {
		tics_in = 0,
		tics_out,
		tics_inout
	};

	/**  Small epsilon to evade rounding down in double */
	static const double epsilon = 1e-5;

	/**  A constructor. */
	AbstractAxis();

	/**  A destructor. */
	virtual ~AbstractAxis() {};

	string toString(double d) const;
	/**
	 *   Loads the state from the session file.
	 *   \param reader session reader
	 */
	virtual void load(SessionReader *reader)       = 0;

	virtual void save(SessionWriter *writer) const = 0;
	virtual void repaint(Export *exp) const        = 0;
	virtual elem_type type() const                 = 0;
	
	/**
	 *   Draws a grid.
	 *   \param exp export to draw with
	 */
	virtual void drawGrid(Export *exp) const       = 0;
	
	/* Setters */
	virtual void      setMin(double v)          { vmin          = v; }
	virtual void      setMax(double v)          { vmax          = v; }
	virtual void      setScale(ScaleType v)     { vscale        = v; }
	virtual void      setMajorInc(double v)     { vmajorinc     = v; }
	virtual void      setNMinor(int v)          { vnminor       = v; }
	virtual void      setMajorH(int v)          { vmajorh       = v; }
	virtual void      setMinorH(int v)          { vminorh       = v; }
	virtual void      setTicsType(TicsType v)   { vticsType     = v; }
	virtual void      setLabel(string v)        { vlabel        = v; }
	virtual void      setLabelSize(int v)       { vlabelSize    = v; }
	virtual void      setLabelCenter(bool v)    { vlabelCenter  = v; }
	virtual void      setGridMajEnabled(bool v) { vgridMaj      = v; }
	virtual void      setGridMinEnabled(bool v) { vgridMin      = v; }
	virtual void      setZeroEnabled(bool v)    { vzeroline     = v; }
	virtual void      setGridMajThick(double v) { vgridMajThick = v; }
	virtual void      setGridMinThick(double v) { vgridMinThick = v; }
	virtual void      setZeroThick(double v)    { vzeroThick    = v; }
	virtual void      setGridMajDash(int v)     { vgridMajDash  = v; }
	virtual void      setGridMinDash(int v)     { vgridMinDash  = v; }
	virtual void      setZeroDash(int v)        { vzeroDash     = v; }
	virtual void      setDataType(int v)        { vdataType     = v; }
	
	/* Getters */
	virtual double    min() const               { return vmin; }
	virtual double    max() const               { return vmax; }
	virtual ScaleType scale() const             { return vscale; }
	virtual double    majorInc() const          { return vmajorinc; }
	virtual int       nMinor() const            { return vnminor; }
	virtual int       majorH() const            { return vmajorh; }
	virtual int       minorH() const            { return vminorh; }
	virtual TicsType  ticsType() const          { return vticsType; }
	virtual string    label() const             { return vlabel; }
	virtual int       labelSize() const         { return vlabelSize; }
	virtual bool      labelCenter() const       { return vlabelCenter; }
	virtual bool      gridMajEnabled() const    { return vgridMaj; }
	virtual bool      gridMinEnabled() const    { return vgridMin; }
	virtual bool      zeroEnabled() const       { return vzeroline; }
	virtual double    gridMajThick() const      { return vgridMajThick; }
	virtual double    gridMinThick() const      { return vgridMinThick; }
	virtual double    zeroThick() const         { return vzeroThick; }
	virtual int       gridMajDash() const       { return vgridMajDash; }
	virtual int       gridMinDash() const       { return vgridMinDash; }
	virtual int       zeroDash() const          { return vzeroDash; }
	virtual int       dataType() const          { return vdataType; }
protected:
	double    vmin;          /**< Lower bound */
	double    vmax;          /**< Higher bound */
	ScaleType vscale;        /**< Axis scale type */
	double    vmajorinc;     /**< Axis major increment */
	int       vnminor;       /**< Number of minor tics */
	int       vmajorh;       /**< Major tics height */
	int       vminorh;       /**< Minor tics height */
	TicsType  vticsType;     /**< Axis tics type */
	string    vlabel;        /**< Axis label */
	int       vlabelSize;    /**< Axis label size */
	bool      vlabelCenter;  /**< Controls label centering */
	bool      vgridMaj;      /**< Controls major grid */
	bool      vgridMin;      /**< Controls minor grid */
	bool      vzeroline;     /**< Controls zero line */
	double    vgridMajThick; /**< Major grid thickness */
	double    vgridMinThick; /**< Minor grid thickness */
	double    vzeroThick;    /**< Zero line thickness */
	int       vgridMajDash;  /**< Major grid dash pattern */
	int       vgridMinDash;  /**< Minor grid dash pattern */
	int       vzeroDash;     /**< Zero line dash */
	int       vdataType;
};

/**
 *   2D X axis class.
 */
class XAxis2D: public AbstractAxis
{
public:
	/**  A constructor. */
	XAxis2D(XY *xychart);
	
	/**  A destructor. */
	~XAxis2D() {};
	void load(SessionReader *reader);
	void save(SessionWriter *writer) const;
	void repaint(Export *exp) const;
	elem_type type() const  { return elem_unknown; };
	void drawGrid(Export *exp) const;

	/**
	 *   Gets the height of the box needed to render the axis.
	 *   \param exp export to calculate with
	 *   \return height of the axis box
	 */
	double height(Export *exp) const;

	/**
	 *   Calculates needed right margin for tick labels.
	 *   \param exp export to calculate with
	 *   \return right margin
	 */
	double rightMargin(Export *exp) const;
	
	/**
	 *   Transform X according to X scale
	 *   \param x value to be transformed
	 *   \return transformed X
	 */
	double trX(double x) const;

	/**
	 *   Anti-transform X according to X scale
	 *   Do inverse transformation of X.
	 *   \param x value to be transformed
	 *   \return transformed X
	 */
	double atrX(double x) const;

	/**
	 *   Increment X (major) on a given axis scale.
	 *   \param min minimum
	 *   \param inc value of the increment
	 *   \param j increment number
	 *   \return incremented X value
	 */
	double incrementX(double min, double inc, int j) const;
	
	/**
	 *   Increment X (major) on a given axis scale.
	 *   \param x value of the last major thick
	 *   \param inc value of the increment
	 *   \return incremented X value
	 */
	double minincX(double x, double inc) const;
private:
	XY *xychart;             /**< Parent XY widget */
};

/**
 *   2D Y axis.
 */
class YAxis2D: public AbstractAxis
{
public:
	/**  A constructor. */
	YAxis2D(XY *xychart);
	
	/**  A destructor. */
	~YAxis2D() {};
	void load(SessionReader *reader);
	void save(SessionWriter *writer) const;
	void repaint(Export *exp) const;
	elem_type type() const  { return elem_unknown; };
	void drawGrid(Export *exp) const;
	
	/**
	 *   Gets the width of the box needed to render the axis.
	 *   \param exp export to calculate with
	 *   \return width of the axis box
	 */
	double width(Export *exp) const;
	
	/**
	 *   Transform Y according to Y scale
	 *   \param y value to be transformed
	 *   \return transformed Y
	 */
	double trY(double y) const;
	
	/**
	 *   Anti-transform Y according to Y scale
	 *   Do inverse transformation of Y.
	 *   \param y value to be transformed
	 *   \return transformed Y
	 */
	double atrY(double y) const;
	
	/**
	 *   Increment Y (major) on a given axis scale.
	 *   \param min minimum
	 *   \param inc value of the increment
	 *   \param j increment number
	 *   \return incremented Y value
	 */
	double incrementY(double min, double inc, int j) const;
	
	/**
	 *   Increment Y (major) on a given axis scale.
	 *   \param y value of the last major thick
	 *   \param inc value of the increment
	 *   \return incremented Y value
	 */
	double minincY(double y, double inc) const;
private:
	XY *xychart;             /**< Parent XY widget */
};

#endif /* AXES_H */
