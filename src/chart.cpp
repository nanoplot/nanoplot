/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2010, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "options.h"
#include <gettext.h>
#include <glib.h>
#include <sstream>
#include <string.h>
#include "chart.h"
#include "config.h"
#include "nplotnamespace.h"
#include "dialogs.h"
#include "images/chart.xpm"

#define _(String) (gettext(String))

using namespace std;

Chart::Chart(QWidget *parent): WSWindow(parent)
{
	Init();
	gchar *title = g_strdup_printf(_("Chart %d"), counter++);
	setWindowTitle(title);
	chart()->setName(title);
	g_free(title);
}

Chart::Chart(SessionReader *reader, QWidget *parent): WSWindow(parent)
{
	Init();

	setWindowTitle(reader->getProperty("title"));
	chart()->setName((const char*) windowTitle().toAscii());
	hidden = reader->getPropertyBool("hidden");

	reader->processChildren();
	
	for (const char *name = reader->nextNode(); name;
	     name = reader->nextNode()) {
		if (strcmp(name, "XY") == 0)
			chart()->load(reader);
	}
}

void Chart::Init()
{
	setMinimumWidth(400);
	setMinimumHeight(300);
	setWindowIcon(QIcon(chart_xpm));

	setBackgroundRole(QPalette::Light);

	QWidget *bgwidget = new QWidget(this);
	widget = new ChartWidget(this);
	dreader = new DataReader(widget, this);

	connect(widget, SIGNAL(xyChanged(double, double)), dreader, 
		SLOT(setXY(double, double)));

	QVBoxLayout *vbox = new QVBoxLayout;
	vbox->addWidget(dreader);
	vbox->addWidget(widget);
	vbox->setMargin(0);
	bgwidget->setLayout(vbox);
	
	setCentralWidget(bgwidget);
}

int Chart::type() const
{
	return nplot::wdw_chart;
}

void Chart::save(SessionWriter *writer) const
{
	writer->startElement("Chart");
	writer->setProperty("title", (const char*) windowTitle().toAscii());
	writer->setProperty("hidden", isHidden());
	chart()->save(writer);
	writer->endElement();
}

XY* Chart::chart() const
{
	return widget->chart();
}

int Chart::counter = 1;

void Chart::addDataXY(Array data, int type,
                      bool symbols, bool line)
{
	int n = chart()->numElements() + 1;
	gchar *name = g_strdup_printf(_("data%d"), n);
	chart()->addDataXY(name, data, type, symbols, line);
	g_free(name);
	chart()->xAxis->setDataType(data.columnType(0));
	chart()->yAxis->setDataType(data.columnType(1));
	chart()->fitZoom();
}

void Chart::hideDataReader()
{
	dreader->hide();
}

void Chart::setDataReaderMode(int m)
{
	dreader->setMode(m);
}

int Chart::dataReaderMode() const
{
	return dreader->getMode();
}

