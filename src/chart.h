/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CHART_H
#define CHART_H

#include <QtGui>
#include <cmath>
#include <sstream>
#include <vector>
#include <string>
#include "xy.h"
#include "array.h"
#include "chartwidget.h"
#include "datareader.h"
#include "sessionreader.h"
#include "wswindow.h"

using namespace std;

class ChartWidget;
class DataReader;
class XY;

/** 
 *   Chart window.
 */
class Chart: public WSWindow 
{
Q_OBJECT
public:
	/**  
	 *   A constructor.
	 *   Creates an empty chart window
	 *   \param parent Pointer to parent widget
	 */
	Chart(QWidget *parent = 0);

	/**
	 *   A constructor creating chart using information from a session
	 *   file.
	 *   \param reader session reader
	 *   \param parent Pointer to parent widget
	 */
	Chart(SessionReader *reader, QWidget *parent = 0);

	/**  A destructor */
	~Chart() {};
	int type() const;
		
	/**
	 *   Save chart.
	 *   Method used for saving information to project file.
	 *   \param writer session writer
	 */
	void save(SessionWriter *writer) const;
	
	/**  Returns a pointer to ChartWidget */
	ChartWidget* chartWidget() const { return widget; };

	/**  Returns a pointer to XY object */
	XY* chart() const;
	static int counter;           /**< Holds the counter of notepads */
public slots:

	/** 
	 *   Adds XY data series to the chart.
	 *   Calls subsequently XY::addDataXY() and XY::fitZoom() methods 
	 *   of the XY object
	 *   \param data Array of data (also vectors)
	 *   \param type Type of element to add (nplot::elem_type)
	 *   \param symbols If \c true, symbols will be enabled, otherwise
	 *   disabled
	 *   \param lines If \c true, connecting with lines will be enabled,
	 *   otherwise disabled
	 *   \sa XY
	 */
	void addDataXY(Array data, int type, 
	               bool symbols = false, bool lines = false);

	void hideDataReader();        /**< Hides data reader \sa DataReader */
	
	/**
	 *   Sets data reader mode.
	 *   Calls DataReader::setMode() method.
	 *   \param mode Mode to be set; 0 - hidden, 1 - coord reading, 
	 *   2 - data reading 
	 *   \sa DataReader 
	 */
	void setDataReaderMode(int mode);
	int dataReaderMode() const;   /**< Returns data reader mode */
private:
	void Init();                  /**< Initializes window */
	DataReader *dreader;      /**< Holds a pointer to DataReader widget */
	ChartWidget *widget;          /**< Holds a pointer to XY widget */
};

#endif /* CHART_H */
