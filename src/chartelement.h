/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2008, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CHARTELEMENT_H
#define CHARTELEMENT_H

#include <QtGui>
#include <string>
#include "export.h"
#include "sessionwriter.h"
#include "nplotnamespace.h"

using namespace std;
using namespace nplot;

/**
 *   Base class for all chart elements.
 */
class ChartElement
{
public:
	/**  A constructor. */
	ChartElement();
	
	/** A destructor. */
	virtual ~ChartElement() {};

	/**
	 *   Save element.
	 *   Method used for saving element to a session file.
	 *   \param reader session writer
	 */
	virtual void save(SessionWriter *writer) const = 0;

	/**
	 *   Repaint element.
	 *   \param exp pointer to Export used to draw the element
	 */
	virtual void repaint(Export *exp = 0) const = 0;

	/**
	 *   Element type.
	 *   \return nplot::elem_type containing type of the element
	 */
	virtual elem_type type() const = 0;

	/**
	 *   Sets elements visibility.
	 *   \param b hides element if \c true, otherwise sets it as visible
	 */
	void setHidden(bool b)  { hidden = b; };

	/**
	 *   Checks visibility of the element.
	 *   \return \c true if element is set to be visible, \c false otherwise
	 */
	bool isHidden() const   { return hidden; };

	string name;                 /**< Name of the element */
protected:
	bool hidden;                 /**< Visibility flag */
};

#endif /* CHARTELEMENT_H */
