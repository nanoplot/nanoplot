/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2010, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "options.h"
#include <gettext.h>
#include <glib.h>
#include "chartsettings.h"
#include "napp.h"
#include "nplotnamespace.h"
#include "tex.h"
#include "texsymbols.h"
#include "images/symb1.xpm"
#include "images/symb2.xpm"
#include "images/symb3.xpm"
#include "images/symb4.xpm"
#include "images/symb5.xpm"
#include "images/symb6.xpm"
#include "images/symb7.xpm"
#include "images/symb8.xpm"

#define _(String) (gettext(String))

using namespace std;
using namespace nplot;

ChartSettings::ChartSettings(ChartWidget *widget)
 : QDialog(MainWindow::pointer)
{
	currentAxis  = 0;
	tabWidget    = new QTabWidget;
	axesTab      = new AxesTab(widget, this);
	ticsTab      = new TicsTab(widget, this);
	gridTab      = new GridTab(widget, this);
	legendTab    = new LegendTab(widget, this);
	chartElemTab = new ChartElementsTab(widget, this);

	tabWidget->addTab(axesTab, _("Axes"));
	tabWidget->addTab(ticsTab, _("Tics"));
	tabWidget->addTab(gridTab, _("Grid"));
	tabWidget->addTab(legendTab, _("Legend"));
	tabWidget->addTab(chartElemTab, _("Chart elements"));

	QPushButton *okButton = new QPushButton(_("&OK"), this);
	okButton->setDefault(true);
	QPushButton *applyButton = new QPushButton(_("&Apply"), this);
	QPushButton *cancelButton = new QPushButton(_("&Cancel"), this);
	
	connect(okButton, SIGNAL(clicked()), this, SLOT(ok()));
	connect(applyButton, SIGNAL(clicked()), this, SLOT(apply()));
	connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));
	
	QHBoxLayout *hlayout = new QHBoxLayout;
	hlayout->addStretch(1);
	hlayout->addWidget(okButton);
	hlayout->addWidget(applyButton);
	hlayout->addWidget(cancelButton);

	QVBoxLayout *mainLayout = new QVBoxLayout;
	mainLayout->addWidget(tabWidget);
	mainLayout->addLayout(hlayout);;
	setLayout(mainLayout);

	setMinimumHeight(480);
	setWindowTitle(_("Chart settings"));

	connect(tabWidget, SIGNAL(currentChanged(int)), this,
	        SLOT(tabChanged(int)));
}

bool ChartSettings::apply()
{
	
	switch (tabWidget->currentIndex()) {
	case 0:
		return axesTab->apply();
	case 1:
		return ticsTab->apply();
	case 2:	
		return gridTab->apply();
	case 3:	
		return legendTab->apply();
	case 4:
		return chartElemTab->apply();
	default:
		return false;
	}
}

void ChartSettings::ok()
{
	if (this->apply())
		this->accept();
}

void ChartSettings::update()
{
	chartElemTab->update();
	legendTab->update();
	gridTab->update();
	ticsTab->update();
	axesTab->update();
}

void ChartSettings::tabChanged(int t)
{
	switch (t) {
	case 0:
		axesTab->switchAxis(currentAxis);
		break;
	case 1:
		ticsTab->switchAxis(currentAxis);
		break;
	case 2:
		gridTab->switchAxis(currentAxis);
		break;
	}
}

AxesTab::AxesTab(ChartWidget *_widget, ChartSettings *parent)
 : QWidget(parent), widget(_widget)
{
	this->parent = parent;
	
	QVBoxLayout *mainLayout       = new QVBoxLayout;
	QVBoxLayout *labelBoxLayout   = new QVBoxLayout;
	QHBoxLayout *labelLayout      = new QHBoxLayout;
	QHBoxLayout *labelToolsLayout = new QHBoxLayout;
	QGridLayout *scaleLayout      = new QGridLayout;
	QHBoxLayout *dataLayout       = new QHBoxLayout;

	comAxis = new QComboBox;
	comAxis->addItem(_("X axis"));
	comAxis->addItem(_("Y axis"));

	/* TRANSLATORS: Axis label */
	QGroupBox *labelBox = new QGroupBox(_("Label"));
	labelEdit = new QLineEdit;

	/* TRANSLATORS: Size of the axis label */
	QLabel *sizeLabel = new QLabel(_("Size:"));
	labelSpin = new QSpinBox;
	labelSpin->setRange(8,32);
	labelSpin->setSingleStep(1);
	TeXButton *grLett = new TeXButton("\\alpha");
	SymbolPopup *grPopup = new SymbolPopup(0, 30, labelEdit, grLett);
	grLett->setMenu(grPopup);
	TeXButton *ugrLett = new TeXButton("\\Gamma");
	SymbolPopup *ugrPopup = new SymbolPopup(30, 41, labelEdit, ugrLett);
	ugrLett->setMenu(ugrPopup);
	TeXButton *symbBtn = new TeXButton("\\hslash");
	SymbolPopup *symbPopup = new SymbolPopup(41, 52, labelEdit, symbBtn);
	symbBtn->setMenu(symbPopup);
	TeXButton *supBtn = new TeXButton("x^2", "^{}", labelEdit);
	TeXButton *subBtn = new TeXButton("x_2", "_{}", labelEdit);
	alignCombo = new QComboBox;
	/* TRANSLATORS: Centering axis label */
	alignCombo->addItem(_("Center"));
	/* TRANSLATORS: Right justification of axis label */
	alignCombo->addItem(_("Right"));

	/* TRANSLATOR: As in "logarithmic scale" */
	QGroupBox *scaleBox = new QGroupBox(_("Scale"));
	QLabel *typeLabel = new QLabel(_("Type:"));
	typeCombo = new QComboBox;
	/* TRANSLATORS: Adjective; linear scale */
	typeCombo->addItem(_("linear"));
	/* TRANSLATORS: Adjective; log base 10 scale */
	typeCombo->addItem(_("log10"));
	/* TRANSLATORS: Adjective; natural logarithmic scale */
	typeCombo->addItem(_("ln"));
	/* TRANSLATORS: Adjective; sqrt scale */
	typeCombo->addItem(_("sqrt"));
	/* TRANSLATORS: Adjective; reciprocal (1/x) scale */
	typeCombo->addItem(_("reciprocal"));
	QLabel *minLabel = new QLabel(_("Minimum:"));
	minEdit = new QLineEdit;
	minEdit->setSizePolicy(typeCombo->sizePolicy());
	QLabel *maxLabel = new QLabel(_("Maximum:"));
	maxEdit = new QLineEdit;
	maxEdit->setSizePolicy(typeCombo->sizePolicy());

	QLabel *dataLabel = new QLabel(_("Data type:"));
	dataCombo = new QComboBox;
	vector<datatype_info> types = nApp::instance()->types(); 
	for (unsigned int i = 0; i < types.size(); ++i) {
		datatype_info info = types[i];
		dataCombo->addItem(info.name);
	}

	labelLayout->addWidget(labelEdit);
	labelLayout->addWidget(sizeLabel);
	labelLayout->addWidget(labelSpin);
	labelToolsLayout->setSpacing(1);
	labelToolsLayout->addWidget(grLett);
	labelToolsLayout->addWidget(ugrLett);
	labelToolsLayout->addWidget(symbBtn);
	labelToolsLayout->addWidget(supBtn);
	labelToolsLayout->addWidget(subBtn);
	labelToolsLayout->addWidget(alignCombo);
	labelToolsLayout->addStretch(1);
	labelBoxLayout->addLayout(labelLayout);
	labelBoxLayout->addLayout(labelToolsLayout);
	labelBox->setLayout(labelBoxLayout);

	scaleLayout->addWidget(typeLabel, 0, 0);
	scaleLayout->addWidget(typeCombo, 0, 1);
	scaleLayout->addWidget(minLabel, 1, 0);
	scaleLayout->addWidget(minEdit, 1, 1);
	scaleLayout->addWidget(maxLabel, 2, 0);
	scaleLayout->addWidget(maxEdit, 2, 1);
	scaleBox->setLayout(scaleLayout);
	
	dataLayout->addWidget(dataLabel);
	dataLayout->addWidget(dataCombo);

	mainLayout->addWidget(comAxis);
	mainLayout->addWidget(labelBox);
	mainLayout->addWidget(scaleBox);
	mainLayout->addLayout(dataLayout);
	mainLayout->addStretch(1);
	setLayout(mainLayout);
	
	connect(comAxis, SIGNAL(currentIndexChanged(int)), this,
	        SLOT(switchAxis(int)));
}

void AxesTab::switchAxis(int axis)
{
	QString tmpstr;
	XY *chart = widget->chart();

	comAxis->setCurrentIndex(axis);
	if (axis == 0) {
		labelEdit->setText(chart->xAxis->label().c_str());
		labelSpin->setValue(chart->xAxis->labelSize());
		alignCombo->setCurrentIndex(chart->xAxis->labelCenter()? 0
		                                                       : 1);
		typeCombo->setCurrentIndex(chart->xAxis->scale());
		tmpstr = QString("%1").arg(chart->xAxis->min());
		minEdit->setText(tmpstr);
		tmpstr = QString("%1").arg(chart->xAxis->max());
		maxEdit->setText(tmpstr);
		dataCombo->setCurrentIndex(chart->xAxis->dataType());
		parent->currentAxis = 0;
	} else {
		labelEdit->setText(chart->yAxis->label().c_str());
		labelSpin->setValue(chart->yAxis->labelSize());
		alignCombo->setCurrentIndex(chart->yAxis->labelCenter()? 0
		                                                       : 1);
		typeCombo->setCurrentIndex(chart->yAxis->scale());
		tmpstr = QString("%1").arg(chart->yAxis->min());
		minEdit->setText(tmpstr);
		tmpstr = QString("%1").arg(chart->yAxis->max());
		maxEdit->setText(tmpstr);
		dataCombo->setCurrentIndex(chart->yAxis->dataType());
		parent->currentAxis = 1;
	}
}

void AxesTab::update()
{
	switchAxis(parent->currentAxis);
}

bool AxesTab::apply()
{
	double setmin = strtod((const char*) minEdit->text().toAscii(), NULL);
	double setmax = strtod((const char*) maxEdit->text().toAscii(), NULL);
	XY *chart = widget->chart();

	if (setmax == setmin) {
		QMessageBox::information(this, _("Information"), 
				_("Maximum must not be equal to minimum!"));
		return false;
	}

	if (comAxis->currentIndex() == 0) {
		chart->xAxis->setLabel((const char*)
		                       labelEdit->text().toUtf8());
		chart->xAxis->setLabelSize(labelSpin->value());
		chart->xAxis->setLabelCenter(alignCombo->currentIndex() == 0);
		chart->xAxis->setScale((AbstractAxis::ScaleType)
		                       typeCombo->currentIndex());
		chart->xAxis->setMin(setmin);
		chart->xAxis->setMax(setmax);
		chart->xAxis->setDataType(dataCombo->currentIndex());
	} else {
		chart->yAxis->setLabel((const char*)
		                       labelEdit->text().toUtf8());
		chart->yAxis->setLabelSize(labelSpin->value());
		chart->yAxis->setLabelCenter(alignCombo->currentIndex() == 0);
		chart->yAxis->setScale((AbstractAxis::ScaleType)
		                       typeCombo->currentIndex());
		chart->yAxis->setMin(setmin);
		chart->yAxis->setMax(setmax);
		chart->yAxis->setDataType(dataCombo->currentIndex());
	}
	chart->calcMargins();
	chart->setModified();
	widget->update();
	
	return true;
}

TicsTab::TicsTab(ChartWidget *_widget, ChartSettings *parent)
 : QWidget(parent), widget(_widget)
{
	this->parent = parent;
	
	QVBoxLayout *mainLayout    = new QVBoxLayout;
	QGridLayout *majTicsLayout = new QGridLayout;
	QGridLayout *minTicsLayout = new QGridLayout;
	QHBoxLayout *typeLayout    = new QHBoxLayout;

	comAxis = new QComboBox;
	comAxis->addItem(_("X axis"));
	comAxis->addItem(_("Y axis"));

	QGroupBox *majTicsBox = new QGroupBox(_("Main tics"));
	QLabel *majorLabel = new QLabel(_("Increment:"));
	majorEdit = new QLineEdit;
	QLabel *majorhLabel = new QLabel(_("Height:"));
	majorhSpin = new QSpinBox;
	majorhSpin->setRange(0, 20);
	majorhSpin->setSingleStep(1);
	majorEdit->setSizePolicy(majorhSpin->sizePolicy());

	QGroupBox *minTicsBox = new QGroupBox(_("Minor tics"));

	/* TRANSLATORS: Number of minor tics */
	QLabel *minorLabel = new QLabel(_("Number:"));
	minorSpin = new QSpinBox;
	minorSpin->setRange(0, 20);
	minorSpin->setSingleStep(1);
	QLabel *minorhLabel = new QLabel(_("Height:"));
	minorhSpin = new QSpinBox;
	minorhSpin->setRange(0, 10);
	minorhSpin->setSingleStep(1);

	QLabel *typeLabel = new QLabel(_("Type:"));
	typeCombo = new QComboBox;
	/* Axis tics type: Inward */
	typeCombo->addItem(_("In"));
	/* Axis tics type: Outward*/
	typeCombo->addItem(_("Out"));
	/* Axis tics type: Inward/outward */
	typeCombo->addItem(_("In/Out"));

	majTicsLayout->addWidget(majorLabel, 0, 0);
	majTicsLayout->addWidget(majorEdit, 0, 1);
	majTicsLayout->addWidget(majorhLabel, 1, 0);
	majTicsLayout->addWidget(majorhSpin, 1, 1);
	majTicsBox->setLayout(majTicsLayout);
	
	minTicsLayout->addWidget(minorLabel, 0, 0);
	minTicsLayout->addWidget(minorSpin, 0, 1);
	minTicsLayout->addWidget(minorhLabel, 1, 0);
	minTicsLayout->addWidget(minorhSpin, 1, 1);
	minTicsBox->setLayout(minTicsLayout);

	typeLayout->addWidget(typeLabel);
	typeLayout->addWidget(typeCombo);

	mainLayout->addWidget(comAxis);
	mainLayout->addWidget(majTicsBox);
	mainLayout->addWidget(minTicsBox);
	mainLayout->addLayout(typeLayout);
	mainLayout->addStretch(1);
	setLayout(mainLayout);
	
	connect(comAxis, SIGNAL(currentIndexChanged(int)), this,
	        SLOT(switchAxis(int)));
}

void TicsTab::switchAxis(int axis)
{
	XY *chart = widget->chart();
	gchar *tmpstr;

	comAxis->setCurrentIndex(axis);
	if (axis == 0) {
		tmpstr = g_strdup_printf("%lg", chart->xAxis->majorInc());
		majorEdit->setText(tmpstr);
		g_free(tmpstr);
		majorhSpin->setValue(chart->xAxis->majorH());
		minorSpin->setValue(chart->xAxis->nMinor());
		minorhSpin->setValue(chart->xAxis->minorH());
		typeCombo->setCurrentIndex(chart->xAxis->ticsType());
		parent->currentAxis = 0;
	} else {
		tmpstr = g_strdup_printf("%lg", chart->yAxis->majorInc());
		majorEdit->setText(tmpstr);
		g_free(tmpstr);
		majorhSpin->setValue(chart->yAxis->majorH());
		minorSpin->setValue(chart->yAxis->nMinor());
		minorhSpin->setValue(chart->yAxis->minorH());
		typeCombo->setCurrentIndex(chart->yAxis->ticsType());
		parent->currentAxis = 1;
	}
}

void TicsTab::update()
{
	switchAxis(parent->currentAxis);
}

bool TicsTab::apply()
{
	double setinc = strtod((const char*) majorEdit->text().toAscii(),
	                       NULL);
	XY *chart = widget->chart();

	if (setinc <= 0) {
		QMessageBox::information(this, _("Information"), 
				      _("Main tick must be greater than 0!"));
		return false;
	}

	if (comAxis->currentIndex() == 0) {
		chart->xAxis->setMajorInc(setinc);
		chart->xAxis->setMajorH(majorhSpin->value());
		chart->xAxis->setNMinor(minorSpin->value());
		chart->xAxis->setMinorH(minorhSpin->value());
		chart->xAxis->setTicsType((AbstractAxis::TicsType)
		                          typeCombo->currentIndex());
	} else {
		chart->yAxis->setMajorInc(setinc);
		chart->yAxis->setMajorH(majorhSpin->value());
		chart->yAxis->setNMinor(minorSpin->value());
		chart->yAxis->setMinorH(minorhSpin->value());
		chart->yAxis->setTicsType((AbstractAxis::TicsType)
		                          typeCombo->currentIndex());
	}
	chart->calcMargins();
	chart->setModified();
	widget->update();
	
	return true;
}

GridTab::GridTab(ChartWidget *_widget, ChartSettings *parent)
 : QWidget(parent), widget(_widget)
{
	this->parent = parent;

	QVBoxLayout *mainLayout  = new QVBoxLayout;
	QGridLayout *majorLayout = new QGridLayout;
	QGridLayout *minorLayout = new QGridLayout;
	QGridLayout *zeroLayout  = new QGridLayout;

	comAxis = new QComboBox;
	comAxis->addItem(_("X axis"));
	comAxis->addItem(_("Y axis"));

	majorBox = new QGroupBox(_("Main grid lines"));
	majorBox->setCheckable(true);
	majorBox->setChecked(false);
	QLabel *majorThLabel = new QLabel(_("Thickness:"));
	majorThSpin = new QDoubleSpinBox;
	majorThSpin->setRange(0, 5);
	majorThSpin->setSingleStep(0.1);
	QLabel *majorDashLabel = new QLabel(_("Dash pattern:"));
	majorDashCom = new QComboBox;
	majorDashCom->addItem("---------------");
	majorDashCom->addItem("-- -- -- -- -- -");
	majorDashCom->addItem("\xc2\xb7 \xc2\xb7 \xc2\xb7 \xc2\xb7 \xc2\xb7 "
	                      "\xc2\xb7 \xc2\xb7 \xc2\xb7 \xc2\xb7 \xc2\xb7");
	majorDashCom->addItem("-- \xc2\xb7 -- \xc2\xb7 -- \xc2\xb7 -");
	majorDashCom->addItem("-- \xc2\xb7 \xc2\xb7 -- \xc2\xb7 \xc2\xb7 --");

	minorBox = new QGroupBox(_("Minor grid lines"));
	minorBox->setCheckable(true);
	minorBox->setChecked(false);
	QLabel *minorThLabel = new QLabel(_("Thickness:"));
	minorThSpin = new QDoubleSpinBox;
	minorThSpin->setRange(0, 5);
	minorThSpin->setSingleStep(0.1);
	QLabel *minorDashLabel = new QLabel(_("Dash pattern:"));
	minorDashCom = new QComboBox;
	minorDashCom->addItem("---------------");
	minorDashCom->addItem("-- -- -- -- -- -");
	minorDashCom->addItem("\xc2\xb7 \xc2\xb7 \xc2\xb7 \xc2\xb7 \xc2\xb7 "
	                      "\xc2\xb7 \xc2\xb7 \xc2\xb7 \xc2\xb7 \xc2\xb7");
	minorDashCom->addItem("-- \xc2\xb7 -- \xc2\xb7 -- \xc2\xb7 -");
	minorDashCom->addItem("-- \xc2\xb7 \xc2\xb7 -- \xc2\xb7 \xc2\xb7 --");
	
	zeroBox = new QGroupBox(_("Zero line"));
	zeroBox->setCheckable(true);
	zeroBox->setChecked(false);
	QLabel *zeroThLabel = new QLabel(_("Thickness:"));
	zeroThSpin = new QDoubleSpinBox;
	zeroThSpin->setRange(0, 5);
	zeroThSpin->setSingleStep(0.1);
	QLabel *zeroDashLabel = new QLabel(_("Dash pattern:"));
	zeroDashCom = new QComboBox;
	zeroDashCom->addItem("---------------");
	zeroDashCom->addItem("-- -- -- -- -- -");
	zeroDashCom->addItem("\xc2\xb7 \xc2\xb7 \xc2\xb7 \xc2\xb7 \xc2\xb7 "
	                      "\xc2\xb7 \xc2\xb7 \xc2\xb7 \xc2\xb7 \xc2\xb7");
	zeroDashCom->addItem("-- \xc2\xb7 -- \xc2\xb7 -- \xc2\xb7 -");
	zeroDashCom->addItem("-- \xc2\xb7 \xc2\xb7 -- \xc2\xb7 \xc2\xb7 --");
	
	majorLayout->addWidget(majorThLabel, 1, 0);
	majorLayout->addWidget(majorThSpin, 1, 1);
	majorLayout->addWidget(majorDashLabel, 2, 0);
	majorLayout->addWidget(majorDashCom, 2, 1);
	majorLayout->setRowStretch(3, 1);
	majorBox->setLayout(majorLayout);

	minorLayout->addWidget(minorThLabel, 1, 0);
	minorLayout->addWidget(minorThSpin, 1, 1);
	minorLayout->addWidget(minorDashLabel, 2, 0);
	minorLayout->addWidget(minorDashCom, 2, 1);
	minorLayout->setRowStretch(3, 1);
	minorBox->setLayout(minorLayout);

	zeroLayout->addWidget(zeroThLabel, 1, 0);
	zeroLayout->addWidget(zeroThSpin, 1, 1);
	zeroLayout->addWidget(zeroDashLabel, 2, 0);
	zeroLayout->addWidget(zeroDashCom, 2, 1);
	zeroLayout->setRowStretch(3, 1);
	zeroBox->setLayout(zeroLayout);

	mainLayout->addWidget(comAxis);
	mainLayout->addWidget(majorBox);
	mainLayout->addWidget(minorBox);
	mainLayout->addWidget(zeroBox);
	mainLayout->addStretch(1);
	setLayout(mainLayout);
	
	connect(comAxis, SIGNAL(currentIndexChanged(int)), this,
	        SLOT(switchAxis(int)));
	switchAxis(0);
}

void GridTab::switchAxis(int axis)
{
	XY *chart = widget->chart();

	comAxis->setCurrentIndex(axis);
	if (axis == 0) {
		majorThSpin->setValue(chart->xAxis->gridMajThick());
		minorThSpin->setValue(chart->xAxis->gridMinThick());
		zeroThSpin->setValue(chart->xAxis->zeroThick());
		majorDashCom->setCurrentIndex(chart->xAxis->gridMajDash() - 1);
		minorDashCom->setCurrentIndex(chart->xAxis->gridMinDash() - 1);
		zeroDashCom->setCurrentIndex(chart->xAxis->zeroDash() - 1);
		majorBox->setChecked(chart->xAxis->gridMajEnabled());
		minorBox->setChecked(chart->xAxis->gridMinEnabled());
		zeroBox->setChecked(chart->xAxis->zeroEnabled());
		parent->currentAxis = 0;
	} else {
		majorThSpin->setValue(chart->yAxis->gridMajThick());
		minorThSpin->setValue(chart->yAxis->gridMinThick());
		zeroThSpin->setValue(chart->yAxis->zeroThick());
		majorDashCom->setCurrentIndex(chart->yAxis->gridMajDash() - 1);
		minorDashCom->setCurrentIndex(chart->yAxis->gridMinDash() - 1);
		zeroDashCom->setCurrentIndex(chart->yAxis->zeroDash() - 1);
		majorBox->setChecked(chart->yAxis->gridMajEnabled());
		minorBox->setChecked(chart->yAxis->gridMinEnabled());
		zeroBox->setChecked(chart->yAxis->zeroEnabled());
		parent->currentAxis = 1;
	}
}

void GridTab::update()
{
	switchAxis(parent->currentAxis);
}

bool GridTab::apply()
{
	XY *chart = widget->chart();

	if (comAxis->currentIndex() == 0) {
		chart->xAxis->setGridMajEnabled(majorBox->isChecked());
		chart->xAxis->setGridMinEnabled(minorBox->isChecked());
		chart->xAxis->setZeroEnabled(zeroBox->isChecked());
		chart->xAxis->setGridMajThick(majorThSpin->value());
		chart->xAxis->setGridMinThick(minorThSpin->value());
		chart->xAxis->setZeroThick(zeroThSpin->value());
		chart->xAxis->setGridMajDash(majorDashCom->currentIndex() + 1);
		chart->xAxis->setGridMinDash(minorDashCom->currentIndex() + 1);
		chart->xAxis->setZeroDash(zeroDashCom->currentIndex() + 1);
	} else {
		chart->yAxis->setGridMajEnabled(majorBox->isChecked());
		chart->yAxis->setGridMinEnabled(minorBox->isChecked());
		chart->yAxis->setZeroEnabled(zeroBox->isChecked());
		chart->yAxis->setGridMajThick(majorThSpin->value());
		chart->yAxis->setGridMinThick(minorThSpin->value());
		chart->yAxis->setZeroThick(zeroThSpin->value());
		chart->yAxis->setGridMajDash(majorDashCom->currentIndex() + 1);
		chart->yAxis->setGridMinDash(minorDashCom->currentIndex() + 1);
		chart->yAxis->setZeroDash(zeroDashCom->currentIndex() + 1);
	}
	chart->setModified();
	widget->update();

	return true;
}

LegendTab::LegendTab(ChartWidget *_widget, ChartSettings *parent)
 : QWidget(parent), widget(_widget)
{
	QVBoxLayout *mainLayout = new QVBoxLayout;
	QGridLayout *legendLayout = new QGridLayout;
	
	legendBox = new QGroupBox(_("Legend"));
	legendBox->setCheckable(true);
	legendBox->setChecked(false);
	QLabel *fontsizeLabel = new QLabel(_("Font size:"));
	fontsizeSpin = new QSpinBox;
	fontsizeSpin->setRange(8, 32);
	fontsizeSpin->setSingleStep(1);

	legendLayout->addWidget(fontsizeLabel, 0, 0);
	legendLayout->addWidget(fontsizeSpin, 0, 1);
	legendBox->setLayout(legendLayout);
	mainLayout->addWidget(legendBox);
	mainLayout->addStretch(1);
	setLayout(mainLayout);
}

void LegendTab::update()
{
	XY *chart = widget->chart();

	legendBox->setChecked(!(chart->legend->isHidden()));
	fontsizeSpin->setValue(chart->legend->fontSize());
}

bool LegendTab::apply()
{
	XY *chart = widget->chart();

	chart->legend->setHidden(!legendBox->isChecked());
	chart->legend->setFontSize(fontsizeSpin->value());
	chart->setModified();
	widget->update();

	return true;
}

ChartElementsTab::ChartElementsTab(ChartWidget *_widget,
                                   ChartSettings *parent)
 : QWidget(parent), widget(_widget)
{
	elements = &(widget->chart()->elements);
	QHBoxLayout *mainLayout   = new QHBoxLayout;
	QVBoxLayout *listLayout   = new QVBoxLayout;
	QVBoxLayout *groupLayout  = new QVBoxLayout;
	QHBoxLayout *hideLayout   = new QHBoxLayout;
	QVBoxLayout *updownLayout = new QVBoxLayout;

	listElements = new QListWidget;
	for (unsigned int i = 0; i < elements->size(); i++) {
		QListWidgetItem *item = 
		           new QListWidgetItem(elements->at(i)->name.c_str());
		listElements->addItem(item);
	}
	listElements->setMaximumWidth(100);

	groupBox = new QGroupBox(this);
	groupBox->setFlat(true);
	hideButton = new QPushButton(_("Hide"));
	QToolButton *upButton = new QToolButton();
	upButton->setArrowType(Qt::UpArrow);
	upButton->setFixedHeight(15);
	QToolButton *downButton = new QToolButton();
	downButton->setArrowType(Qt::DownArrow);
	downButton->setFixedHeight(15);

	updownLayout->addWidget(upButton);
	updownLayout->addWidget(downButton);
	hideLayout->setSpacing(0);
	hideLayout->addWidget(hideButton);
	hideLayout->addLayout(updownLayout);

	deleteButton = new QPushButton(_("Delete"));

	groupLayout->setMargin(0);
	groupLayout->setSpacing(1);
	groupLayout->addLayout(hideLayout);
	groupLayout->addWidget(deleteButton);
	groupBox->setLayout(groupLayout);
	groupBox->setDisabled(true);

	listLayout->addWidget(listElements);
	listLayout->addWidget(groupBox);

	pagesWidget = new QStackedWidget;
	xyPage      = new XYPage(widget);
	funcxyPage  = new FuncXYPage;
	pagesWidget->addWidget(new QWidget);
	pagesWidget->addWidget(xyPage);
	pagesWidget->addWidget(funcxyPage);
	
	mainLayout->addLayout(listLayout);
	mainLayout->addWidget(pagesWidget);
	setLayout(mainLayout);

	connect(listElements, SIGNAL(currentRowChanged(int)), this, 
	        SLOT(rowChanged(int)));
	connect(hideButton, SIGNAL(clicked()), this,
	        SLOT(hideCurrentElement()));
	connect(upButton, SIGNAL(clicked()), this,
	        SLOT(moveCurrentElementUp()));
	connect(downButton, SIGNAL(clicked()), this,
	        SLOT(moveCurrentElementDown()));
	connect(deleteButton, SIGNAL(clicked()), this,
	        SLOT(delCurrentElement()));
}

void ChartElementsTab::rowChanged(int row)
{
	if (row < 0) {
		groupBox->setDisabled(true);
		pagesWidget->setCurrentIndex(0);
		return;
	}
	if (row == (int) elements->size())
		row--;

	groupBox->setEnabled(true);
	if (elements->at(row)->isHidden())
		hideButton->setText(_("Show"));
	else
		hideButton->setText(_("Hide"));

	if ((elements->at(row)->type() & elem_xy) != 0) {
		pagesWidget->setCurrentIndex(1);
		((XYPage*) pagesWidget->currentWidget())->setup((DataSeriesXY*)
		                                           elements->at(row));
	} else if (elements->at(row)->type() == elem_func) {
		pagesWidget->setCurrentIndex(2);
		((FuncXYPage*) pagesWidget->currentWidget())->setup((FuncXY*)
		                                           elements->at(row));
	} else {
		pagesWidget->setCurrentIndex(0);
	}
}

void ChartElementsTab::hideCurrentElement()
{
	int i = listElements->currentRow();
	if (i < 0)
		return;
	
	ChartElement *el = elements->at(i);
	if (el->isHidden()) {
		el->setHidden(false);
		hideButton->setText(_("Hide"));
		listElements->item(i)->setTextColor(Qt::black);
	} else {
		el->setHidden(true);
		hideButton->setText(_("Show"));
		listElements->item(i)->setTextColor(Qt::gray);
	}
	widget->chart()->setModified();
	widget->update();
}

void ChartElementsTab::delCurrentElement()
{
	int i = listElements->currentRow();
	if (i < 0)
		return;
	ChartElement *el = elements->at(i);
	elements->erase(elements->begin() + i);
	QListWidgetItem *item = listElements->takeItem(i);
	if (item)
		delete item;
	delete el;
	widget->chart()->setModified();
	widget->update();
}

void ChartElementsTab::moveCurrentElementUp()
{
	int i = listElements->currentRow();
	if (i < 1)
		return;

	ChartElement *el = elements->at(i - 1);
	elements->at(i - 1) = elements->at(i);
	elements->at(i) = el;
	listElements->insertItem(i - 1, listElements->takeItem(i));
	listElements->setCurrentRow(i - 1);
	widget->chart()->setModified();
	widget->update();
}

void ChartElementsTab::moveCurrentElementDown()
{
	int i = listElements->currentRow();
	if (i < 0 || i > (int) (elements->size() - 2))
		return;
	ChartElement *el = elements->at(i + 1);
	elements->at(i + 1) = elements->at(i);
	elements->at(i) = el;
	listElements->insertItem(i + 1, listElements->takeItem(i));
	listElements->setCurrentRow(i + 1);
	widget->chart()->setModified();
	widget->update();
}

void ChartElementsTab::update()
{
	for (unsigned int i = 0; i < elements->size(); i++) {
		QListWidgetItem *item = listElements->item(i);
		if (item != 0) {
			item->setText(elements->at(i)->name.c_str());
		} else {
			item = 
			   new QListWidgetItem(elements->at(i)->name.c_str());
			listElements->addItem(item);
		}
	}
}

bool ChartElementsTab::apply()
{
	if (pagesWidget->currentIndex() == 1)
		xyPage->apply();
	else if (pagesWidget->currentIndex() == 2)
		funcxyPage->apply();

	widget->chart()->setModified();
	widget->update();
	update();
	return true;	
}

XYPage::XYPage(ChartWidget *_widget): widget(_widget)
{
	QVBoxLayout *mainLayout = new QVBoxLayout;
	QGridLayout *gridLayout = new QGridLayout;
	QGridLayout *symbLayout = new QGridLayout;
	QGridLayout *connLayout = new QGridLayout;
	QHBoxLayout *lblsLayout = new QHBoxLayout;
	QHBoxLayout *editLayout = new QHBoxLayout;

	QPushButton *editBut = new QPushButton(_("Edit data"));

	QLabel *nameLabel = new QLabel(_("Name:"));
	nameEdit = new QLineEdit;
	nameEdit->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);

	QLabel *colorLabel = new QLabel(_("Color:"));
	colorButton = new QToolButton();
	QPixmap pix(16, 16);
	pix.fill(Qt::black);
	colorButton->setIcon(QIcon(pix));

	checkSymb = new QCheckBox(_("Symbols"));
	/* TRANSLATORS: Symbol shape */
	QLabel *symbLbl = new QLabel(_("Shape:"));
	symbLbl->hide();
	comSymb = new QComboBox;
	comSymb->addItem(QIcon(symb1_xpm), QString());
	comSymb->addItem(QIcon(symb2_xpm), QString());
	comSymb->addItem(QIcon(symb3_xpm), QString());
	comSymb->addItem(QIcon(symb4_xpm), QString());
	comSymb->addItem(QIcon(symb5_xpm), QString());
	comSymb->addItem(QIcon(symb6_xpm), QString());
	comSymb->addItem(QIcon(symb7_xpm), QString());
	comSymb->addItem(QIcon(symb8_xpm), QString());
	comSymb->hide();
	QLabel *sizeLbl = new QLabel(_("Size:"));
	sizeLbl->hide();
	spinSize = new QSpinBox;
	spinSize->setRange(1, 20);
	spinSize->setSingleStep(1);
	spinSize->hide();
	
	checkConn = new QCheckBox(_("Connected plot"));
	QLabel *thickLbl = new QLabel(_("Line thickness:"));
	thickLbl->hide();
	thickSpin = new QDoubleSpinBox;
	thickSpin->setRange(0, 5);
	thickSpin->setSingleStep(0.1);
	thickSpin->hide();
	QLabel *dashLbl = new QLabel(_("Dash:"));
	dashLbl->hide();
	dashCombo = new QComboBox;
	dashCombo->addItem("---------------");
	dashCombo->addItem("-- -- -- -- -- -");
	dashCombo->addItem("\xc2\xb7 \xc2\xb7 \xc2\xb7 \xc2\xb7 \xc2\xb7 "
	                   "\xc2\xb7 \xc2\xb7 \xc2\xb7 \xc2\xb7 \xc2\xb7");
	dashCombo->addItem("-- \xc2\xb7 -- \xc2\xb7 -- \xc2\xb7 -");
	dashCombo->addItem("-- \xc2\xb7 \xc2\xb7 -- \xc2\xb7 \xc2\xb7 --");
	dashCombo->hide();
	
	checkLbls = new QCheckBox(_("Labels"));
	QLabel *lblstypeLbl = new QLabel(_("Type:"));
	lblstypeLbl->hide();
	comLblsType = new QComboBox;
	comLblsType->addItem("X");
	comLblsType->addItem("Y");
	comLblsType->addItem("X;Y");
	comLblsType->hide();

	editLayout->addWidget(editBut);
	editLayout->addStretch(1);
	gridLayout->addWidget(nameLabel, 0, 0);
	gridLayout->addWidget(nameEdit, 0, 1);
	gridLayout->addWidget(colorLabel, 1, 0);
	gridLayout->addWidget(colorButton, 1, 1);
	symbLayout->addWidget(symbLbl, 0, 0);
	symbLayout->addWidget(comSymb, 0, 1);
	symbLayout->addWidget(sizeLbl, 1, 0);
	symbLayout->addWidget(spinSize, 1, 1);
	connLayout->addWidget(dashLbl, 0, 0);
	connLayout->addWidget(dashCombo, 0, 1);
	connLayout->addWidget(thickLbl, 1, 0);
	connLayout->addWidget(thickSpin, 1, 1);
	lblsLayout->addWidget(lblstypeLbl);
	lblsLayout->addWidget(comLblsType);
	mainLayout->addLayout(editLayout);
	mainLayout->addLayout(gridLayout);
	mainLayout->addWidget(checkSymb);
	mainLayout->addLayout(symbLayout);
	mainLayout->addWidget(checkConn);
	mainLayout->addLayout(connLayout);
	mainLayout->addWidget(checkLbls);
	mainLayout->addLayout(lblsLayout);
	mainLayout->addStretch(2);
	setLayout(mainLayout);

	connect(editBut, SIGNAL(clicked()), this, SLOT(editData()));
	connect(colorButton, SIGNAL(clicked()), this, SLOT(changeColor()));
	connect(checkSymb, SIGNAL(toggled(bool)), symbLbl,
	        SLOT(setVisible(bool)));
	connect(checkSymb, SIGNAL(toggled(bool)), comSymb,
	        SLOT(setVisible(bool)));
	connect(checkSymb, SIGNAL(toggled(bool)), sizeLbl,
	        SLOT(setVisible(bool)));
	connect(checkSymb, SIGNAL(toggled(bool)), spinSize,
	        SLOT(setVisible(bool)));
	connect(checkConn, SIGNAL(toggled(bool)), thickLbl,
	        SLOT(setVisible(bool)));
	connect(checkConn, SIGNAL(toggled(bool)), thickSpin,
	        SLOT(setVisible(bool)));
	connect(checkConn, SIGNAL(toggled(bool)), dashLbl,
	        SLOT(setVisible(bool)));
	connect(checkConn, SIGNAL(toggled(bool)), dashCombo,
	        SLOT(setVisible(bool)));
	connect(checkLbls, SIGNAL(toggled(bool)), lblstypeLbl,
	        SLOT(setVisible(bool)));
	connect(checkLbls, SIGNAL(toggled(bool)), comLblsType,
	        SLOT(setVisible(bool)));
}

void XYPage::setup(DataSeriesXY *data)
{
	currentData = data;

	nameEdit->setText(data->name.c_str());

	color = data->color();
	QColor c;
	c.setRgbF(color.red(), color.green(), color.blue());
	QPixmap pix(16, 16);
	pix.fill(c);
	colorButton->setIcon(QIcon(pix));

	if (data->isSymbolsEnabled())
		checkSymb->setCheckState(Qt::Checked);
	else
		checkSymb->setCheckState(Qt::Unchecked);

	if (data->isConnectedEnabled())
		checkConn->setCheckState(Qt::Checked);
	else
		checkConn->setCheckState(Qt::Unchecked);

	if (data->isLabelsEnabled())
		checkLbls->setCheckState(Qt::Checked);
	else
		checkLbls->setCheckState(Qt::Unchecked);

	comSymb->setCurrentIndex(data->symbolShape());
	spinSize->setValue(data->symbolSize());
	thickSpin->setValue(data->lineThickness());
	dashCombo->setCurrentIndex(data->dash() - 1);
	comLblsType->setCurrentIndex(data->labelType());
}

void XYPage::apply()
{
	currentData->name = (const char*)nameEdit->text().toAscii();
	currentData->setColor(color);
	currentData->setSymbolsEnabled(checkSymb->checkState());
	currentData->setConnectedEnabled(checkConn->checkState());
	currentData->setLineThickness(thickSpin->value());
	currentData->setDash((line_dash) (dashCombo->currentIndex() + 1));
	currentData->setLabelsEnabled(checkLbls->checkState());
	currentData->setSymbolShape((point_shape) comSymb->currentIndex());
	currentData->setSymbolSize(spinSize->value());
	currentData->setLabelType(comLblsType->currentIndex());
}

void XYPage::editData()
{
	QDialog editDialog(this);
	editDialog.setWindowTitle(_("Edit data..."));

	QVBoxLayout mainLayout;
	QHBoxLayout butLayout;
	
	Sheet table(currentData->getData());
	QPushButton okButton(_("OK"));
	connect(&okButton, SIGNAL(clicked()), &editDialog, SLOT(accept()));
	QPushButton cancelButton(_("Cancel"));
	connect(&cancelButton, SIGNAL(clicked()), &editDialog, SLOT(reject()));

	butLayout.addStretch(0);
	butLayout.addWidget(&okButton);
	butLayout.addWidget(&cancelButton);
	mainLayout.addWidget(&table);
	mainLayout.addLayout(&butLayout);

	editDialog.setLayout(&mainLayout);

	if (!editDialog.exec())
		return;
	currentData->setData(table.getData());
	widget->chart()->setModified();
	widget->update();
}

void XYPage::changeColor()
{
	QColor c;
	c.setRgbF(color.red(), color.green(), color.blue());
	c = QColorDialog::getColor(c, this);
	if (!c.isValid())
		return;

	QPixmap pix(16, 16);
	pix.fill(c);
	colorButton->setIcon(QIcon(pix));
	color = Color(c.redF(), c.greenF(), c.blueF());
}

FuncXYPage::FuncXYPage()
{
	QGridLayout *mainLayout = new QGridLayout;

	QLabel *nameLabel = new QLabel(_("Name:"));
	nameEdit = new QLineEdit;
	nameEdit->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);

	QLabel *colorLabel = new QLabel(_("Color:"));
	colorButton = new QToolButton();
	QPixmap pix(16, 16);
	pix.fill(Qt::black);
	colorButton->setIcon(QIcon(pix));

	QLabel *thickLabel = new QLabel(_("Line thickness:"));
	thickSpin = new QDoubleSpinBox;
	thickSpin->setRange(0, 5);
	thickSpin->setSingleStep(0.1);

	QLabel *dashLabel = new QLabel(_("Dash:"));
	dashCombo = new QComboBox;
	dashCombo->addItem("---------------");
	dashCombo->addItem("-- -- -- -- -- -");
	dashCombo->addItem("\xc2\xb7 \xc2\xb7 \xc2\xb7 \xc2\xb7 \xc2\xb7 "
	                   "\xc2\xb7 \xc2\xb7 \xc2\xb7 \xc2\xb7 \xc2\xb7");
	dashCombo->addItem("-- \xc2\xb7 -- \xc2\xb7 -- \xc2\xb7 -");
	dashCombo->addItem("-- \xc2\xb7 \xc2\xb7 -- \xc2\xb7 \xc2\xb7 --");
	
	/* TRANSLATORS: Number of approximation points of a curve */
	QLabel *pointsLabel = new QLabel(_("Approximation points:"));
	pointsSpin = new QSpinBox;
	pointsSpin->setRange(1, 1000);
	pointsSpin->setSingleStep(1);

	leftBoundCheck = new QCheckBox(_("Left bound:"));
	leftBoundEdit = new QLineEdit;
	rightBoundCheck = new QCheckBox(_("Right bound:"));
	rightBoundEdit = new QLineEdit;

	mainLayout->addWidget(nameLabel, 0, 0);
	mainLayout->addWidget(nameEdit, 0, 1);
	mainLayout->addWidget(colorLabel, 1, 0);
	mainLayout->addWidget(colorButton, 1, 1);
	mainLayout->addWidget(thickLabel, 2, 0);
	mainLayout->addWidget(thickSpin, 2, 1);
	mainLayout->addWidget(dashLabel, 3, 0);
	mainLayout->addWidget(dashCombo, 3, 1);
	mainLayout->addWidget(pointsLabel, 4, 0);
	mainLayout->addWidget(pointsSpin, 4, 1);
	mainLayout->addWidget(leftBoundCheck, 5, 0);
	mainLayout->addWidget(leftBoundEdit, 5, 1);
	mainLayout->addWidget(rightBoundCheck, 6, 0);
	mainLayout->addWidget(rightBoundEdit, 6, 1);
	mainLayout->setRowStretch(7, 1);

	setLayout(mainLayout);

	connect(colorButton, SIGNAL(clicked()), this, SLOT(changeColor()));
	connect(leftBoundCheck, SIGNAL(toggled(bool)), leftBoundEdit,
	        SLOT(setEnabled(bool)));
	connect(rightBoundCheck, SIGNAL(toggled(bool)), rightBoundEdit,
	        SLOT(setEnabled(bool)));
}

void FuncXYPage::setup(FuncXY *data)
{
	currentData = data;
	
	nameEdit->setText(data->name.c_str());

	color = data->color();
	QColor c;
	c.setRgbF(color.red(), color.green(), color.blue());
	QPixmap pix(16, 16);
	pix.fill(c);
	colorButton->setIcon(QIcon(pix));
	
	thickSpin->setValue(data->lineThickness());
	dashCombo->setCurrentIndex(data->dash() - 1);
	pointsSpin->setValue(data->approxPoints());

	leftBoundCheck->setChecked(data->isLeftBoundEnabled());
	rightBoundCheck->setChecked(data->isRightBoundEnabled());
	leftBoundEdit->setEnabled(data->isLeftBoundEnabled());
	rightBoundEdit->setEnabled(data->isRightBoundEnabled());
	leftBoundEdit->setText(QString("%1").arg(data->leftBound()));
	rightBoundEdit->setText(QString("%1").arg(data->rightBound()));
}

void FuncXYPage::apply()
{
	currentData->name = (const char*) nameEdit->text().toAscii();
	currentData->setColor(color);
	currentData->setLineThickness(thickSpin->value());
	currentData->setDash((line_dash) (dashCombo->currentIndex() + 1));
	currentData->setApproxPoints(pointsSpin->value());
	currentData->setLeftBoundEnabled(leftBoundCheck->isChecked());
	currentData->setRightBoundEnabled(rightBoundCheck->isChecked());
	currentData->setLeftBound(atof(leftBoundEdit->text().toAscii()));
	currentData->setRightBound(atof(rightBoundEdit->text().toAscii()));
}

void FuncXYPage::changeColor()
{
	QColor c;
	c.setRgbF(color.red(), color.green(), color.blue());
	c = QColorDialog::getColor(c, this);
	if (!c.isValid())
		return;

	QPixmap pix(16, 16);
	pix.fill(c);
	colorButton->setIcon(QIcon(pix));
	color = Color(c.redF(), c.greenF(), c.blueF());
}

TeXButton::TeXButton(QString _label, QString _text, QLineEdit *_edit)
 : label(_label), text(_text), edit(_edit)
{
	if (text == NULL)
		text=label;

	setFixedWidth(40);

	if (edit != NULL)
		connect(this, SIGNAL(clicked()), this, SLOT(enterSymbol()));
}

void TeXButton::enterSymbol()
{
	edit->insert(text);
	edit->cursorBackward(false, 1);
	edit->setFocus();
}

void TeXButton::paintEvent(QPaintEvent *e)
{
	QPushButton::paintEvent(e);
	QPainter canvas;
	NativeExport exp(&canvas);
	canvas.begin(this);
	TeX tex(&exp);
	int t = isDown() ? 0 : 1;
	tex.textInterpret((width() - tex.textWidth((const char*)
	                                           label.toAscii(), 14)) / 2
	                  - t, height() - (height() - exp.getFontAscent()) / 2
	                  - t, (const char*) label.toAscii(), 14, 0);
	canvas.end();
}

SymbolButton::SymbolButton(QString _text, QWidget *parent)
 : QAbstractButton(parent)
{
	text = _text;
	setFixedWidth(30);
	setFixedHeight(30);
	setToolTip(text);
}

void SymbolButton::paintEvent(QPaintEvent*)
{
	QPainter canvas;
	NativeExport exp(&canvas);
	canvas.begin(this);
	TeX tex(&exp);	
	if (hasFocus()) {
		QStyleOptionMenuItem opt;
		opt.rect = rect();
		opt.palette = palette();
		opt.state |= QStyle::State_Selected | QStyle::State_Active 
		             | QStyle::State_Enabled;
		style()->drawControl(QStyle::CE_MenuItem, &opt, &canvas, this);
	}
	tex.textInterpret((width() - tex.textWidth((const char*)
	                                           text.toAscii(), 18)) / 2,
	                  20, (const char*) text.toAscii(), 18, 0);
	canvas.end();
}

void SymbolButton::enterEvent(QEvent*)
{
	setFocus();
	update();
}

void SymbolButton::leaveEvent(QEvent*)
{
	clearFocus();
	update();
}

QString SymbolButton::symbol()
{
	return text;
}

SymbolPopup::SymbolPopup(int min, int max, 
                         QLineEdit *_lineEdit,
                         QWidget *parent)
 : QMenu(parent)
{
	lineEdit = _lineEdit;
	QGridLayout *grid = new QGridLayout();
	grid->setMargin(5);
	grid->setSpacing(0);
	SymbolButton *item;
	for (int i = min; i < max; ++i) {
		item = new SymbolButton(symbol_array[i].symb, this);
		connect(item, SIGNAL(clicked()), this, SLOT(symbolSelected()));
		grid->addWidget(item, (i - min) / 4, (i - min) % 4);
	}
	setLayout(grid);
}

void SymbolPopup::symbolSelected()
{
	lineEdit->insert(((SymbolButton*) sender())->symbol());
	lineEdit->setFocus();
	hide();	
}

