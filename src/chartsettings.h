/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CHARTSETTINGS_H
#define CHARTSETTINGS_H

#include <QtGui>
#include <vector>
#include "chartelement.h"
#include "chartwidget.h"
#include "dataseriesxy.h"
#include "funcxy.h"
#include "spreadsheet.h"
#include "types.h"
#include "legend.h"

using namespace std;

class ChartElement;
class ChartWidget;
class DataSeriesXY;
class FuncXY;
class LegendTab;
class AxesTab;
class TicsTab;
class GridTab;
class ChartElementsTab;
class XYPage;
class FuncXYPage;

/**
 *   Setting dialog class.
 */
class ChartSettings: public QDialog
{
Q_OBJECT
public:
	/**
	 *   A constructor.
	 *   \param xy pointer to XY widget (to which settings apply)
	 *   \param parent pointer to pwrent widget
	 */
	ChartSettings(ChartWidget *widget);
	
	/**  A destructor. */
	~ChartSettings() {}; 
public slots:
	void ok();                   /**< Ok clicked event */
	bool apply();                /**< Apply changes */
	void update();               /**< Update dialog to external changes */
	
	/**
	 *   Tab changed event.
	 *   \param t tab number
	 */
	void tabChanged(int t);
protected:
	int currentAxis;         /**< Holds current axis for axis combo box */
private:
	QTabWidget *tabWidget;       /**< Pointer to tab widget */
	AxesTab *axesTab;            /**< Pointer to axes tab */
	TicsTab *ticsTab;            /**< Pointer to tics tab */
	GridTab *gridTab;            /**< Pointer to grid tab */
	LegendTab *legendTab;        /**< Pointer to legend tab */
	ChartElementsTab* chartElemTab; /**< Pointer to chart element tab */
	friend class AxesTab;
	friend class TicsTab;
	friend class GridTab;
};

/**
 *   Axes settings tab class.
 */
class AxesTab: public QWidget
{
Q_OBJECT
public:
	/**
	 *   A constructor.
	 *   \param xy pointer to XY widget (to which settings apply)
	 *   \param parent pointer to pwrent widget
	 */
	AxesTab(ChartWidget *widget, ChartSettings *parent);
	
	/**  A destructor. */
	~AxesTab() {};
public slots:

	/**
	 *   Switches axis.
	 *   \param axis axis to switch
	 */
	void switchAxis(int axis);
	bool apply();               /**< Apply changes */
	void update();              /**< Update dialog to external changes */
private:
	QComboBox *comAxis;         /**< Pointer to axis combo box */
	QLineEdit *labelEdit;       /**< Pointer to label line edit */
	QSpinBox *labelSpin;        /**< Pointer to label size spin box */
	QComboBox *alignCombo;      /**< Pointer to align combo box */
	QComboBox *typeCombo;       /**< Pointer to axis scale type combo box*/
	QComboBox *dataCombo;
	QLineEdit *minEdit;         /**< Pointer to min line edit */
	QLineEdit *maxEdit;         /**< Pointer to max line edit */
	int currentAxis;            /**< Current axis */
	ChartWidget *widget;        /**< Pointer to XY widget */
	ChartSettings *parent;     /**< Pointer to parent widget */
};

/**
 *   Tics settings tab class.
 */
class TicsTab: public QWidget
{
Q_OBJECT
public:
	/**
	 *   A constructor.
	 *   \param xy pointer to XY widget (to which settings apply)
	 *   \param parent pointer to pwrent widget
	 */
	TicsTab(ChartWidget *xy, ChartSettings *parent);
	
	/**  A destructor. */
	~TicsTab() {};
public slots:
	/**
	 *   Switches axis.
	 *   \param axis axis to switch
	 */
	void switchAxis(int axis);
	bool apply();               /**< Apply changes */
	void update();              /**< Update dialog to external changes */
private:
	QComboBox *comAxis;         /**< Pointer to axis combo box */
	QLineEdit *majorEdit;       /**< Pointer to major tick line edit */
	QSpinBox *majorhSpin;     /**< Pointer to major tics height spin box */
	QSpinBox *minorSpin;      /**< Pointer to minor tics number spin box */
	QSpinBox *minorhSpin;       /**< Pointer to minor tics height */
	QComboBox *typeCombo;       /**< Pointer to tics type combo box */
	int currentAxis;            /**< Current axis */
	ChartWidget *widget;        /**< Pointer to XY widget */
	ChartSettings *parent;     /**< Pointer to parent widget */
};

/**
 *   Grid settings tab class.
 */
class GridTab: public QWidget
{
Q_OBJECT
public:
	/**
	 *   A constructor.
	 *   \param xy pointer to XY widget (to which settings apply)
	 *   \param parent pointer to pwrent widget
	 */
	GridTab(ChartWidget *widget, ChartSettings *parent);
	
	/**  A destructor. */
	~GridTab() {};
public slots:
	/**
	 *   Switches axis.
	 *   \param axis axis to switch
	 */
	void switchAxis(int axis);
	bool apply();               /**< Apply changes */
	void update();              /**< Update dialog to external changes */
private:
	QComboBox *comAxis;         /**< Pointer to axis combo box */

	/** Major grid thickness spin box */
	QDoubleSpinBox *majorThSpin;  

	/** Minor grid thickness spin box */
	QDoubleSpinBox *minorThSpin;

	/** Zero line thickness spin box */
	QDoubleSpinBox *zeroThSpin;

	/** Major grid dash pattern combo box */
	QComboBox *majorDashCom;

	/** Major grid dash pattern combo box */
	QComboBox *minorDashCom;

	/** Zero line dash pattern combo box */
	QComboBox *zeroDashCom;
	QGroupBox *majorBox;        /**< Pointer to major grid group box */
	QGroupBox *minorBox;        /**< Pointer to minor grid group box */
	QGroupBox *zeroBox;         /**< Pointer to zero line group box */
	int currentAxis;            /**< Current axis */
	ChartWidget *widget;        /**< Pointer to XY widget */
	ChartSettings *parent;     /**< Pointer to parent widget */
};

/**
 *   Legend settings tab class.
 */
class LegendTab: public QWidget
{
Q_OBJECT
public:
	/**
	 *   A constructor.
	 *   \param xy pointer to XY widget (to which settings apply)
	 *   \param parent pointer to pwrent widget
	 */
	LegendTab(ChartWidget *widget, ChartSettings *parent);
	
	/**  A destructor. */
	~LegendTab() {};
public slots:
	bool apply();               /**< Apply changes */
	void update();              /**< Update dialog to external changes */
private:
	QGroupBox *legendBox;       /**< Pointer to legend group box */
	QSpinBox *fontsizeSpin;     /**< Pointer to font size spin box */
	ChartWidget *widget;        /**< Pointer to XY widget */
	ChartSettings *parent;     /**< Pointer to parent widget */
};

/**
 *   Chart elements settings tab.
 */
class ChartElementsTab: public QWidget
{
Q_OBJECT
public:
	/**
	 *   A constructor.
	 *   \param xy pointer to XY widget (to which settings apply)
	 *   \param parent pointer to pwrent widget
	 */
	ChartElementsTab(ChartWidget *widget, ChartSettings *parent);

	/**  A destructor. */
	~ChartElementsTab() {};
public slots:
	void rowChanged(int row);      /**< Row changed event */
	void hideCurrentElement();     /**< Hides corrent element */
	void delCurrentElement();      /**< Shows current element */
	void moveCurrentElementUp();   /**< Moves current enelent down */
	void moveCurrentElementDown(); /**< Moves current element down */
	bool apply();                  /**< Apply changes */
	void update();               /**< Update dialog to external changes */
private:
	QStackedWidget *pagesWidget;   /**< Pointer to pages stacked widget */
	XYPage *xyPage;                /**< Pointer to DataSeriesXY page */
	FuncXYPage *funcxyPage;        /**< Pointer to FuncXY Page */
	QGroupBox *groupBox;           /**< Pointer to group box */
	QPushButton *hideButton;       /**< Pointer to hide button */
	QPushButton *deleteButton;     /**< Pointer to delete button */
	QListWidget *listElements;     /**< Pointer to elements list widget */
	vector<ChartElement*> *elements; /**< Pointer to elements vector */
	ChartWidget *widget;           /**< Pointer to XY widget */
	ChartSettings *parent;        /**< Pointer to parent widget */
};

/**
 *   DataSeriesXY settings page widget.
 */
class XYPage: public QWidget
{
Q_OBJECT
public:
	/**
	 *   A constructor.
	 *   \param xy pointer to XY widget (to which settings apply)
	 */
	XYPage(ChartWidget *widget);

	/**  A destructor. */
	~XYPage() {};

	/**
	 *   Read data from a given DataSeriesXY.
	 *   \param xy pointer to DataSeriesXY object to read from
	 */
	void setup(DataSeriesXY *xy);
	void apply();                  /**< Apply changes */
private slots:
	void editData();               /**< Invoke data editing dialog */
	void changeColor();            /**< Invoke color changing dialog */
private:
	QLineEdit *nameEdit;           /**< Name edit widget */
	QToolButton *colorButton;      /**< Color button */
	QCheckBox *checkSymb;          /**< Symbol check box */
	QCheckBox *checkConn;          /**< Connected plotting check box */
	QCheckBox *checkLbls;          /**< Labels check box */
	QComboBox *comSymb;            /**< Symbols combo box */
	QComboBox *comLblsType;        /**< Labels type combo */
	QComboBox *dashCombo;          /**< Dash pattern combo box */
	QDoubleSpinBox *thickSpin;     /**< Line thickness double spin box */
	QSpinBox *spinSize;            /**< Symbol size spin box */
	DataSeriesXY *currentData;     /**< Pointer to current data object */
	ChartWidget *widget;           /**< XY widget */
	Color color;                   /**< Symbols color */
};

/**
 *   FuncXY settings page widget.
 */
class FuncXYPage: public QWidget
{
Q_OBJECT
public:
	/**  A constructor. */
	FuncXYPage();

	/** A destructor. */
	~FuncXYPage() {};

	/**
	 *   Read data from a given FuncXY.
	 *   \param fxy pointer to FuncXY object to read from.
	 */
	void setup(FuncXY *fxy);
	void apply();                  /**< Apply changes */
private slots:
	void changeColor();            /**< Invoke color changing dialog */
private:
	QLineEdit *nameEdit;           /**< Name edit widget */
	QToolButton *colorButton;      /**< Color button */
	QComboBox *dashCombo;          /**< Dash pattern combo box */
	QSpinBox *pointsSpin;          /**< Approximation points spin box */
	QDoubleSpinBox *thickSpin;     /**< Line thickness double spin box */
	QCheckBox *leftBoundCheck;     /**< Left bound check box */
	QLineEdit *leftBoundEdit;      /**< Left bound line edit */
	QCheckBox *rightBoundCheck;    /**< Right bound check box */
	QLineEdit *rightBoundEdit;     /**< Right bound line edit */
	FuncXY* currentData;           /**< Pointer to currrent data object */
	Color color;                   /**< Symbols color */
};

/**
 *   Push button with TeX label.
 *   \sa SymbolButton
 */
class TeXButton: public QPushButton
{
Q_OBJECT
public:
	/**
	 *   A constructor.
	 *   \param label TeXed label
	 *   \param text text to insert on click
	 *   \param edit poiner to QLineEdit to paste into
	 */
	TeXButton(QString label, QString text = 0, QLineEdit *edit = 0);

	/**  A destructor. */
	~TeXButton() {};
public slots:
	void enterSymbol();            /**< Inserts the symbol to line edit */
protected slots:
	/**
	 *   Event triggered when repaint is necessary.
	 */
	void paintEvent(QPaintEvent*);
private:
	QString label;                 /**< Label of the button */
	QString text;                  /**< Text to paste on click */
	QLineEdit *edit;               /**< Line edit to paste into */
};

/**
 *  Ordinary button with TeX label.
 *  \sa TeXButton
 */
class SymbolButton: public QAbstractButton
{
Q_OBJECT
public:
	/**
	 *   A constructor.
	 *   \param text TeXed label and symbol
	 *   \param parent parent widget
	 */
	SymbolButton(QString text, QWidget *parent);

	/**  A destructor. */
	~SymbolButton() {};

	/**
	 *   Returns symbol.
	 *   \returns TeXed symbol
	 */
	QString symbol();
protected slots:
	/**
	 *   Event triggered when repaint is necessary.
	 */
	void paintEvent(QPaintEvent*); 
	void enterEvent(QEvent*);      /**< Event triggered on mouse over */
	void leaveEvent(QEvent*);      /**< Event triggered on mouse out */
private:
	QString text;                  /**< Label/symbol */
};

/**
 *  Symbol popup menu. 
 */
class SymbolPopup: public QMenu
{
Q_OBJECT
public:
	/**
	 *   A constructor.
	 *   For reference on symbol numbers, see texsymbols.h
	 *   \param min lower symbol number
	 *   \param max upper symbol number
	 *   \param edit QLineEdit to paste a symbol into
	 *   \param parent parent widget
	 */
	SymbolPopup(int min, int max, QLineEdit *edit, QWidget *parent);

	/**  A destructor. */
	~SymbolPopup() {};
public slots:
	void symbolSelected();         /**< Symbol selected event */
private:
	QLineEdit *lineEdit;           /**< Line edit for pasting */
};

#endif /* CHARTSETTINGS_H */

