/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "chartwidget.h"

using namespace std;

ChartWidget::ChartWidget(QWidget *parent): QWidget(parent)
{
	mouseX = 0;
	mouseY = 0;

	nativeExport   = new NativeCairoExport;
	vchart         = new XY(width(), height(), nativeExport);
	settingsDialog = new ChartSettings(this);

	setMouseTracking(true);
}

ChartWidget::~ChartWidget()
{
	delete settingsDialog;
	delete vchart;
	delete nativeExport;
}

void ChartWidget::settings()
{
	settingsDialog->show();
	settingsDialog->update();
}

void ChartWidget::findPeaks()
{
	vchart->findPeaks();
	settingsDialog->update();
	update();
}

void ChartWidget::fit(nplot::fit_type type)
{
	chart()->fit(type);
	settingsDialog->update();
	update();
}

void ChartWidget::fitZoom()
{
	vchart->fitZoom();
	settingsDialog->update();
	update();
}

void ChartWidget::paintEvent(QPaintEvent*)
{
	if (vchart->isModified()) {
		nativeExport->begin(this);
		vchart->repaint();
		nativeExport->end();
	} else {
		nativeExport->redrawFromBuffer(this);
	}	

}

void ChartWidget::resizeEvent(QResizeEvent*)
{
	vchart->setWidth(width());
	vchart->setHeight(height());
}

void ChartWidget::mouseMoveEvent(QMouseEvent *event)
{
	int    x  = event->x();
	int    y  = event->y();
	double wx = vchart->wdw2realX(x);
	double wy = vchart->wdw2realY(y);

	switch (vchart->objectAt(x, y)) {
	case 1:
		setCursor(QCursor(Qt::SizeHorCursor));
		break;
	case 2:
		setCursor(QCursor(Qt::SizeVerCursor));
		break;
	default:
		setCursor(QCursor(Qt::CrossCursor));
	}

	if (event->buttons() == Qt::RightButton) {
		switch (vchart->objectAt(mouseX, mouseY)) {
		case 1:
			vchart->scroll(mouseX, x, 0, 0);
			break;
		case 2:
			vchart->scroll(0, 0, mouseY, y);
			break;
		default:
			vchart->scroll(mouseX, x, mouseY, y);
		}
		mouseX = x;
		mouseY = y;
	} else if (event->buttons() == Qt::MidButton) {
		switch (vchart->objectAt(mouseX, mouseY)) {
		case 1:
			vchart->zoom(mouseX, x, 0, 0);
			break;
		case 2:
			vchart->zoom(0, 0, mouseY, y);
			break;
		default:
			vchart->zoom(mouseX, x, mouseY, y);
		}
		mouseX = x;
		mouseY = y;
	}

	emit xyChanged(wx, wy);
	if (vchart->isModified()) {
		settingsDialog->update();
		update();
	}
}

void ChartWidget::mousePressEvent(QMouseEvent *event)
{
	if ((event->button() & (Qt::RightButton | Qt::MidButton)) == 0)
		return;
	mouseX = event->x();
	mouseY = event->y();
}

void ChartWidget::wheelEvent(QWheelEvent *event)
{
	double delta = event->delta() / 6000.;

	if (event->modifiers() == Qt::CTRL) {
		vchart->scroll(0, width(), 0, 0, delta);
	} else if (event->modifiers() == Qt::SHIFT) {
		vchart->scroll(0, 0, 0, height(), delta);
	} else {
		switch (vchart->objectAt(event->x(), event->y())) {
		case 1:
			vchart->zoom(0, width(), 0, 0, delta);
			break;
		case 2:
			vchart->zoom(0, 0, 0, -height(), delta);
			break;
		default:
			vchart->zoom(0, width(), 0, -height(), delta);
		}
	}

	if (vchart->isModified()) {
		settingsDialog->update();
		update();
	}
}

void ChartWidget::mouseDoubleClickEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
		settings(); 
}

