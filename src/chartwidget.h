/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CHARTWIDGET_H
#define CHARTWIDGET_H

#include <QtGui>
#include "chartsettings.h"
#include "qtexports.h"
#include "xy.h"

class ChartSettings;

using namespace std;

/** Widget drawing chart using XY object */
class ChartWidget: public QWidget
{
Q_OBJECT
public:
	/**
	 *   A constructor
	 *   \param paretnt parent widget
	 */
	ChartWidget(QWidget *parent);
	virtual ~ChartWidget();      /**< A destructor */
	void settings();             /**< Show a settings dialog */

	/**  Returns pointer to XY object */
	XY* chart() const   { return vchart; };

	void findPeaks();            /**< Find peaks */

	/**
	 *   Fits a given curve to the data
	 *   \param type type of fitting
	 */
	void fit(nplot::fit_type type);

	void fitZoom();              /**< Zoom to fit all the data sets */
protected slots:
	/**  Called when repaint is necessary */
	void paintEvent(QPaintEvent*);

	/**  Called on widget resize */
	void resizeEvent(QResizeEvent*);

	/**  Called on mouse move */
	void mouseMoveEvent(QMouseEvent*);

	/**  Called when mouse button is pressed */
	void mousePressEvent(QMouseEvent*);

	/**  Called on mouse wheel */
	void wheelEvent(QWheelEvent*);

	/**  Called on double click */
	void mouseDoubleClickEvent(QMouseEvent*);
signals:
	/**
	 * Emited when coords change.
	 * Used with coord reader.
	 * \param x,y new coords
	 */
	void xyChanged(double x, double y);
private:
	int mouseX;
	int mouseY;
	ChartSettings    *settingsDialog;
	NativeCairoExport *nativeExport;
	XY *vchart;
};

#endif /* CHARTWIDGET_H */
