/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cairoexport.h"
#include "options.h"
#include "nplotnamespace.h"
#include <sstream>
#include <cmath>

using namespace std;
using namespace nplot;

CairoExport::CairoExport()
{
	canvas = NULL;
	FT_Library library;
	FT_Face ft_font;

	FT_Init_FreeType(&library);
	FT_New_Face(library, datadir "fonts/cmmi10.ttf", 0, &ft_font);
	cairo_mathfont = cairo_ft_font_face_create_for_ft_face(ft_font, 0);
	FT_New_Face(library, datadir "fonts/cmsy10.ttf", 0, &ft_font);
	cairo_symbolfont = cairo_ft_font_face_create_for_ft_face(ft_font, 0);
	FT_New_Face(library, datadir "fonts/msbm10.ttf", 0, &ft_font);
	cairo_symbol2font = cairo_ft_font_face_create_for_ft_face(ft_font, 0);
}

void CairoExport::drawPoint(double x, double y, point_shape type, int size)
{
	if (!canvas)
		return;

	cairo_set_dash(canvas, NULL, 0, 0.0);
	cairo_set_line_width(canvas, 1.0);
	switch (type) {
	case shp_x:
		cairo_move_to(canvas, x - size, y - size);
		cairo_line_to(canvas, x + size, y + size);
		cairo_move_to(canvas, x + size, y - size);
		cairo_line_to(canvas, x - size, y + size);
		cairo_stroke(canvas);
		break;
	case shp_plus:
		cairo_move_to(canvas, x - size, y);
		cairo_line_to(canvas, x + size, y);
		cairo_move_to(canvas, x, y - size);
		cairo_line_to(canvas, x, y + size); 
		cairo_stroke(canvas);
		break;
	case shp_circle:
		cairo_arc(canvas, x, y, size, 0, 360);
		cairo_stroke(canvas);
		break;
	case shp_fcircle:
		cairo_arc(canvas, x, y, size, 0, 360);
		cairo_fill(canvas);
		break;
	case shp_square:
		cairo_move_to(canvas, x - size, y - size);
		cairo_line_to(canvas, x + size, y - size); 
		cairo_line_to(canvas, x + size, y + size);
		cairo_line_to(canvas, x - size, y + size); 
		cairo_close_path(canvas);
		cairo_stroke(canvas);
		break;
	case shp_fsquare:
		cairo_move_to(canvas, x - size, y - size);
		cairo_line_to(canvas, x + size, y - size); 
		cairo_line_to(canvas, x + size, y + size);
		cairo_line_to(canvas, x - size, y + size);
		cairo_close_path(canvas);
		cairo_fill(canvas);
		break;
	case shp_diamond:
		cairo_move_to(canvas, x - size, y);
		cairo_line_to(canvas, x, y - size);
		cairo_line_to(canvas, x + size, y);
		cairo_line_to(canvas, x, y + size);
		cairo_close_path(canvas);
		cairo_stroke(canvas);
		break;
	case shp_fdiamond:
		cairo_move_to(canvas, x - size, y);
		cairo_line_to(canvas, x, y - size);
		cairo_line_to(canvas, x + size, y);
		cairo_line_to(canvas, x, y + size);
		cairo_close_path(canvas);
		cairo_fill(canvas);
	default:
		break;
	}
}

void CairoExport::drawLine(double x1, double y1,
                           double x2, double y2,
                           double thick, line_dash ldash)
{
	if (!canvas)
		return;

	double pattern[8];
	double *p = pattern;
	double l = 6.0 * thick;
	double s = 3.0 * thick;
	switch (ldash) {          /* XXX: UGLY! */
	case ld_solid:
		*p = 0;
		break;
	case ld_dash:
		*p++ = l;
		*p++ = s;
		break;
	case ld_dot:
		*p++ = thick;
		*p++ = s;
		break;
	case ld_dash_dot:
		*p++ = l;
		*p++ = s;
		*p++ = thick;
		*p++ = s;
		break;
	case ld_dash_dot_dot:
		*p++ = l;
		*p++ = s;
		*p++ = thick;
		*p++ = s;
		*p++ = thick;
		*p++ = s;
	default:
		break;
	} 

	cairo_set_dash(canvas, pattern, p - pattern, 0.0);
	cairo_set_line_width(canvas, thick);
	cairo_move_to(canvas, x1, y1);
	cairo_line_to(canvas, x2, y2);
	cairo_stroke(canvas);
}

double CairoExport::drawText(double x, double y, const char *text)
{
	if (!canvas)
		return 0;

	if (!textMeasure) {
		cairo_save(canvas);
		translate(x, y);
		cairo_show_text(canvas, text);
		cairo_stroke(canvas);
		cairo_restore(canvas);
	}
	cairo_text_extents_t text_ext;
	cairo_text_extents(canvas, text, &text_ext);
	if (text[strlen(text) - 1] == ' ')
		return text_ext.width + 2 * stringWidth(",");

	return text_ext.width + 1;
}

void CairoExport::setFont(font_face face, double size)
{
	if (!canvas)
		return;

	switch (face) {
	case times: 
		cairo_select_font_face(canvas, "Serif",
		                       CAIRO_FONT_SLANT_NORMAL, 
		                       CAIRO_FONT_WEIGHT_NORMAL);
		break;
	case math:
		cairo_set_font_face(canvas, cairo_mathfont);
		break;
	case symbol:
		cairo_set_font_face(canvas, cairo_symbolfont);
		break;
	case symbol2:
		cairo_set_font_face(canvas, cairo_symbol2font);
		break;
	}
	cairo_set_font_size(canvas, size);
}

void CairoExport::setColor(double r, double g, double b)
{
	cairo_set_source_rgb(canvas, r, g, b);
}

double CairoExport::stringWidth(const char *text)
{
	if (!canvas)
		return 0;

	cairo_text_extents_t text_ext;
	cairo_text_extents(canvas, text, &text_ext);
	return text_ext.width;
}

double CairoExport::getFontAscent()
{
	if (!canvas)
		return 0;

	cairo_font_extents_t font_ext;
	cairo_font_extents(canvas, &font_ext);
	
	return font_ext.ascent;
}

double CairoExport::getFontDescent()
{
	if (!canvas)
		return 0;

	cairo_font_extents_t font_ext;
	cairo_font_extents(canvas, &font_ext);
	
	return font_ext.descent;
}

double CairoExport::getFontHeight()
{
	if (!canvas)
		return 0;
	
	cairo_font_extents_t font_ext;
	cairo_font_extents(canvas, &font_ext);
	
	return font_ext.height;
}


void CairoExport::translate(double x, double y)
{
	if (!canvas)
		return;

	cairo_translate(canvas, x, y);
}

void CairoExport::rotate(double angle)
{
	if (!canvas)
		return;

	cairo_rotate(canvas, angle * M_PI / 180);
}

void CairoExport::moveto(double x, double y)
{
	cairo_move_to(canvas, x, y);
}

void CairoExport::lineto(double x, double y)
{
	cairo_line_to(canvas, x, y);
}

void CairoExport::stroke(double thick, line_dash ldash)
{
	if (!canvas)
		return;

	double  pattern[8];
	double *p = pattern;
	double  l = 6.0 * thick;
	double  s = 3.0 * thick;
	switch (ldash) {
	case ld_solid:
		*p = 0;
		break;
	case ld_dash:
		*p++ = l;
		*p++ = s;
		break;
	case ld_dot:
		*p++ = thick;
		*p++ = s;
		break;
	case ld_dash_dot:
		*p++ = l;
		*p++ = s;
		*p++ = thick;
		*p++ = s;
		break;
	case ld_dash_dot_dot:
		*p++ = l;
		*p++ = s;
		*p++ = thick;
		*p++ = s;
		*p++ = thick;
		*p++ = s;
	default:
		break;
	} 

	cairo_set_dash(canvas, pattern, p - pattern, 0.0);
	cairo_set_line_width(canvas, thick);
	cairo_stroke(canvas);
}

int CairoExport::width()
{
	return IMG_WIDTH;
}

int CairoExport::height()
{
	return IMG_HEIGHT;
}

void CairoExport::setClipRect(double x1, double y1, double x2, double y2)
{
	cairo_reset_clip(canvas);
	cairo_move_to(canvas, x1, y1);
	cairo_line_to(canvas, x2, y1);
	cairo_line_to(canvas, x2, y2);
	cairo_line_to(canvas, x1, y2);
	cairo_close_path(canvas);
	cairo_clip(canvas);
}


