/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CAIROEXPORT_H
#define CAIROEXPORT_H

#include "export.h"
#include "nplotnamespace.h"
#include <cairo.h>
#include <cairo-ft.h>
#include <ft2build.h>
#include FT_FREETYPE_H
#include <string>

using namespace std;
using namespace nplot;

/**
 *   Base class for cairo exports.
 */
class CairoExport: public Export
{
public:
	/**
	 *   A constructor.
	 */
	CairoExport();

	/** A destructor. */
	virtual ~CairoExport() {};
	void drawPoint(double x, double y, point_shape type, int size);
	void drawLine(double x1, double y1, double x2, double y2, double thick,
	              line_dash ldash);
	double drawText(double x, double y, const char *text);
	void setColor(double r, double g, double b);
	void translate(double x, double y);
	void rotate(double angle);
	void moveto(double x, double y);
	void lineto(double x, double y);
	void stroke(double thick, line_dash ldash);
	int width();
	int height();
	void setFont(font_face face, double size);
	double stringWidth(const char *text);
	double getFontAscent();
	double getFontDescent();
	double getFontHeight();
	void setClipRect(double x1, double y1, double x2, double y2);
protected:
	cairo_t *canvas;             /**< Cairo drawing context */
private:
	/**  cmmi font */
	cairo_font_face_t *cairo_mathfont;
	/**  cmsy font */
	cairo_font_face_t *cairo_symbolfont;
	/**  msbm font */
	cairo_font_face_t *cairo_symbol2font;
};

#endif /* CAIROEXPORT_H */
