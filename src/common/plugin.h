/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PLUGIN_H
#define PLUGIN_H

#include "export.h"
#include "types.h"

#undef BEGIN_C_DECLS
#undef END_C_DECLS
#ifdef __cplusplus
# define BEGIN_C_DECLS extern "C" {
# define END_C_DECLS }
#else
# define BEGIN_C_DECLS /* empty */
# define END_C_DECLS /* empty */
#endif

struct _plugin_info {
	const char *name_generic;
	const char *name;
	const char *provides;
};

struct _export_info {
	Export* (*instantiate)(const char *fname, int width, int height);
	bool ask_dim;
	const char *name_abbrev;
	const char *name_full;
	const char *fext;
};

struct _datatype_info {
	Variant (*fromString)(const char *str);
	void (*toString)(Variant v, char *buf, int buf_len);
	data_type primitive;
	const char *name_generic;
	const char *name;
};

struct _exported_symbol {
	const char *type;
	const char *symbol;
};

typedef struct _plugin_info plugin_info;
typedef struct _export_info export_info;
typedef struct _datatype_info datatype_info;
typedef struct _exported_symbol exported_symbol;
	
#endif /* PLUGIN_H */
