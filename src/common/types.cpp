/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "types.h"
#include <vector>
#include <stdlib.h>

#define MAX(a,b) (a > b)? a : b

using namespace std;

/*****************************V A R I A N T *********************************/

Variant::Variant()
 : ptr(NULL)
{
}

Variant::Variant(double d)
{
	ptr = malloc(MAX(sizeof(int), sizeof(double)));
	*((double*) ptr) = d;
}

Variant::Variant(int i)
{
	ptr = malloc(MAX(sizeof(int), sizeof(double)));
	*((int*) ptr) = i;
}

Variant::Variant(const Variant &rhs)
{
	if (rhs.good()) {
		ptr = malloc(MAX(sizeof(int), sizeof(double)));
		*((double*) ptr) = *((double*) rhs.ptr);
	} else {
		ptr = NULL;
	}
}

Variant::~Variant()
{
	if (ptr)
		free(ptr);
}

double Variant::toDouble() const
{
	if (!ptr)
		return 0;
	return *((double*) ptr);
}

int Variant::toInt() const
{
	if (!ptr)
		return 0;
	return *((int*) ptr);
}

void Variant::setValue(double d)
{
	ensureGood();
	*((double*) ptr) = d;
}

void Variant::setValue(int i)
{
	ensureGood();
	*((int*) ptr) = i;
}

bool Variant::good() const
{
	return !(ptr == NULL);
}

const Variant& Variant::operator=(const Variant &rhs)
{
	if (rhs.good()) {
		ensureGood();
		*((double*) ptr) = *((double*) rhs.ptr);
	} else if (good()) {
		free(ptr);
		ptr = NULL;
	}
	return rhs;
}

const double Variant::operator=(const double rhs)
{
	ensureGood();
	*((double*) ptr) = rhs;
	return rhs;
}

const int Variant::operator=(const int rhs)
{
	ensureGood();
	*((int*) ptr) = rhs;
	return rhs;
}

void Variant::ensureGood()
{
	if (!ptr)
		ptr = malloc(MAX(sizeof(int), sizeof(double)));	
}

/****************************** C O L O R ***********************************/

Color::Color(double _r, double _g, double _b)
 : r(_r), g(_g), b(_b), magic_number(0)
{
	if (r < 0)
		r = 0;
	else if (r > 1)
		r = 1;

	if (g < 0)
		g = 0;
	else if (g > 1)
		g = 1;

	if (b < 0)
		b = 0;
	else if (b > 1)
		b = 1;
}

Color::Color(string s)
 : magic_number(0)
{
	vector<double> c;
	string token;
	for (string::iterator it = s.begin(); it != s.end(); ++it) {
		if (*it != ':' && *it != ' ') {
			token += *it;
			continue;
		}
		c.push_back(atof(token.c_str()));
		token.clear();
	}
	if (token != "")
		c.push_back(atof(token.c_str()));
	if (c.size() == 3) {
		r = c.at(0);
		g = c.at(1);
		b = c.at(2);
	} else {
		r = 0;
		g = 0;
		b = 0;
	}
}

Color::Color()
 : r(0), g(0), b(0), magic_number(0)
{
}

string Color::str() const
{
	stringstream s;
	s << red() << ':' << green() << ':' << blue();
	return s.str();
}

Color& Color::first()
{
	Color *c = new Color(1, 0, 0);
	return *c;
}

const Color& Color::next() const
{
	int mn = magic_number + 1;
	double c[3];
	int rest = mn % 3;
	int div  = (mn / 3) % 3;
	c[rest] = 1.0;
	if (div != 0)
		c[(rest + div) % 3] = 0.5;

	Color *nc = new Color(c[0], c[1], c[2]);
	(*nc).magic_number = mn;
	return *nc;
}

ostream& operator<<(ostream &s, const Color &c)
{
	s << '\"' << c.red() << " " << c.green() << " " << c.blue() << '\"';
	return s;
}

/***************************** M A T R I X **********************************/

Matrix::Matrix(const vector< vector<double> > &mtrx)
 : matrix(mtrx)
{
}

Matrix::Matrix(int rows, int cols)
{
	for (int i = 0; i < rows; ++i)
		matrix.push_back(vector<double>(cols));
}

Matrix::Matrix()
{
}

double Matrix::det() const
{
	/*
	 *   Calculating determinant with Gauss elimination method
	 */

	unsigned int i, j, k;
	double det = 1;
	double d;
	if (matrix.size() != matrix[0].size())
		return 0;
	if (matrix.size() == 0)
		return 0;
	if (matrix.size() == 1)
		return matrix[0][0];
	
	vector< vector<double> > m = matrix;
	for (i = 0; i < m.size(); ++i) {
		if (m[i][i] == 0) {
			for (j = i; j < m.size(); ++j) {
				if (m[j][i] == 0)
					continue;
				vector<double> swap(m.size());
				swap = m[i];
				m[i] = m[j];
				m[j] = swap;
				det *= -1;
			}
		}
		if (m[i][i] == 0)
			return 0;
		det *= m[i][i];
		d    = m[i][i];
		for (j = 0; j < m[i].size(); ++j)
			m[i][j] /= d;
		for (j = i + 1; j < m.size(); ++j) {
			d = m[j][i];
			for (k = i; k < m.size(); ++k)
				m[j][k] -= m[i][k] * d;
		}
	}

	return det;
}

double Matrix::at(int i, int j) const
{
	return matrix[i][j];
}

double &Matrix::at(int i, int j)
{
	return matrix[i][j];
}

unsigned int Matrix::rows() const
{
	return matrix.size();
}

unsigned int Matrix::columns() const
{
	return matrix[0].size();
}

void Matrix::operator=(const Matrix &m)
{
	matrix = m.matrix;
}

ostream& operator<<(ostream &s, const Matrix &m) {
	for (unsigned int i = 0; i < m.rows(); ++i) {
		for (unsigned int j = 0; j < m.columns(); ++j)
			s << m.at(i, j) << " ";
		s << endl;
	}
	return s;
}
