/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TYPES_H
#define TYPES_H

#include <sstream>
#include <string>
#include <iostream>
#include <vector>

using namespace std;

class Variant
{
public:
	Variant(double d);
	Variant(int i);
	Variant();
	Variant(const Variant &rhs);
	~Variant();
	double toDouble() const;
	int toInt() const;
	void setValue(double d);
	void setValue(int i);
	bool good() const;
	const Variant& operator=(const Variant &rhs);
	const double operator=(const double rhs);
	const int operator=(const int rhs);
private:
	void ensureGood();
	void *ptr;
};

/**
 *   Class holding a color.
 */
class Color
{
public:
	/**
	 *   A constructor.
	 *   \param r Red component
	 *   \param g Green component
	 *   \param b Blue component
	 */
	Color(double r, double g, double b);

	/**
	 *   Alternative constructor
	 *   \param s string containing 3 numbers coresponding to RGB components 
	 *   respectively
	 */
	Color(string s);

	/**  An empty constructor creating by default a black color. */
	Color();

	/** A destructor */
	~Color() {};

	/** Returns red component */
	double red() const   { return r; }

	/** Returns blue component */
	double green() const { return g; }

	/** Returns blue component */
	double blue() const  { return b; }

	string str() const;         /**< Returns string with tree components */

	static Color& first();

	const Color& next() const;
private:
	double r;                   /**< Holds red component */
	double g;                   /**< Holds green component */
	double b;                   /**< Holds blue component */
	int    magic_number;
};

/** Operator extracting Color components to a ostream */
ostream& operator<<(ostream&, const Color&);

/**
 *   Class implementing matrix to calculate determinants.
 */
class Matrix
{
public:
	/**
	 *   A constructor creating a matrix from an array.
	 *   \param array Two dimensional array defining matrix.
	 */
	Matrix(const vector< vector<double> > &array);

	/**
	 *   A constructor creating a zero matrix.
	 *   \param rows number of rows of a matrix to create
	 *   \param rows number of columns of a matrix to create
	 */
	Matrix(int rows, int cols);

	/**
	 *   A default constructor.
	 */
	Matrix();

	/**  A destructor */
	~Matrix() {};

	/**
	 *   Calculates determinant.
	 *   \returns Determinant of the matrix.
	 */
	double det() const;

	/**
	 *   Gets an element of a matrix.
	 *   \param i element row
	 *   \param j element column
	 *   \returns matrix element (i, j)
	 */
	double at(int i, int j) const;

	/**
	 *    Method for changing elements of a matrix
	 *   \param i element row
	 *   \param j element column
	 *   \returns reference to a matrix element (i, j)
	 */
	double &at(int i, int j);

	unsigned int rows() const;       /**< Returns matrix row count */
	
	unsigned int columns() const;    /**< Returns matrix column count */

	/**
	 *   Assignment operator.
	 *   \param m right hand matrix
	 */
	void operator=(const Matrix &m);
private:
	vector< vector<double> > matrix; /**< Holds two dimensional array.*/
};

/** Operator extracting Matrix to a ostream */
ostream& operator<<(ostream&, const Matrix&);

#endif /* TYPES_H */
