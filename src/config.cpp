/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fstream>
#include <iostream>
#include "config.h"
#include "options.h"
#include "nplotnamespace.h"

using namespace std;
using namespace nplot;

KeywordParser::KeywordParser(string s)
{
	parse(s);
}

bool KeywordParser::parse(string s)    /* Parse a "keyword=value" string */
{
	string token;
	bool keyword = true;
	int state = cps_normal;

	for (string::iterator it = s.begin(); it != s.end(); ++it) {
		switch(state) {
		case cps_normal:
			if (*it == '\"') {
				state = cps_safemode;
			} else if (*it == '=') {
				if (token.size() == 0) {
					state = cps_skipline;
					break;
				}
				keyword = false;
				keywords.push_back(token);
				token.clear();
			} else if (*it == '\n') {
				if (keyword == true)
					continue;   /* omitting empty lines */
				keyword = true;
				values.push_back(token);
				token.clear();
			} else if (*it != ' ')
				token += *it;
			break;
		case cps_safemode:
			if (*it == '\"')
				state = cps_normal;
			else if (*it == '\\')
				state = cps_escape;
			else
				token += *it;
			break;
		case cps_escape:              /* Unescape */
			switch (*it) {
			case 'n':
				token += '\n';
				break;
			default:
				token += *it;
			}
			state = cps_safemode;
			break;
		case cps_skipline:
			if (*it == '\n')
				state = cps_normal;
			break;
		}
	}

	if (token.size() > 0) 
		keyword? keywords.push_back(token) : values.push_back(token);
	if (keywords.size() != values.size()) {
		keywords.clear();
		values.clear();
		return false;
	}
	return true;
}

string KeywordParser::getVar(string key)
{
	if (keywords.size() == 0 || values.size() == 0)
		return "";

	vector<string>::iterator keys = keywords.begin();
	vector<string>::iterator vals = values.begin();
	while (*keys != key) {
		if (++keys == keywords.end() || ++vals == values.end())
			return string();
	}
	return *vals;
}

void KeywordParser::setVar(string key, string val)
{
	vector<string>::iterator keys = keywords.begin();
	vector<string>::iterator vals = values.begin();
	/* Its somewhat crappy, but it works! */
	do {
		if (keys == keywords.end() || vals++ == values.end()) {
			keywords.push_back(key);
			values.push_back(val);
			return;
		}
	} while (*(keys++) != key);
	*(--vals) = val;
}

bool KeywordParser::varExists(string key)
{
	vector<string>::iterator keys = keywords.begin();
	for (keys = keywords.begin(); keys != keywords.end(); ++keys) {
		if ((*keys) == key)
			return true;
	}
	return false;
}

Cfg::Cfg()
{
	string str;
	pointer = this;
	char *home = homedir;
	str += home;
	str += cfgfile;
	readFile(str.c_str());
}

Cfg::~Cfg()
{
	pointer = 0;
}

Cfg* Cfg::pointer = 0;

bool Cfg::save()
{
	string str;
	char *home = homedir;
	str += home;
	str += cfgfile;
	return writeFile(str.c_str());
}

/* I'm not convinced this is a most effective way of reading a file.
 * Should be changed someday */
bool Cfg::readFile(const char* name)
{
	ifstream infile(name);
	string s;
	char c;
	if (!infile.good())
		return false;
	while ((c = infile.get()) != -1)
		s += c;
	return parse(s);
}

bool Cfg::writeFile(const char *name)
{
	ofstream outfile(name);
	if (!outfile.good())
		return false;
	if (keywords.size() != values.size())
		return false;

	vector<string>::iterator key = keywords.begin();
	vector<string>::iterator val = values.begin();
	while (key != keywords.end())
		outfile << *key++ << "=" << *(val++) << '\n';
	outfile << endl;
	outfile.close();
	return true;
}

Cfg* Cfg::instance()
{
	if (pointer == 0)
		pointer = new Cfg();
	return
		pointer;
}

void Cfg::destroy()
{
	if (pointer != 0)
		delete pointer;
}

string Cfg::escape(string s)
{
	string output;
	for (string::iterator it = s.begin(); it != s.end(); ++it) {
		switch (*it) {
		case '\"':
			output += "\\\"";
			break;
		case '\n':
			output += "\\n";
			break;
		case '\\':
			output += "\\\\";
			break;
		default:
			output += *it;
		}
	}
	return output;
}

