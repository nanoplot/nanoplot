/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2007, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONFIG_H
#define CONFIG_H

#include <vector>
#include <string>

using namespace std;

/**
 *   Class providing key=value parsing functionality.
 */
class KeywordParser
{
public:
	/**
	 *   A constructor.
	 *   \param s String to be parsed
	 */
	KeywordParser(string s);

	/**  An empty constructor */
	KeywordParser() {};
	
	/**  A destructor */
	~KeywordParser() {};

	/**
	 *   Get value of a variable.
	 *   \param key Key
	 *   \return Value (string) of the coresponding key
	 */
	string getVar(string key);

	/**
	 *   Set value of a variable.
	 *   \param key Key
	 *   \param value Value to be set
	 */
	void setVar(string key, string value);

	bool varExists(string key);
protected:
	/**
	 *   Parse a string.
	 *   \param s String to be parsed
	 *   \return \c true if parsing was successful, \c false otherwise
	 */
	bool parse(string s);
	vector<string> keywords;          /**< Holds vector of keywords */
	vector<string> values;            /**< Holds vector of values */
};

/**
 *   Config parser class.
 */
class Cfg: public KeywordParser
{
public:
	Cfg();                            /**< A constructor */
	~Cfg();                           /**< A destructor */
	static Cfg* instance();           /**< Returns an instance of Cfg */
	static void destroy();            /**< Destroys existing instance of Cfg */

	/**
	 *   Escape given string.
	 *   \param s String to be escaped
	 *   \return Escaped string
	 */
	static string escape(string s);

	/**
	 *   Save config.
	 *   \return \c true if saving was successful, \c false otherwise
	 */
	bool save();

	/**
	 *   Read file
	 *   \param name Name of file to be read
	 *   \return \c true if reading and parsing ware successfull, 
	 *   \c false otherwise
	 */
	bool readFile(const char *name);

	/**
	 *   Write file.
	 *   \param name Name of file to be read
	 *   \return \c true if writing was successful, \c false otherwise
	 */
	bool writeFile(const char *name);
private:
	/**  Holds pointer to existing instance of Cfg or 0, if none exists */
	static Cfg* pointer;
};

#endif /* CONFIG_H */
