/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2010, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "datareader.h"
#include <sstream>
#include <iomanip>

using namespace std;

DataReader::DataReader(ChartWidget *_widget, QWidget *parent): QWidget(parent)
{
	widget = _widget;
	canvas = new QPainter;
	mode = 0;

	drfont = new QFont("times", 14);
	QFontMetrics fm = QFontMetrics(*drfont);

	setFixedHeight(fm.height());
	setMouseTracking(true);
	hide();
}

DataReader::~DataReader()
{
	delete drfont;
	delete canvas;
}

void DataReader::hide()
{
	mode = 0;
	QWidget::hide();
}

void DataReader::setMode(int m)
{
	mode = m;
	show();
	repaint();
}

int DataReader::getMode()
{
	return mode;
}

void DataReader::setXY(double _x, double _y)
{
	x = _x;
	y = _y;
	repaint();
}

void DataReader::paintEvent(QPaintEvent*)
{
	canvas->begin(this);

	canvas->fillRect(0, 0, width(), height(), QBrush(Qt::lightGray));

	canvas->setFont(*drfont);
	QFontMetrics fm = QFontMetrics(*drfont);
	if (mode == 1) {
		stringstream s;
		s.setf(ios::showpoint);
		s << setprecision(6)
		  << "X=" << widget->chart()->xAxis->toString(x)
		  << "; Y=" << widget->chart()->yAxis->toString(y);
		canvas->drawText(10, fm.ascent(), s.str().c_str());
	} else if (mode == 2) {
		canvas->drawText(10, fm.ascent(),
		             QString(widget->chart()->closestDataPoint(x, y)));
	}

	canvas->end();
}

