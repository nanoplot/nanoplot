/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DATAREADER_H
#define DATAREADER_H

#include <QtGui>
#include "chartwidget.h"

class ChartWidget;

/**
 *   Widget providing data reader and coord reader functionality.
 */
class DataReader: public QWidget
{
Q_OBJECT
public:
	/**
	 *   A constructor.
	 *   \param widget Pointer to ChartWidget
	 *   \param parent Pointer to parent widget
	 */
	DataReader(ChartWidget *widget, QWidget *parent = 0);
	~DataReader();                     /**< A destructor */
	void hide();                       /**< Hides reader */
public slots:
	/**
	 *   Sets mode (data or coord reading).
	 *   \param mode Mode to be set; 0 - hidden, 1 - coord reading,
	 *   2 - data reading 
	 */
	void setMode(int mode);
	int getMode();                     /**< Returns mode */
	void setXY(double x, double y);    /**< Sets coords */
protected slots:
	void paintEvent(QPaintEvent*); /**< Called when repaint is necessary */
private:
	int mode;                          /**< Holds mode */
	double x;                          /**< Holds X coord */
	double y;                          /**< Holds Y coord */
	QPainter *canvas;              /**< Holds pointer to painting canvas */
	QFont *drfont;                     /**< Holds font */
	ChartWidget *widget;               /**< Holds pointer to ChartWidget */
};

#endif /* DATAREADER_H */
