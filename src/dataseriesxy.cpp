/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dataseriesxy.h"
#include "config.h"

DataSeriesXY::DataSeriesXY(const char *nm,
                           const Array &_data, 
                           int _type,
                           XY *parent)
 : array(_data), dstype((elem_type) _type), xychart(parent)
{
	Init();
	name = nm;
}

DataSeriesXY::DataSeriesXY(SessionReader *reader, XY *parent)
{
	Init();
	xychart = parent;

	name      = reader->getProperty("name");
	vcolor    = Color(reader->getProperty("vcolor"));
	symbols   = reader->getPropertyBool("symbols");
	connected = reader->getPropertyBool("connected");
	labels    = reader->getPropertyBool("labels");
	shape     = (point_shape) reader->getPropertyInt("shape");
	size      = reader->getPropertyInt("size");
	lineThick = reader->getPropertyDouble("lineThick");
	vdash     = (line_dash) reader->getPropertyInt("vdash");
	lbltype   = reader->getPropertyInt("lbltype");
	dstype    = (elem_type) reader->getPropertyInt("dstype");
	
	reader->processChildren();

	for (const char *name = reader->nextNode(); name;
	     name = reader->nextNode())
	{
		if (strcmp(name, "Array") == 0) {
			array.load(reader);
		}
	}
}

void DataSeriesXY::Init()
{
	symbols   = true;
	connected = false;
	labels    = false;
	shape     = shp_x;
	size      = 2;
	vdash     = ld_solid;
	lineThick = 1;
	lbltype   = 0;
	vcolor    = Color(0, 0, 0);
}

void DataSeriesXY::save(SessionWriter *writer) const
{
	writer->startElement("DataSeriesXY");
	writer->setProperty("name", name.c_str());
	writer->setProperty("vcolor", vcolor.str().c_str());
	writer->setProperty("symbols", symbols);
	writer->setProperty("connected", connected);
	writer->setProperty("labels", labels);
	writer->setProperty("shape", shape);
	writer->setProperty("size", size);
	writer->setProperty("lineThick", lineThick);
	writer->setProperty("vdash", vdash);
	writer->setProperty("lbltype", lbltype);
	writer->setProperty("dstype", dstype);

	array.save(writer);

	writer->endElement();
}

void DataSeriesXY::repaint(Export *exp) const
{
	if (hidden)
		return;

	point_shape tmpshape = shape; 
	int tmplbltype       = lbltype;
	
	if (!symbols)
		tmpshape = shp_unknown;
	
	if (!labels)
		tmplbltype = -1;
	
	exp->setColor(vcolor.red(), vcolor.green(), vcolor.blue());

	for (int i = 0; i < array.rows(); ++i)
		drawPoint(i, tmpshape, tmplbltype, exp);
	
	if (connected) {
		exp->moveto(xychart->wdwX(array.toDouble(0, 0), exp->width()),
		            xychart->wdwY(array.toDouble(1, 0), exp->height()));
		for (int i = 1; i < array.rows(); ++i) {
			exp->lineto(xychart->wdwX(array.toDouble(0, i),
			                          exp->width()),
			            xychart->wdwY(array.toDouble(1, i),
			                          exp->height()));
		}
		exp->stroke(lineThick, vdash);
	}
}

void DataSeriesXY::drawPoint(int i, point_shape _shape, int _lbltype,
                             Export *exp) const
{
	xychart->drawPoint(array.toDouble(0, i), array.toDouble(1, i), _shape,
	                   size, _lbltype, exp);

	if ((dstype & elem_xydx) != elem_xy)
		drawDXBar(i, 2, exp);
	
	if (dstype == elem_xydy)
		drawDYBar(i, 2, exp);
	else if (dstype == elem_xydxdy)
		drawDYBar(i, 3, exp);
}

void DataSeriesXY::drawDXBar(int i, int col, Export *exp) const
{
	double w  = exp->width();
	double h  = exp->height();
	double rx = array.toDouble(0, i);
	double dx = array.toDouble(col, i);
	double x1 = xychart->wdwX(rx - dx, w);
	double x2 = xychart->wdwX(rx + dx, w);
	double y  = xychart->wdwY(array.toDouble(1, i), h);
	exp->moveto(x1, y);
	exp->lineto(x2, y);
	exp->stroke(1.);
	exp->moveto(x1, y - 3);
	exp->lineto(x1, y + 3);
	exp->stroke(1.);
	exp->moveto(x2, y - 3);
	exp->lineto(x2, y + 3);
	exp->stroke(1.);
}

void DataSeriesXY::drawDYBar(int i, int col, Export *exp) const
{
	double w  = exp->width();
	double h  = exp->height();
	double x  = xychart->wdwX(array.toDouble(0, i), w);
	double ry = array.toDouble(1, i);
	double dy = array.toDouble(col, i);
	double y1 = xychart->wdwY(ry - dy, h);
	double y2 = xychart->wdwY(ry + dy, h);
	exp->moveto(x, y1);
	exp->lineto(x, y2);
	exp->stroke(1.0);
	exp->moveto(x - 3, y1);
	exp->lineto(x + 3, y1);
	exp->stroke(1.0);
	exp->moveto(x - 3, y2);
	exp->lineto(x + 3, y2);
	exp->stroke(1.0);
}

nplot::elem_type DataSeriesXY::type() const      { return dstype; }

void DataSeriesXY::setColor(const Color &c)      { vcolor = c; }

Color DataSeriesXY::color() const                { return vcolor; }

void DataSeriesXY::setSymbolsEnabled(bool s)     { symbols = s; }

bool DataSeriesXY::isSymbolsEnabled() const      { return symbols; }

void DataSeriesXY::setConnectedEnabled(bool c)   { connected = c; }

bool DataSeriesXY::isConnectedEnabled() const    { return connected; }

void DataSeriesXY::setLabelsEnabled(bool l)      { labels = l; }

bool DataSeriesXY::isLabelsEnabled() const       { return labels; }

void DataSeriesXY::setSymbolShape(point_shape s) { shape = s; }

point_shape DataSeriesXY::symbolShape() const    { return shape; }

void DataSeriesXY::setSymbolSize(int s)          { size = s; }

int DataSeriesXY::symbolSize() const             { return size; }

void DataSeriesXY::setDash(line_dash _vdash)     { vdash = _vdash; }

line_dash DataSeriesXY::dash() const             { return vdash; }

void DataSeriesXY::setLineThickness(double t)    { lineThick = t; }

double DataSeriesXY::lineThickness() const       { return lineThick; }

void DataSeriesXY::setLabelType(int l)           { lbltype = l; }

int DataSeriesXY::labelType() const              { return lbltype; }

void DataSeriesXY::setData(Array _data)          { array = _data; }

vector<double> DataSeriesXY::x() const       { return array.columnDouble(0); }

vector<double> DataSeriesXY::y() const       { return array.columnDouble(1); }

Array DataSeriesXY::getData() const { return array; }

