/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DATASERIESXY_H
#define DATASERIESXY_H

#include <sstream>
#include <vector>
#include "array.h"
#include "chartelement.h"
#include "export.h"
#include "nplotnamespace.h"
#include "types.h"
#include "sessionreader.h"
#include "xy.h"

class XY;

using namespace std;
using namespace nplot;

/**
 *   Class used for visualization of data sets.
 */
class DataSeriesXY: public ChartElement
{
public:
	/** 
	 *   A constructor.
	 *   \param name name of the element
	 *   \param data Array containing data
	 *   \param type type of element \sa nplot::elem_type
	 *   \param xy pointer to parent XY widget
	 */
	DataSeriesXY(const char *name, 
	             const Array &data, 
	             int type,
	             XY *xy);
	/**
	 *   A constructor creating data set using information from a session
	 *   file.
	 *   \param reader session reader
	 *   \param xy pointer to parent XY widget
	 */
	DataSeriesXY(SessionReader *reader, XY *xy);
        
	/**  A destructor */
	virtual ~DataSeriesXY() {};
	void save(SessionWriter *writer) const;
	virtual void repaint(Export *exp) const;
	virtual elem_type type() const;
	
	/**
	 *   Sets elements' color.
	 *   \param r red component
	 *   \param g green component
	 *   \param b blue component
	 */
	void setColor(double r, double g, double b);
	
	/**
	 *   Sets elements' color.
	 *   \param c Color describing elenent's new color
	 */
	void setColor(const Color &c);
	
	/**
	 *   Returns color of the element.
	 *   \return Color describing elements' color
	 */
	Color color() const;

	/**
	 *   Enables (or disables) symbols.
	 *   \param b if \c true, method will enable symbols; will disable them 
	 *   otherwise
	 */
	void setSymbolsEnabled(bool b);

	/**
	 *   Checks if symbols are enabled.
	 *   \return \c true if symbols are enabled, \c false otherwise
	 */
	bool isSymbolsEnabled() const;

	/**
	 *   Enables (or disables) connected plotting.
	 *   \param b if \c true, method will enable connected plotting; will 
	 *   disable it otherwise
	 */
	void setConnectedEnabled(bool b);

	/**
	 *   Checks if connected plotting is enabled.
	 *   \return \c true if connected plotting is enabled, \c false otherwise
	 */
	bool isConnectedEnabled() const;

	/**
	 *   Enables (or disables) points labels.
	 *   \param b if true, method will enable labels; will disable them 
	 *   otherwise
	 */
	void setLabelsEnabled(bool b);

	/**
	 *   Checks if points labels are enabled
	 *   \return \c true if labels are enabled, \c false otherwise
	 */
	bool isLabelsEnabled() const;

	/**
	 *   Sets symbols shape
	 *   \param s symbol shape
	 */
	void setSymbolShape(point_shape s);

	/** Returns symbol shape */
	point_shape symbolShape() const;

	/** Sets symbol size */
	void setSymbolSize(int s);

	/** Returns symbol size */
	int symbolSize() const;
	
	/**
	 *   Sets the dash pattern.
	 *   \param pattern dash pattern number
	 */
	void setDash(line_dash pattern);

	line_dash dash() const;                 /**< Returns dash pattern number */

	/**
	 *   Sets the line thickness.
	 *   \param thick line thickness
	 */
	void setLineThickness(double thick);
	
	double lineThickness() const;           /**< Returns line thickness */

	/** Sets label type */
	void setLabelType(int t);

	/** Returns label type */
	int labelType() const;

	/**
	 *   Sets data set.
	 *   \param data std::vector of columns of data
	 */
	void setData(Array data);

	/**
	 *   Returns X components of data.
	 *   \return std::vector containing X components of data
	 */
	vector<double> x() const;

	/**
	 *   Returns Y components of data.
	 *   \return std::vector containing Y components of data
	 */
	vector<double> y() const;

	/**
	 *   Returns whole data set.
	 *   \return std::vector containing columns of data
	 */
	Array getData() const;
protected:
	/**
	 *   Draws a single point.
	 *   \param i point number
	 *   \param shape shape of the point
	 *   \param lbltype label type of point
	 *   \param exp Export to be used
	 */
	virtual void drawPoint(int i, point_shape shape, int lbltype,
	                       Export *exp) const;

	/**
	 *   Draws X-error bar.
	 *   \param i point number
	 *   \param col column containing X-error
	 *   \param exp Export to be used
	 */
	virtual void drawDXBar(int i, int col, Export *exp) const;

	/**
	 *   Draws Y-error bar.
	 *   \param i point number
	 *   \param col column containing Y-error
	 *   \param exp Export to be used
	 */
	virtual void drawDYBar(int i, int col, Export *exp) const;

	Array array;
	bool symbols;                  /**< Holds symbols status flag */
	bool connected;                /**< Holds connected plotting status flag */
	bool labels;                   /**< Holds labels status flag */
	point_shape shape;             /**< Holds shape of point symbols */
	int size;                      /**< Holds size of point symbols */
	line_dash vdash;               /**< Holds dash pattern number */
	double lineThick;              /**< Holds line thickness */
	int lbltype;                   /**< Holds label type */
	Color vcolor;                  /**< Holds color of the element */
	elem_type dstype;              /**< Holds element type */
	XY *xychart;                   /**< Holds pointer to parent XY widget */
private:
	void Init();                   /**< Initializes needed variables */
};

#endif /* DATASERIESXY_H*/
