/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2010, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "options.h"
#include <gettext.h>
#include <glib.h>
#include "dialogs.h"
#include "license.h"
#include "napp.h"
#include "settingsdialog.h"

#define _(String) (gettext(String))

QString Dialogs::currentDir = QString("");

QString Dialogs::id_filename = QString("");

void Dialogs::errorMessage(nplot::fit_err err)
{
	switch (err) {
	case nplot::ferr_data:
		error(_("Error: Need more data to do the fitting!"));
		return;
	case nplot::ferr_domain:
		error(_("Error: Domain!"));
		return;
	default:
		;
	}
	return;
}

void Dialogs::error(const char *msg)
{
		QMessageBox::critical(NULL, _("Error!"), msg);
}

bool Dialogs::getRowsCols(int *rows, int *cols)
{
	bool ret = true;
	QDialog *crDialog = new QDialog;
	crDialog->setWindowTitle(_("Information"));

	QVBoxLayout *mainLayout = new QVBoxLayout;
	QGridLayout *gridLayout = new QGridLayout;
	QHBoxLayout *butLayout  = new QHBoxLayout;
	
	QLabel *rowsLabel = new QLabel(_("Rows:"));
	QSpinBox *rowsSpin = new QSpinBox;
	rowsSpin->setMinimum(2);
	rowsSpin->setMaximum(MAX_ROWS);
	rowsSpin->setValue(10);

	QLabel *colsLabel = new QLabel(_("Cols:"));
	QSpinBox *colsSpin = new QSpinBox;
	colsSpin->setMinimum(1);
	colsSpin->setMaximum(26);
	colsSpin->setValue(2);
	
	QPushButton *okButton = new QPushButton(_("&OK"));
	okButton->setDefault(true);
	connect(okButton, SIGNAL(clicked()), crDialog, SLOT(accept()));

	QPushButton *cancelButton = new QPushButton(_("&Cancel"));
	connect(cancelButton, SIGNAL(clicked()), crDialog, SLOT(reject()));

	gridLayout->addWidget(rowsLabel, 0,0);
	gridLayout->addWidget(rowsSpin, 0,1);
	gridLayout->addWidget(colsLabel, 1,0);
	gridLayout->addWidget(colsSpin, 1,1);

	butLayout->addWidget(okButton);
	butLayout->addWidget(cancelButton);

	mainLayout->addLayout(gridLayout);
	mainLayout->addLayout(butLayout);
	crDialog->setLayout(mainLayout);

	rowsSpin->setFocus();

	if (!crDialog->exec()) {
		ret = false;
	} else {
		*rows = rowsSpin->value();
		*cols = colsSpin->value();
	}
	delete crDialog;
	return ret;
}


bool Dialogs::getResolution(int *width, int *height)
{
	bool ret = true;
	QDialog *resDialog = new QDialog;
	resDialog->setWindowTitle(_("PNG resolution"));

	QVBoxLayout *mainLayout = new QVBoxLayout;
	QGridLayout *gridLayout = new QGridLayout;
	QHBoxLayout *butLayout  = new QHBoxLayout;
	
	QLabel *widthLabel = new QLabel(_("Width:"));
	QSpinBox *widthSpin = new QSpinBox;
	widthSpin->setMinimum(100);
	widthSpin->setMaximum(4096);
	widthSpin->setValue(1024);

	QLabel *heightLabel = new QLabel(_("Height:"));
	QSpinBox *heightSpin = new QSpinBox;
	heightSpin->setMinimum(100);
	heightSpin->setMaximum(4096);
	heightSpin->setValue(768);
	
	QPushButton *okButton = new QPushButton(_("&OK"));
	okButton->setDefault(true);
	connect(okButton, SIGNAL(clicked()), resDialog, SLOT(accept()));

	QPushButton *cancelButton = new QPushButton(_("&Cancel"));
	connect(cancelButton, SIGNAL(clicked()), resDialog, SLOT(reject()));

	gridLayout->addWidget(widthLabel, 0,0);
	gridLayout->addWidget(widthSpin, 0,1);
	gridLayout->addWidget(heightLabel, 1,0);
	gridLayout->addWidget(heightSpin, 1,1);

	butLayout->addWidget(okButton);
	butLayout->addWidget(cancelButton);

	mainLayout->addLayout(gridLayout);
	mainLayout->addLayout(butLayout);
	resDialog->setLayout(mainLayout);

	if (!resDialog->exec()) {
		ret = false;
	} else {
		*width = widthSpin->value();
		*height = heightSpin->value();
	}
	delete resDialog;
	return ret;
}

void Dialogs::settings()
{
	SettingsDialog *dialog = new SettingsDialog(MainWindow::pointer);
	dialog->show();
}

bool Dialogs::makeChartXY(int ncols, XY **xy, int *x, int *y, int *type)
{
	bool ret = true;
	QDialog *chartDialog = new QDialog;
	chartDialog->setWindowTitle(_("Make chart..."));

	QVBoxLayout *mainLayout = new QVBoxLayout;
	QGridLayout *gridLayout = new QGridLayout;
	QHBoxLayout *butLayout  = new QHBoxLayout;

	/* TRANSLATORS: Add to chart */
	QLabel *addtoLabel = new QLabel(_("Add to:"));
	QComboBox *addtoCombo = new QComboBox;
	addtoCombo->addItem(_("New plot"));
	for (list<XY*>::iterator it=MainWindow::xylist.begin(); 
			it!=MainWindow::xylist.end(); ++it)
	{
		addtoCombo->addItem((*it)->name());
	}

	QLabel *typeLabel = new QLabel(_("Plot type:"));
	QComboBox *typeCombo = new QComboBox;
	/* TRANSLATORS: Scatter plot */
	typeCombo->addItem(_("Scatter"));
	/* TRANSLATORS: Connected plot */
	typeCombo->addItem(_("Line"));

	QLabel *xLabel = new QLabel(_("X axis:"));
	QComboBox *xCombo = new QComboBox;
	xCombo->addItem(_("row index"));
	for (char c = 'A'; c < (char)('A' + ncols); ++c)
		xCombo->addItem(QString(c));
	xCombo->setCurrentIndex(1 % ncols);

	QLabel *yLabel = new QLabel(_("Y axis:"));
	QComboBox *yCombo = new QComboBox;
	yCombo->addItem(_("row index"));
	for (char c = 'A'; c < (char)('A' + ncols); ++c)
		yCombo->addItem(QString(c));
	yCombo->setCurrentIndex((1 % ncols) + 1);

	QPushButton *okButton = new QPushButton(_("&OK"));
	okButton->setDefault(true);
	connect(okButton, SIGNAL(clicked()), chartDialog, SLOT(accept()));

	QPushButton *cancelButton = new QPushButton(_("&Cancel"));
	connect(cancelButton, SIGNAL(clicked()), chartDialog, SLOT(reject()));

	gridLayout->addWidget(addtoLabel, 0,0);
	gridLayout->addWidget(addtoCombo, 0,1);
	gridLayout->addWidget(typeLabel, 1,0);
	gridLayout->addWidget(typeCombo, 1,1);
	gridLayout->addWidget(xLabel, 2,0);
	gridLayout->addWidget(xCombo, 2,1);
	gridLayout->addWidget(yLabel, 3,0);
	gridLayout->addWidget(yCombo, 3,1);

	butLayout->addWidget(okButton);
	butLayout->addWidget(cancelButton);

	mainLayout->addLayout(gridLayout);
	mainLayout->addLayout(butLayout);
	chartDialog->setLayout(mainLayout);

	if (!chartDialog->exec()) {
		ret = false;
	} else {
		int i = addtoCombo->currentIndex();    /* Checking the outcome */
		if (i == 0) {
			*xy = NULL;
		} else {
			for (list<XY*>::iterator it=MainWindow::xylist.begin();
					it!=MainWindow::xylist.end(); ++it) 
			{
				--i;
				if (i == 0) {
					*xy = *it; 
					break;
				}
			}
		}
		*x = xCombo->currentIndex();
		*y = yCombo->currentIndex();
		*type = typeCombo->currentIndex();
	}
	delete chartDialog;
	return ret;
}

bool Dialogs::makeChartXYD(int  ncols, int  ctype, XY **xy, int *x, int *y, 
                           int *d,     int *type)
{
	bool ret = true;
	QDialog *chartDialog = new QDialog;
	chartDialog->setWindowTitle(_("Make chart..."));

	QVBoxLayout *mainLayout = new QVBoxLayout;
	QGridLayout *gridLayout = new QGridLayout;
	QHBoxLayout *butLayout  = new QHBoxLayout;
	
	QLabel *addtoLabel = new QLabel(_("Add to:"));
	QComboBox *addtoCombo = new QComboBox;
	addtoCombo->addItem(_("New plot"));
	for (list<XY*>::iterator it=MainWindow::xylist.begin();
			it!=MainWindow::xylist.end(); ++it)
	{
		addtoCombo->addItem((*it)->name());
	}
	
	QLabel *typeLabel = new QLabel(_("Plot type:"));
	QComboBox *typeCombo = new QComboBox;
	typeCombo->addItem(_("Scatter"));
	typeCombo->addItem(_("Line"));

	QLabel *xLabel = new QLabel(_("X axis:"));
	QComboBox *xCombo = new QComboBox;
	xCombo->addItem(_("row index"));
	for (char c = 'A'; c < (char)('A' + ncols); ++c)
		xCombo->addItem(QString(c));
	xCombo->setCurrentIndex(1 % ncols);

	QLabel *yLabel = new QLabel(_("Y axis:"));
	QComboBox *yCombo = new QComboBox;
	yCombo->addItem(_("row index"));
	for (char c = 'A'; c < (char)('A' + ncols); ++c)
		yCombo->addItem(QString(c));
	yCombo->setCurrentIndex(1 % ncols + 1);

	QLabel *dLabel;
	if (ctype == nplot::elem_xydx) dLabel = new QLabel(_("DX:"));
	else dLabel = new QLabel(_("DY:"));
	QComboBox *dCombo = new QComboBox;
	for (char c = 'A'; c < (char)('A' + ncols); ++c)
		dCombo->addItem(QString(c));
	dCombo->setCurrentIndex(2 % ncols);

	QPushButton *okButton = new QPushButton(_("&OK"));
	okButton->setDefault(true);
	connect(okButton, SIGNAL(clicked()), chartDialog, SLOT(accept()));

	QPushButton *cancelButton = new QPushButton(_("&Cancel"));
	connect(cancelButton, SIGNAL(clicked()), chartDialog, SLOT(reject()));
	
	gridLayout->addWidget(addtoLabel, 0,0);
	gridLayout->addWidget(addtoCombo, 0,1);
	gridLayout->addWidget(typeLabel, 1,0);
	gridLayout->addWidget(typeCombo, 1,1);
	gridLayout->addWidget(xLabel, 2,0);
	gridLayout->addWidget(xCombo, 2,1);
	gridLayout->addWidget(yLabel, 3,0);
	gridLayout->addWidget(yCombo, 3,1);
	gridLayout->addWidget(dLabel, 4,0);
	gridLayout->addWidget(dCombo, 4,1);

	butLayout->addWidget(okButton);
	butLayout->addWidget(cancelButton);

	mainLayout->addLayout(gridLayout);
	mainLayout->addLayout(butLayout);
	chartDialog->setLayout(mainLayout);

	if (!chartDialog->exec()) {
		ret = false;
	} else {
		int i = addtoCombo->currentIndex();
		if (i == 0) {
			*xy = NULL;
		} else {
			for (list<XY*>::iterator it=MainWindow::xylist.begin(); 
					it!=MainWindow::xylist.end(); ++it) 
			{
				--i;
				if (i == 0) {*xy = *it; break;}
			}
		}
		*x = xCombo->currentIndex();
		*y = yCombo->currentIndex();
		*d = dCombo->currentIndex();
		*type = typeCombo->currentIndex();
	}
	delete chartDialog;
	return ret;
}

bool Dialogs::makeChartXYD2(int ncols, XY **xy,
                            int *x, int *y,
                            int *dx, int *dy,
                            int *type)
{
	bool ret = true;
	QDialog *chartDialog = new QDialog;
	chartDialog->setWindowTitle(_("Make chart..."));

	QVBoxLayout *mainLayout = new QVBoxLayout;
	QGridLayout *gridLayout = new QGridLayout;
	QHBoxLayout *butLayout  = new QHBoxLayout;

	QLabel *addtoLabel = new QLabel(_("Add to:"));
	QComboBox *addtoCombo = new QComboBox;
	addtoCombo->addItem(_("New plot"));
	for (list<XY*>::iterator it=MainWindow::xylist.begin(); 
			it!=MainWindow::xylist.end(); ++it)
	{
		addtoCombo->addItem((*it)->name());
	}

	QLabel *typeLabel = new QLabel(_("Plot type:"));
	QComboBox *typeCombo = new QComboBox;
	typeCombo->addItem(_("Scatter"));
	typeCombo->addItem(_("Line"));

	QLabel *xLabel = new QLabel(_("X axis:"));
	QComboBox *xCombo = new QComboBox;
	xCombo->addItem(_("row index"));
	for (char c = 'A'; c < (char)('A' + ncols); ++c) 
		xCombo->addItem(QString(c));
	xCombo->setCurrentIndex(1 % ncols);

	QLabel *yLabel = new QLabel(_("Y axis:"));
	QComboBox *yCombo = new QComboBox;
	yCombo->addItem("row index");
	for (char c = 'A'; c < (char)('A' + ncols); ++c)
		yCombo->addItem(QString(c));
	yCombo->setCurrentIndex(1 % ncols + 1);

	QLabel *dxLabel = new QLabel(_("DX:"));
	QComboBox *dxCombo = new QComboBox;
	for (char c = 'A'; c < (char)('A' + ncols); ++c)
		dxCombo->addItem(QString(c));
	dxCombo->setCurrentIndex(2 % ncols);
	
	QLabel *dyLabel = new QLabel(_("DY:"));
	QComboBox *dyCombo = new QComboBox;
	for (char c = 'A'; c < (char)('A' + ncols); ++c)
		dyCombo->addItem(QString(c));
	dyCombo->setCurrentIndex(3 % ncols);

	QPushButton *okButton = new QPushButton(_("&OK"));
	okButton->setDefault(true);
	connect(okButton, SIGNAL(clicked()), chartDialog, SLOT(accept()));

	QPushButton *cancelButton = new QPushButton(_("&Cancel"));
	connect(cancelButton, SIGNAL(clicked()), chartDialog, SLOT(reject()));

	gridLayout->addWidget(addtoLabel, 0,0);
	gridLayout->addWidget(addtoCombo, 0,1);
	gridLayout->addWidget(typeLabel, 1,0);
	gridLayout->addWidget(typeCombo, 1,1);
	gridLayout->addWidget(xLabel, 2,0);
	gridLayout->addWidget(xCombo, 2,1);
	gridLayout->addWidget(yLabel, 3,0);
	gridLayout->addWidget(yCombo, 3,1);
	gridLayout->addWidget(dxLabel, 4,0);
	gridLayout->addWidget(dxCombo, 4,1);
	gridLayout->addWidget(dyLabel, 5,0);
	gridLayout->addWidget(dyCombo, 5,1);

	butLayout->addWidget(okButton);
	butLayout->addWidget(cancelButton);

	mainLayout->addLayout(gridLayout);
	mainLayout->addLayout(butLayout);
	chartDialog->setLayout(mainLayout);

	if (!chartDialog->exec()) {
		ret = false;
	} else {
		int i = addtoCombo->currentIndex();
		if (i == 0) {
			*xy = NULL;
		} else {
			for (list<XY*>::iterator it=MainWindow::xylist.begin();
					it!=MainWindow::xylist.end(); ++it)
			{
				--i;
				if (i == 0) {
					*xy = *it;
					break;
				}
			}
		}
		*x = xCombo->currentIndex();
		*y = yCombo->currentIndex();
		*dx = dxCombo->currentIndex();
		*dy = dyCombo->currentIndex();
		*type = typeCombo->currentIndex();
	}
	delete chartDialog;
	return ret;
}


bool Dialogs::findPeaks(const vector<ChartElement*> &elements, string *name,
                        int *index, int *type, double *threshold)
{
	QDialog fDialog;
	fDialog.setWindowTitle(_("Find peaks"));

	QVBoxLayout *mainLayout = new QVBoxLayout;
	QGridLayout *gridLayout = new QGridLayout;
	QHBoxLayout *butLayout  = new QHBoxLayout;

	QLabel    *dataLabel = new QLabel(_("Data:"));
	QComboBox *dataCombo = new QComboBox;
	for (vector<ChartElement*>::const_iterator it = elements.begin();
	     it != elements.end(); ++it)
	{
		if (((*it)->type() & nplot::elem_xy) == 0)
			continue;
		dataCombo->addItem((*it)->name.c_str());
	}

	QLabel *typeLabel = new QLabel(_("Type:"));
	QComboBox *typeCombo = new QComboBox;
	typeCombo->addItem(_("Only positive"));
	typeCombo->addItem(_("Only negative"));
	typeCombo->addItem(_("Both positive and negative"));

	QLabel *thresholdLabel = new QLabel(_("Threshold:"));
	QDoubleSpinBox *thresholdSpin = new QDoubleSpinBox;
	thresholdSpin->setValue(2.0);
	thresholdSpin->setMinimum(0);
	thresholdSpin->setMaximum(10);
	thresholdSpin->setSingleStep(0.1);
	thresholdSpin->setDecimals(2);

	QPushButton *okButton = new QPushButton(_("OK"));
	connect(okButton, SIGNAL(clicked()), &fDialog, SLOT(accept()));
	QPushButton *cancelButton = new QPushButton(_("Cancel"));
	connect(cancelButton, SIGNAL(clicked()), &fDialog, SLOT(reject()));

	gridLayout->addWidget(dataLabel, 0, 0);
	gridLayout->addWidget(dataCombo, 0, 1);
	gridLayout->addWidget(typeLabel, 1, 0);
	gridLayout->addWidget(typeCombo, 1, 1);
	gridLayout->addWidget(thresholdLabel, 2, 0);
	gridLayout->addWidget(thresholdSpin, 2, 1);

	butLayout->addWidget(okButton);
	butLayout->addWidget(cancelButton);

	mainLayout->addLayout(gridLayout);
	mainLayout->addLayout(butLayout);
	fDialog.setLayout(mainLayout);

	if (!fDialog.exec())
		return false;

	gchar *gname = g_strdup_printf(_("peaks of %s"),
	                  (const gchar *) dataCombo->currentText().toAscii());
	*name      = gname;
	*index     = dataCombo->currentIndex();
	*threshold = thresholdSpin->value();
	*type      = typeCombo->currentIndex();

	g_free(gname);

	return true;
}

void Dialogs::setCurrentDir(QString cd)
{
	int p = cd.lastIndexOf('/');
	cd.chop(cd.size()-p);
	currentDir = cd;
}

ImportDialog::ImportDialog(QString *_filename): filename(_filename)
{
	setWindowTitle(_("Open data file..."));

	QVBoxLayout *mainLayout  = new QVBoxLayout;
	QHBoxLayout *fileLayout  = new QHBoxLayout;
	QGridLayout *groupLayout = new QGridLayout;
	QHBoxLayout *butLayout   = new QHBoxLayout;

	fileLineEdit = new QLineEdit;
	fileLineEdit->setText(Dialogs::id_filename);
	QToolButton *browseButton = new QToolButton(this);
	browseButton->setText("...");
	QGroupBox *groupBox = new QGroupBox(_("Settings"));
	QLabel *sepLabel = new QLabel(_("Decimal separator:"));
	sepCombo = new QComboBox;
	sepCombo->addItem(_(". [period]"));
	sepCombo->addItem(_(", [comma]"));
	
	/* Confrontation with settings */
	if (nApp::settings->decimalSeparator == ',')
		sepCombo->setCurrentIndex(1);
	QLabel *comLabel = new QLabel(_("Comment sign:"));

	comCombo = new QComboBox;
	comCombo->addItem("#");
	comCombo->addItem("/");
	comCombo->addItem(";");
	comCombo->addItem("!");
	comCombo->addItem("%");
	
	/* Confrontation with settings */
	switch (nApp::settings->commentSign) { 
	case '#':
		comCombo->setCurrentIndex(0);
		break;
	case '/':
		comCombo->setCurrentIndex(1);
		break;
	case ';':
		comCombo->setCurrentIndex(2);
		break;
	case '!':
		comCombo->setCurrentIndex(3);
		break;
	case '%':
		comCombo->setCurrentIndex(4);
		break;
	}
	
	QLabel *colsLabel = new QLabel(_("Columns:"));
	colsCombo = new QComboBox;
	colsCombo->addItem(_("Guess"));
	for (int i=1; i<=26; ++i) colsCombo->addItem(QString("%1").arg(i));
	colsCombo->setCurrentIndex(nApp::settings->ncols);

	QPushButton *okButton = new QPushButton(_("&OK"), this);
	okButton->setDefault(true);
	QPushButton *cancelButton = new QPushButton(_("&Cancel"));
	
	fileLayout->addWidget(fileLineEdit);
	fileLayout->addWidget(browseButton);
	groupLayout->addWidget(sepLabel, 0, 0);
	groupLayout->addWidget(sepCombo, 0, 1);
	groupLayout->addWidget(comLabel, 1, 0);
	groupLayout->addWidget(comCombo, 1, 1);
	groupLayout->addWidget(colsLabel, 2, 0);
	groupLayout->addWidget(colsCombo, 2, 1);
	butLayout->addWidget(okButton);
	butLayout->addWidget(cancelButton);

	groupBox->setLayout(groupLayout);
	mainLayout->addLayout(fileLayout);
	mainLayout->addWidget(groupBox);
	mainLayout->addLayout(butLayout);
	setLayout(mainLayout);

	connect(browseButton, SIGNAL(clicked()), this, SLOT(getFile()));
	connect(okButton, SIGNAL(clicked()), this, SLOT(accept()));
	connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));
}

void ImportDialog::getFile()
{
	QString fmtmp = QFileDialog::getOpenFileName(this, _("Open file..."), 
	                Dialogs::id_filename, _("txt files (*.txt);;"
		        "dat files (*.dat);;all files (*)"));

	if (!fmtmp.isEmpty())
		fileLineEdit->setText(fmtmp);
}

void ImportDialog::accept()
{
	if (fileLineEdit->text().isEmpty()) {
		reject();
		return;
	}
	
	nApp::settings->decimalSeparator = sepCombo->currentText().data()->unicode();
	nApp::settings->commentSign = comCombo->currentText().data()->unicode();
	*filename = fileLineEdit->text();
	nApp::settings->ncols = colsCombo->currentIndex();
	QDialog::accept();
}

/*************** R A N G E  D I A L O G *****************/

RangeDialog::RangeDialog(vector<ChartElement*> *_elements, int *_ds,
                         int *_f, int *_t, int *_d)
 : elements(_elements), ds(_ds), f(_f), t(_t), d(_d)
{
	setWindowTitle(_("Set range"));

	QVBoxLayout *mainLayout = new QVBoxLayout;
	QGridLayout *gridLayout = new QGridLayout;
	QHBoxLayout *butLayout  = new QHBoxLayout;

	/* TRANSLATORS: Fit curve to data set */
	QLabel *dataLabel = new QLabel(_("Fit to:"));
	dataCombo = new QComboBox;
	int nrows=0;
	for (vector<ChartElement*>::iterator it = elements->begin(); 
	     it != elements->end(); ++it)
	{
		if (((*it)->type() & nplot::elem_xy) == 0)
			continue;
		dataCombo->addItem((*it)->name.c_str());
		if (nrows == 0)
			nrows = ((DataSeriesXY*) (*it))->x().size();
	}
	connect(dataCombo, SIGNAL(currentIndexChanged(int)), this, 
	        SLOT(dataChange(int)));

	QLabel *fromLabel = new QLabel(_("From:"));
	fromSpin = new QSpinBox;
	fromSpin->setValue(1);
	fromSpin->setMinimum(1);
	fromSpin->setMaximum(nrows);
	
	QLabel *toLabel = new QLabel(_("To:"));
	toSpin = new QSpinBox;
	toSpin->setMinimum(1);
	toSpin->setMaximum(nrows);
	toSpin->setValue(nrows);

	QPushButton *okButton = new QPushButton(_("OK"));
	connect(okButton, SIGNAL(clicked()), this, SLOT(accept()));
	QPushButton *cancelButton = new QPushButton(_("Cancel"));
	connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));

	gridLayout->addWidget(dataLabel, 0, 0);
	gridLayout->addWidget(dataCombo, 0, 1);
	if (d != NULL) {
		/* TRANSLATORS: Polynomial degree */
		QLabel *degLabel = new QLabel(_("Degree:"));
		degSpin = new QSpinBox;
		degSpin->setValue(5);
		degSpin->setMinimum(1);
		degSpin->setMaximum(20);
		gridLayout->addWidget(degLabel, 1,0);
		gridLayout->addWidget(degSpin, 1,1);
	}
	gridLayout->addWidget(fromLabel, 2,0);
	gridLayout->addWidget(fromSpin, 2,1);
	gridLayout->addWidget(toLabel, 3,0);
	gridLayout->addWidget(toSpin, 3,1);
	
	butLayout->addWidget(okButton);
	butLayout->addWidget(cancelButton);

	mainLayout->addLayout(gridLayout);
	mainLayout->addLayout(butLayout);
	setLayout(mainLayout);
}

void RangeDialog::dataChange(int i)
{
	for (vector<ChartElement*>::iterator it = elements->begin();
	     it!=elements->end(); ++it)
	{
		if (((*it)->type() & nplot::elem_xy) == 0)
			continue;
		if (i-- != 0)
			continue;
		int n = ((DataSeriesXY*) (*it))->x().size();
		fromSpin->setMaximum(n);
		toSpin->setMaximum(n);
		toSpin->setValue(n);
		break;
	}
}

void RangeDialog::accept()
{
	int from = fromSpin->value();
	int to;

	/* Pick the greater one as "to" */
	if (toSpin->value() < from) {
		to = from;
		from = toSpin->value();
	} else {
		to = toSpin->value();
	}

	*ds = dataCombo->currentIndex();
	*f = --from;
	*t = to;
	if (d != NULL)
		*d = degSpin->value();	
	QDialog::accept();
}

HelpDialog::HelpDialog(QWidget *parent): QDialog(parent)
{
	QTabWidget *tabWidget = new QTabWidget;
	
	QLabel *aboutLabel = new QLabel(QString("<center><font size=5>"
	                         "<font face=cmmib10 color=red>n</font>Plot ")
			        + PACKAGE_VERSION + "</font><br><font size=3>"
			           "&copy; 2006-2010, Mariusz Adamski<br><br>"
	                                + _("E-mail:") + " "
			         "<a href='mailto:mariusz.adamski@gmail.com'>"
	                                "mariusz.adamski@gmail.com</a><br>"
			                + _("Project site:") + " "
			          "<a href='http://nanoplot.sourceforge.net'>"
	                             "http://nanoplot.sourceforge.net</a><br>"
			                + _("Bug report:") + " "
			   "<a href='http://sf.net/tracker/?group_id=170749'>"
			      "http://sf.net/tracker/?group_id=170749</a><br>"
			                + _("Mailing list:") + " <a "
			 "href='mailto:nanoplot-devel@lists.sourceforge.net'>"
			            "nanoplot-devel@lists.sourceforge.net</a>"
			                "</font></center>");
	
	QTextBrowser *licenseBrowser = new QTextBrowser;
	licenseBrowser->setHtml(QString("<nobr>(c) 2006-2010, "
	                                "Mariusz Adamski</nobr><br>")
	                     + _("<nobr>This program is distributed under "
			         "the terms of GPL v2 license.</nobr>") 
	                                + QString(license));
	
	tabWidget->addTab(aboutLabel, _("&About"));
	tabWidget->addTab(licenseBrowser, _("&License"));

	QPushButton *okButton = new QPushButton(_("Close"));

	connect(okButton, SIGNAL(clicked()), this, SLOT(accept()));

	QHBoxLayout *hlayout = new QHBoxLayout;
	hlayout->addStretch(1);
	hlayout->addWidget(okButton);
	
	QVBoxLayout *mainLayout = new QVBoxLayout;
	mainLayout->addWidget(tabWidget);
	mainLayout->addLayout(hlayout);;
	setLayout(mainLayout);

	setMinimumWidth(600);
	setWindowTitle(_("About nPlot"));
}

