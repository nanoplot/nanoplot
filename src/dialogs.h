/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DIALOGS_H
#define DIALOGS_H

#include <QtGui>
#include <vector>
#include "chartelement.h"
#include "mainwindow.h"
#include "nplotnamespace.h"
#include "xy.h"

class ChartElement;
class MainWindow;
class XY;

using namespace std;

/**
 *   Class containing static functions invoking various dialogs.
 */
class Dialogs: public QDialog
{
Q_OBJECT
public:
	/** 
	 *   An empty constructor.
	 *   We're using only static members so we dont need to create any
	 *   instances of that object. Its not just a namespace only becouse 
	 *   we need \c tr and \c connect functions provided by \c Q_OBJECT macro.
	 */
	Dialogs() {};
	
	/**  An empty destructor. */
	virtual ~Dialogs() {};

	/**
	 *   Error message dialog.
	 *   \param err Error to be communicated
	 */
	static void errorMessage(nplot::fit_err err);

	/**
	 *   Generic error dialog.
	 *   \param msg Message to be communicated
	 */
	static void error(const char *msg);

	/**
	 *   Get number of rows and columns dialog.
	 *   \param rows place to store chosen number of rows
	 *   \param cols place to store chosen number of columns
	 *   \return \c false if user clicked "Cancel"; \c true otherwise
	 */
	static bool getRowsCols(int *rows, int *cols);

	/**
	 *   Get resolution dialog.
	 *   \param width place to store chosen width
	 *   \param height place to store chosen height
	 *   \return \c false if user clicked "Cancel"; \c true otherwise
	 */
	static bool getResolution(int *width, int *height);

	static void settings();

	/**
	 *   Make chart XY dialog.
	 *   \param ncols number of columns of the spreadsheet
	 *   \param xy place to store the pointer of XY widget chosen to add to
	 *   \param x place to store column number (or 0 for row index) chosen as X
	 *   \param y place to store column number (or 0 for row index) chosen as Y
	 *   \param type place to store chosen chart type (0 - scatter, 1 - line)
	 *   \return \c false if user clicked "Cancel"; \c true otherwise
	 */
	static bool makeChartXY(int ncols, XY **xy, int *x, int *y, int *type);
	
	/**
	 *   Make chart XYD dialog.
	 *   "D" means here delta X or delta Y (error bars)
	 *   \param ncols number of columns of the spreadsheet
	 *   \param ctype chart type (\sa elem_type)
	 *   \param xy place to store the pointer of XY widget chosen to add to
	 *   \param x place to store column number (or 0 for row index) chosen as X
	 *   \param y place to store column number (or 0 for row index) chosen as Y
	 *   \param d place to store column number chosen as DX/DY
	 *   \param type place to store chosen chart type (0 - scatter, 1 - line)
	 *   \return \c false if user clicked "Cancel"; \c true otherwise
	 */
	static bool makeChartXYD(int ncols, int ctype, XY **xy, int *x, int *y,
			int *d, int *type);
	
	/**
	 *   Make chart XYDXDY dialog.
	 *   \param ncols number of columns of the spreadsheet
	 *   \param xy place to store the pointer of XY widget chosen to add to
	 *   \param x place to store column number (or 0 for row index) chosen as X
	 *   \param y place to store column number (or 0 for row index) chosen as Y
	 *   \param dx place to store column number chosen as DX
	 *   \param dy place to store column number chosen as DY
	 *   \param type place to store chosen chart type (0 - scatter, 1 - line)
	 *   \return \c false if user clicked "Cancel"; \c true otherwise
	 */
	static bool makeChartXYD2(int ncols, XY **xy, int *x, int *y, int *dx, 
			int *dy, int *type);
	
	/**
	 *   Find peaks dialog.
	 *   \param elements chart elements to choose from
	 *   \param name place to store name of the peaks element
	 *   \param index place to store index of element chosen
	 *   \param threshold place to store chosen threshold
	 *   \return \c false if user clicked "Cancel"; \c true otherwise
	 */
	static bool findPeaks(const vector<ChartElement*> &elements,
	                      string *name, int *index, int *type,
	                      double *threshold); 

	/**
	 *   Sets the current dir from the given path
	 *   \par cd path to be current
	 */
	static void setCurrentDir(QString cd);
	static QString currentDir;      /**< Current directory */
	static QString id_filename;     /**< Import dialog filename */
};

/**
 *   Import dialog class.
 */
class ImportDialog: public QDialog
{
Q_OBJECT
public:
	/**
	 *   A constructor.
	 *   \param filename place to store chosen filename
	 */
	ImportDialog(QString *filename);

	/**  A destructor */
	~ImportDialog() {};
public slots:
	void getFile();            /**< Invokes a dialog to choose a file */
	void accept();                 /**< Called when user pressed "Ok" button */
private:
	QString *filename;         /**< Place to store chosen filename */
	QLineEdit *fileLineEdit;   /**< File name line edit */
	QComboBox *sepCombo;       /**< Decimal separator combo box */
	QComboBox *comCombo;       /**< Comment char combo box */
	QComboBox *colsCombo;      /**< Number of columns combo box */
};

/**
 *   Range dialog class.
 *   Lets the user choose a range of data points and a specific data set 
 *   to apply e.g. curve fitting
 */
class RangeDialog: public QDialog
{
Q_OBJECT
public:
	/**
	 *   A constructor.
	 *   NOTE: Additional spin box to choose a polynomial degree 
	 *   (for polynomial fitting) will be present only if \c deg is not 
	 *   \c NULL.
	 *   \param elements std::vector of available data sets
	 *   \param ds place to store chosen data set index
	 *   \param from place to store "from"
	 *   \param to place to store "to"
	 *   \param deg place to store degree of polynomial chosen
	 */
	RangeDialog(vector<ChartElement*> *elements, int *ds, int *from, int *to, 
	            int *deg = 0);
	
	/**  A destructor */
	~RangeDialog() {};
public slots:
	/**
	 *   Data change event.
	 *   \param i chosen data index
	 */
	void dataChange(int i);
	void accept();                /**< Called when user pressed "Ok" button */
private:
	vector<ChartElement*> *elements; /**< Available elements */
	int *ds;                      /**< Place to store chosen data set index */
	int *f;                       /**< Place to store "from" */
	int *t;                       /**< Place to store "to" */
	int *d;                       /**< Place to store degree of polynomial */
	QComboBox *dataCombo;         /**< Data set combo box */
	QSpinBox *degSpin;            /**< Polynomial degree spin box */
	QSpinBox *fromSpin;           /**< "From" spin box */
	QSpinBox *toSpin;             /**< "To" spin box */
};

/**
 *   Help dialog class.
 */
class HelpDialog: public QDialog
{
Q_OBJECT
public:
	/**
	 *   A constructor.
	 *   \param parent parent widget
	 */
	HelpDialog(QWidget *parent = 0);
	
	/**  A destructor */
	~HelpDialog() {};
//private:
	//QTabWidget *tabWidget;
};

#endif /* DIALOGS_H */
