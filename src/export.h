/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EXPORTS_H
#define EXPORTS_H

#include "nplotnamespace.h"
#include <string>

#define IMG_WIDTH 600
#define IMG_HEIGHT 400

using namespace std;
using namespace nplot;

/**
 *   Exports base class.
 */
class Export
{
public:
	/**  Fonts enum */
	enum font_face {
		times = 1,
		math = 2,
		symbol = 3,
		symbol2 = 4
	};

	/**  Constructor */
	Export() : textMeasure(false) {};

	/**  Destructor */
	virtual ~Export() {};

	/**
	 *   Draws a point.
	 *   \param x, y coordinates
	 *   \param type type of the point
	 *   \param size size of the point
	 *   \sa point_shape
	 */
	virtual void drawPoint(double x, double y, point_shape type, int size) = 0;

	/**
	 *   Draws line.
	 *   \param x1, y1 coordinates of the first point
	 *   \param x2, y2 coordinates of the second point
	 *   \param thick thickness
	 *   \param ldash line dash pattern
	 */
	virtual void drawLine(double x1, double y1, double x2, double y2,
	                      double thick = 1, line_dash ldash = ld_solid) = 0;

	/**
	 *   Backend-specific draw text.
	 *   Uses currently set font to render.
	 *   \param x, y coordinates
	 *   \param text text to be written
	 *   \return width of drawn text
	 */
	virtual double drawText(double x, double y, const char *text) = 0;
	
	/**
	 *   Set the active color.
	 *   \param r, g, b red, green and blue components
	 */
	virtual void setColor(double r, double g, double b) = 0;

	/**
	 *   Translates the viewport to given location.
	 *   \param x, y coordinates to translate to
	 */
	virtual void translate(double x, double y) = 0;

	/**
	 *   Rotate the viewport.
	 *   \param angle rotation angle
	 */
	virtual void rotate(double angle) = 0;

	/**
	 *   Move graphics cursor.
	 *   Equivalent to "moveto" in PostScript.
	 *   param x, y coordinates to move to
	 */
	virtual void moveto(double x, double y) = 0;

	/**
	 *   Moves graphics cursor adding a line segment to path.
	 *   Equivalent to "lineto" in PostSctipt.
	 *   param x, y coordinates to line to
	 */
	virtual void lineto(double x, double y) = 0;

	/**
	 *   Strokes actual path.
	 *   \param thick thickness of the stroke
	 *   \param ldash line dash pattern
	 */
	virtual void stroke(double thick = 1, line_dash ldash = ld_solid) = 0;

	/**
	 *   Width of the drawing canvas.
	 *   \return width of the drawing canvas
	 */
	virtual int width() = 0;

	/**
	 *   Height of the drawing canvas.
	 *   \return height of the drawing canvas
	 */
	virtual int height() = 0;

	/**
	 *   Set the font face.
	 *   \param face font face to be set
	 *   \param size font size
	 */
	virtual void setFont(font_face face, double size) = 0;

	/**
	 *   Determines ascii string width using current font settings.
	 *   \param text given text
	 */
	virtual double stringWidth(const char *text) = 0;

	/**
	 *   Font ascent.
	 *   \return font ascent
	 */
	virtual double getFontAscent() = 0;

	/**
	 *   Font descent.
	 *   \return font descent
	 */
	virtual double getFontDescent() = 0;

	/**
	 *   Font height.
	 *   \return font height
	 */
	virtual double getFontHeight() = 0;
	
	/**
	 *   Sets the clipping rectangle.
	 *   \param x1, y1 coords of one corner
	 *   \param x2, y2 coords of second corner
	 */
	virtual void setClipRect(double x1, double y1, double x2, double y2) = 0;

	/**
	 *   Sets the test measure mode on/off.
	 *   If the text measure is on, all the text related functions will
	 *   not produce any output, but only return dimensions of the given
	 *   text.
	 *   \param b if \c true, sets text measure mode on; sets it off
	 *   otherwise
	 */
	void setTextMeasure(bool b) { textMeasure = b; };
protected:
	bool textMeasure;  /**< Holds whether or not, text measure mode is on */
};

#endif /* EXPORTS_H */
