/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "funcxy.h"
#include "config.h"
#include "nplotnamespace.h"

using namespace nplot;

FuncXY::FuncXY(const char *nm, const vector<double> &data, int n, XY *parent)
 : approxPts(n), proc(data), xychart(parent)
{
	Init();
	name = nm;
}

FuncXY::FuncXY(SessionReader *reader, XY *parent)
 : xychart(parent)
{
	Init();

	name      = reader->getProperty("name");
	vcolor    = Color(reader->getProperty("vcolor"));
	approxPts = reader->getPropertyInt("approxPts");
	lineThick = reader->getPropertyDouble("lineThick");
	vdash     = (line_dash) reader->getPropertyInt("vdash");
	lBound    = reader->getPropertyDouble("lBound");
	rBound    = reader->getPropertyDouble("rBound");
	lBoundEna = reader->getPropertyBool("lBoundEna");
	rBoundEna = reader->getPropertyBool("rBoundEna");

	reader->processChildren();
	
	for (const char *name = reader->nextNode(); name;
	     name = reader->nextNode())
	{
		if (strcmp(name, "content") == 0) {
			string content = reader->getValue();
			string token = "";
			for (string::iterator it = content.begin();
			     it != content.end(); ++it)
			{
				if (*it != ' ') {
					token += *it;
					continue;
				}
				proc.push_back(atof(token.c_str()));
				token = "";
			}
		}
	}
}

void FuncXY::Init()
{
	vdash     = ld_solid;
	lineThick = 1;
	vcolor    = Color(0,0,0);
	lBoundEna = false;
	rBoundEna = false;
	lBound    = -1e99;
	rBound    = 1e99;
}

void FuncXY::save(SessionWriter *writer) const
{
	stringstream s;
	for (vector<double>::const_iterator it = proc.begin();
	     it != proc.end(); ++it) {
		s << *it << ' ';
	}

	writer->startElement("FuncXY");
	writer->setProperty("name", name.c_str());
	writer->setProperty("vcolor", vcolor.str().c_str());
	writer->setProperty("approxPts", approxPts);
	writer->setProperty("lineThick", lineThick);
	writer->setProperty("vdash", vdash);
	writer->setProperty("lBound", lBound);
	writer->setProperty("rBound", rBound);
	writer->setProperty("lBoundEna", lBoundEna);
	writer->setProperty("rBoundEna", rBoundEna);
	writer->startElement("content");
	writer->setValue(s.str().c_str());
	writer->endElement();
	writer->endElement();
}

void FuncXY::repaint(Export *exp) const
{
	if (hidden)
		return;

	double x1 = xychart->xAxis->min();
	double x2 = xychart->xAxis->max();
	
	if (lBoundEna && x1 < lBound)
		x1 = lBound;
	if (rBoundEna && x2 > rBound)
		x2 = rBound;
	
	double inc = (x2 - x1) / approxPts;
	double w = exp->width();
	double h = exp->height();
	double x = x1;
	int i, err;
	
	for (i = 0; i <= approxPts; ++i) {     /* Omitting points beyond domain */
		x = x1 + i * inc;
		calcValue(x, &err);
		if (err == 0)
			break;
	}
	
	if (i >= approxPts)
		return;
	
	bool stopped = false;

	exp->setColor(vcolor.red(), vcolor.green(), vcolor.blue());
	exp->moveto(xychart->wdwX(x, w), xychart->wdwY(calcValue(x), h));
	for (int j = i; j <= approxPts; ++j) {
		double x = x1 + j * inc;
		double y = calcValue(x, &err);
		if (err != 0) {
			stopped = true;
			continue;
		} else if (stopped == true) {
			exp->moveto(xychart->wdwX(x, w), xychart->wdwY(y, h));
			stopped = false;
		}
		if (!stopped)
			exp->lineto(xychart->wdwX(x, w), xychart->wdwY(y, h));
	}
	exp->stroke(lineThick, vdash);
}

double FuncXY::calcValue(double x, int *err) const
{						
	/*
	 *   Calculating npl reverse Polish notation (forth-like) expressions
	 */

	stack<double> st;
	double op1;
	double op2;

	if (err != NULL)
		*err = 0;

	for (vector<double>::const_iterator it = proc.begin();
	     it != proc.end(); ++it)
	{
		switch ((int)*it) {
		case nplot::npl_push:
			st.push(*++it);
			break;
		case nplot::npl_pop:
			st.pop();
			break;
		case nplot::npl_dup:
			st.push(st.top());
			break;
		case nplot::npl_add:
			op1 = st.top();
			st.pop();
			op1 += st.top();
			st.pop();
			st.push(op1);
			break;
		case nplot::npl_sub:
			op1 = st.top();
			st.pop();
			op1 = st.top() - op1;
			st.pop();
			st.push(op1);
			break;
		case nplot::npl_mul:
			op1 = st.top();
			st.pop();
			op1 *= st.top();
			st.pop();
			st.push(op1);
			break;
		case nplot::npl_div:
			op1 = st.top();
			if (op1 == 0) {
				if (err != NULL)
					*err = nplot::err_domain;
				return 0;
			}
			st.pop();
			op1 = st.top() / op1;
			st.push(op1);
			break;
		case nplot::npl_pow:
			op1 = st.top();
			st.pop();
			if (st.top() < 0 && modf(op1, &op2) != 0) {
				if (err != NULL)
					*err = nplot::err_domain;
				return 0;
			}
			op1 = pow(st.top(), op1);
			st.pop();
			st.push(op1);
			break;
		case nplot::npl_exp:
			op1 = exp(st.top());
			st.pop();
			st.push(op1);
			break;
		case nplot::npl_ln:
			op1 = st.top();
			if (op1 <= 0) {
				if (err != NULL)
					*err = nplot::err_domain;
				return 0;
			}
			op1 = log(op1);
			st.pop();
			st.push(op1);
			break;
		case nplot::npl_varx:
			st.push(x);
			break;
		case nplot::npl_end:
			return st.top();
		}
	}

	if (err != NULL)
		*err = nplot::err_syntax;
	return 0;
}

elem_type FuncXY::type() const            { return elem_func; }

void FuncXY::setColor(const Color &c)     { vcolor = c; }

Color FuncXY::color() const               { return vcolor; }

void FuncXY::setDash(line_dash _vdash)    { vdash = _vdash; }

line_dash FuncXY::dash() const            { return vdash; }

void FuncXY::setApproxPoints(int p)       { approxPts = p; }

int FuncXY::approxPoints() const          { return approxPts; }

void FuncXY::setLineThickness(double t)   { lineThick = t; }

double FuncXY::lineThickness() const      { return lineThick; }

void FuncXY::setLeftBoundEnabled(bool b)  { lBoundEna = b; }

void FuncXY::setRightBoundEnabled(bool b) { rBoundEna = b; }

void FuncXY::setLeftBound(double d)       { lBound = d; }

void FuncXY::setRightBound(double d)      { rBound = d; }

bool FuncXY::isLeftBoundEnabled() const   { return lBoundEna; }

bool FuncXY::isRightBoundEnabled() const  { return rBoundEna; }

double FuncXY::leftBound() const          { return lBound; }

double FuncXY::rightBound() const         { return rBound; }

