/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FUNCXY_H
#define FUNCXY_H

#include <vector>
#include <stack>
#include <sstream>
#include <string>
#include "chartelement.h"
#include "export.h"
#include "nplotnamespace.h"
#include "sessionreader.h"
#include "types.h" 
#include "xy.h"

using namespace nplot;

class XY;

/**
 *   Class used for visualization of analytic functions.
 */
class FuncXY: public ChartElement
{
public:
	/**
	 *   A constructor.
	 *   \param name Name of the element
	 *   \param proc pointer to the NPL procedure describing function
	 *   \param ap number of aproximation points
	 *   \param xy pointer to parent XY widget
	 */
	FuncXY(const char *name, const vector<double> &proc, int ap, XY *xy);
	
	/**
	 *   A constructor creating FuncXY element using information from
	 *   session file.
	 *   \param reader session reader
	 *   \param xy pointer to parent XY widget
	 */
	FuncXY(SessionReader *reader, XY *xy);
        
	/**  A destructor */
	~FuncXY() {};
	void save(SessionWriter *writer) const;
	void repaint(Export *exp = 0) const;
	nplot::elem_type type() const;
	
	/**
	 *   Sets elements' color.
	 *   \param r red component
	 *   \param g green component
	 *   \param b blue component
	 */
	void setColor(double r, double g, double b);
	
	/**
	 *   Sets elements' color.
	 *   \param c Color describing elenent's new color
	 */
	void setColor(const Color &c);
	
	/**
	 *   Returns color of the element.
	 *   \return Color describing elements' color
	 */
	Color color() const;
	
	/**
	 *   Calculates value of the function.
	 *   \param x value of argument
	 *   \param err pointer to int to indicate an error; this parameter
	 *   is not obligatory
	 */
	double calcValue(double x, int *err = 0) const;

	/**
	 *   Sets the dash pattern.
	 *   \param pattern dash pattern number
	 */
	void setDash(line_dash pattern);

	line_dash dash() const;                  /**< Returns dash pattern number */

	/**
	 *   Sets the number of approximation points.
	 *   \param ap number of approximation points
	 */
	void setApproxPoints(int ap);
	
	/** Returns number of approximation points */
	int approxPoints() const;

	/**
	 *   Sets the line thickness.
	 *   \param thick line thickness
	 */
	void setLineThickness(double thick);

	double lineThickness() const;           /**< Returns line thickness */

	/**
	 *   Enables (or disables) left bound.
	 *   \param b if \c true, method will enable left bound; otherise will 
	 *   disable it
	 */
	void setLeftBoundEnabled(bool b);

	/**
	 *   Sets left bound.
	 *  \param d new value of left bound
	 */
	void setLeftBound(double d);

	/**
	 *   Enables (or disables) right bound.
	 *   \param b if \c true, method will enable right bound; otherise will
	 *   disable it
	 */
	void setRightBoundEnabled(bool b);

	/**
	 *   Sets right bound.
	 *   \param d new value of right bound
	 */
	void setRightBound(double d);

	/**
	 *   Checks if left bound is enabled.
	 *   \return \c true if left bound is enabled, otherwise \c false
	 */
	bool isLeftBoundEnabled() const;
	
	/**
	 *   Checks if right bound is enabled.
	 *   \return \c true if right bound is enabled, otherwise \c false
	 */
	bool isRightBoundEnabled() const;

	double leftBound() const;      /**< Returns left bound */
	double rightBound() const;     /**< Returns right bound */
private:
	line_dash vdash;                     /**< Holds dash pattern number */
	int approxPts;                 /**< Holds number of approximation points */
	double lineThick;              /**< Holds line thickness */
	Color vcolor;                  /**< Holds color of the element */
	bool lBoundEna;                /**< Holds left bound status flag */
	bool rBoundEna;                /**< Holds right bound status flag */
	double lBound;                 /**< Holds value of left bound */
	double rBound;                 /**< Holds value of right bound */
	vector<double> proc;           /**< Holds NPL procedure */
	XY *xychart;                   /**< Holds pointer to parent XY widget */
private:
	void Init();                   /**< Initialize needed variables */
};

#endif /* FUNCXY_H */
