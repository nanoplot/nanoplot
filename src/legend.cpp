/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "legend.h"
#include "config.h"
#include "tex.h"

using namespace std;

Legend::Legend(XY *_xychart)
 : vfontSize(12), xychart(_xychart)
{
}

void Legend::save(SessionWriter *writer) const
{
	writer->startElement("Legend");
	writer->setProperty("vfontSize", vfontSize);
	writer->setProperty("hidden", hidden);
	writer->endElement();
}

void Legend::load(SessionReader *reader)
{
	vfontSize = reader->getPropertyInt("vfontSize");
	hidden = reader->getPropertyBool("hidden");
}

int Legend::fontSize() const { return vfontSize; }

void Legend::setFontSize(int n) { vfontSize = n; }

void Legend::repaint(Export *exp) const
{
	if (hidden)
		return;

	TeX tex(exp);
	exp->setFont(Export::times, vfontSize);
	double x = exp->width() - xychart->rightmgn - 5;
	double y = xychart->topmgn + 5 + exp->getFontAscent();
	double yhalf = 0.3 * exp->getFontAscent();
	for (vector<ChartElement*>::iterator it = xychart->elements.begin(); 
	     it != xychart->elements.end(); ++it)
	{
		if ((*it)->isHidden())
			continue;
		double w = tex.textWidth((*it)->name.c_str(), vfontSize) + 26;
		exp->setColor(0, 0, 0);
		tex.textInterpret(x - w, y, (*it)->name.c_str(), vfontSize);
		if (((*it)->type() & nplot::elem_xy) != 0) {
			DataSeriesXY *element = (DataSeriesXY*)(*it);
			exp->setColor(element->color().red(), 
			              element->color().green(),
			              element->color().blue());
			if (element->isSymbolsEnabled()) {
				int ssize = element->symbolSize();
				if (ssize > 5)
					ssize = 5;
				exp->drawPoint(x - 13, y - yhalf,
				               element->symbolShape(), ssize);
			}
			if (element->isConnectedEnabled()) {
				exp->moveto(x - 21, y - yhalf);
				exp->lineto(x - 5, y - yhalf);
				exp->stroke(element->lineThickness(), 
			                 (line_dash) element->dash());
			}
		} else if (((*it)->type() & nplot::elem_func) != 0) {
			FuncXY *element = (FuncXY*) (*it);
			exp->setColor(element->color().red(),
			              element->color().green(), 
			              element->color().blue());
			exp->moveto(x - 21, y - yhalf);
			exp->lineto(x - 5, y - yhalf);
			exp->stroke(element->lineThickness(), 
			            (line_dash)element->dash());
		}
		y += exp->getFontHeight();
	}
}

