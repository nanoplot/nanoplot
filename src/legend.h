/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LEGEND_H
#define LEGEND_H

#include <QtGui>
#include <string>
#include "chartelement.h"
#include "xy.h"

using namespace std;
using namespace nplot;

/**
 *   Class drawing legend on the chart.
 */
class Legend: public ChartElement
{
public:
	Legend(XY *chart);   /**< A constructor */

	/**  A destructor */
	~Legend() {};
	
	elem_type type() const  { return elem_unknown; };
	
	/**
	 *   Loads the state.
	 *   \param reader session reader
	 */
	void load(SessionReader *reader);

	void save(SessionWriter *writer) const;
	
	/**
	 *   Gives font size
	 *   \return Font size
	 */
	int fontSize() const;

	/**
	 *   Sets the font size
	 *   \param size is font size to be set
	 */
	void setFontSize(int size);
	
	/**
	 *   Called when repaint is necessary
	 *   \param exp is pointer to Export used to draw with
	 */
	void repaint(Export *exp) const;
private:
	int vfontSize;             /**< Holds the font size */
	XY *xychart;               /**< Holds pointer to parent XY widget */
};

#endif /* LEGEND_H */
