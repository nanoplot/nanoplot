/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2010, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "options.h"
#include "mainwindow.h"
#include "napp.h"
#include "gettext.h"
#include <QtGui>
#include <iostream>
#include <ltdl.h>

using namespace std;

int main(int argc, char *argv[])
{
	#ifdef ENABLE_NLS
		/* Setting up gettext */
		setlocale(LC_ALL, "");
		setlocale(LC_NUMERIC, "C");
		bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
		textdomain(GETTEXT_PACKAGE);
	#endif /* ENABLE_NLS */

	nApp app(argc, argv);
	QApplication qtApp(argc, argv);
	QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF8"));
	
	QFontDatabase::removeAllApplicationFonts();
	if (QFontDatabase::addApplicationFont(datadir
	                                      "fonts/cmmi10.ttf") == -1)
	{
		cerr << "Err: Cant load font fonts/cmmi10.ttf!" << endl;
		return 0;
	}
	if (QFontDatabase::addApplicationFont(datadir
	                                      "fonts/cmsy10.ttf") == -1)
	{
		cerr << "Err: Cant load font fonts/cmsy10.ttf" << endl;
		return 0;
	}
	if (QFontDatabase::addApplicationFont(datadir
	                                      "fonts/msbm10.ttf") == -1)
	{
		cerr << "Err: Cant load font fonts/msbm10.ttf" << endl;
		return 0;
	}
	if (QFontDatabase::addApplicationFont(datadir
	                                      "fonts/cmmib10.ttf") == -1)
	{
		cerr << "Err: Cant load font fonts/cmmib10.ttf" << endl;
		return 0;
	}

	
	MainWindow form;
	form.show();
	return qtApp.exec();
}
