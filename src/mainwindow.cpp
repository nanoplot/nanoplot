/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2010, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "options.h"
#include <fstream>
#include <gettext.h>
#include <iostream>
#include <vector>
#include <zlib.h>
#include "mainwindow.h"
#include "napp.h"
#include "nplotnamespace.h"
#include "sessionwriter.h"
#include "sessionreader.h"
#include "images/filenew.xpm"
#include "images/fileimport.xpm"
#include "images/fileopen.xpm"
#include "images/filesaveas.xpm"
#include "images/newnote.xpm"
#include "images/help.xpm"
#include "images/coords.xpm"
#include "images/datamode.xpm"
#include "images/nicon.xpm"

#define _(String) (gettext(String))

using namespace std;
using namespace nplot;

MainWindow::MainWindow()
{
	workspace = new QWorkspace;
	setCentralWidget(workspace);

	connect(workspace, SIGNAL(windowActivated(QWidget*)), this,
	        SLOT(updateMenus(QWidget*)));
	
	setWindowTitle("nPlot");
	setWindowIcon(QIcon(nicon_xpm));
	createMenu();
	createToolbar();

	pointer = this;
}

list<XY*> MainWindow::xylist = list<XY*>();

list<WSWindow*> MainWindow::windowlist = list<WSWindow*>();

MainWindow* MainWindow::pointer = NULL;

void MainWindow::createMenu()
{
	/*************** F I L E    M E N U *****************/

	filemenu = menuBar()->addMenu(_("&File"));
	QAction *filemenu1 = new QAction(_("New sheet"), this);
	filemenu1->setShortcut(Qt::CTRL | Qt::Key_N);
	filemenu->addAction(filemenu1);
	connect(filemenu1, SIGNAL(triggered()), this, SLOT(fileNew()));

	QAction *filemenu2 = new QAction(_("Open project"), this);
	filemenu2->setShortcut(Qt::CTRL | Qt::Key_O);
	filemenu->addAction(filemenu2);
	connect(filemenu2, SIGNAL(triggered()), this, SLOT(fileOpen()));

	QAction *filemenu3 = new QAction(_("Save project as..."), this);
	filemenu->addAction(filemenu3);
	connect(filemenu3, SIGNAL(triggered()), this, SLOT(fileSave()));

	QAction *filemenu4 = new QAction(_("Import ASCII"), this);
	filemenu4->setShortcut(Qt::CTRL | Qt::Key_I);
	filemenu->addAction(filemenu4);
	connect(filemenu4, SIGNAL(triggered()), this, SLOT(importASCII()));

	QAction *filemenu5 = new QAction(_("Quit"), this);
	filemenu5->setShortcut(Qt::CTRL | Qt::Key_Q);
	filemenu->addAction(filemenu5);
	connect(filemenu5, SIGNAL(triggered()), this, SLOT(close()));

	/********* S P R E A D S H E E T   M E N U **********/

	sheetmenu = MainWindow::menuBar()->addMenu(_("&Spreadsheet"));
	QAction *sheetmenu1 = new QAction(_("Rows..."), this);
	sheetmenu->addAction(sheetmenu1);
	connect(sheetmenu1, SIGNAL(triggered()), this, SLOT(setRowCount()));
	
	QAction *sheetmenu2 = new QAction(_("Columns..."), this);
	sheetmenu->addAction(sheetmenu2);
	connect(sheetmenu2, SIGNAL(triggered()), this, SLOT(setColCount()));
	
	mkchartmenu = sheetmenu->addMenu(_("Make chart"));

	QAction *mkchartmenu1 = new QAction(_("XY"), this);
	mkchartmenu->addAction(mkchartmenu1);
	connect(mkchartmenu1, SIGNAL(triggered()), this, SLOT(makeChartXY()));

	QAction *mkchartmenu2 = new QAction(_("XYDX"), this);
	mkchartmenu->addAction(mkchartmenu2);
	connect(mkchartmenu2, SIGNAL(triggered()), this,
	        SLOT(makeChartXYDX()));

	QAction *mkchartmenu3 = new QAction(_("XYDY"), this);
	mkchartmenu->addAction(mkchartmenu3);
	connect(mkchartmenu3, SIGNAL(triggered()), this, SLOT(makeChartXYDY()));

	QAction *mkchartmenu4 = new QAction(_("XYDXDY"), this);
	mkchartmenu->addAction(mkchartmenu4);
	connect(mkchartmenu4, SIGNAL(triggered()), this,
	        SLOT(makeChartXYDXDY()));

	QMenu *sexprtmenu = sheetmenu->addMenu(_("Export"));

	QAction *sexprtmenu1 = new QAction(_("ASCII"), this);
	sexprtmenu->addAction(sexprtmenu1);
	connect(sexprtmenu1, SIGNAL(triggered()), this, SLOT(exportASCII()));

	QAction *sexprtmenu2 = new QAction(_("LaTeX tabular"), this);
	sexprtmenu->addAction(sexprtmenu2);
	connect(sexprtmenu2, SIGNAL(triggered()), this,
	        SLOT(exportLatexTab()));

	/*************** C H A R T    M E N U ***************/

	chartmenu = MainWindow::menuBar()->addMenu(_("&Chart"));

	QAction *chartmenu0 = new QAction(_("Chart settings"), this);
	chartmenu0->setShortcut(Qt::Key_S);
	chartmenu->addAction(chartmenu0);
	connect(chartmenu0, SIGNAL(triggered()), this, SLOT(chartSettings()));
	
	QAction *chartmenu1 = new QAction(_("Zoom to fit"), this);
	chartmenu1->setShortcut(Qt::Key_A);
	chartmenu->addAction(chartmenu1);
	connect(chartmenu1, SIGNAL(triggered()), this, SLOT(fitZoom()));

	/* TRANSLATORS: Verb; fit a curve */
	QMenu *fitmenu = chartmenu->addMenu(_("Fit"));
	
	/* TRANSLATORS: Adjective; linear fitting */
	QAction *fitmenu0 = new QAction(_("Linear"), this);
	fitmenu->addAction(fitmenu0);
	connect(fitmenu0, SIGNAL(triggered()), this, SLOT(makeLinReg()));

	/* TRANSLATORS: Adjective; quadratic fitting */
	QAction *fitmenu1 = new QAction(_("Quadratic"), this);
	fitmenu->addAction(fitmenu1);
	connect(fitmenu1, SIGNAL(triggered()), this, SLOT(makeQuadReg()));

	/* TRANSLATORS: Adjective; cubic fitting */
	QAction *fitmenu2 = new QAction(_("Cubic"), this);
	fitmenu->addAction(fitmenu2);
	connect(fitmenu2, SIGNAL(triggered()), this, SLOT(makeCubicReg()));

	/* TRANSLATORS: Adjective; quart fitting */
	QAction *fitmenu3 = new QAction(_("Quart"), this);
	fitmenu->addAction(fitmenu3);
	connect(fitmenu3, SIGNAL(triggered()), this, SLOT(makeQuartReg()));

	/* TRANSLATORS: Adjective; polynomial fitting */
	QAction *fitmenu4 = new QAction(_("Polynomial"), this);
	fitmenu->addAction(fitmenu4);
	connect(fitmenu4, SIGNAL(triggered()), this, SLOT(makePolyReg()));
	
	/* TRANSLATORS: Adjective; exponential fitting */
	QAction *fitmenu5 = new QAction(_("Exponential"), this);
	fitmenu->addAction(fitmenu5);
	connect(fitmenu5, SIGNAL(triggered()), this, SLOT(makeExpReg()));
	
	/* TRANSLATORS: Adjective; logarithmic fitting */
	QAction *fitmenu6 = new QAction(_("Logarithmic"), this);
	fitmenu->addAction(fitmenu6);
	connect(fitmenu6, SIGNAL(triggered()), this, SLOT(makeLnReg()));

	/* TRANSLATORS: Adjective; power fitting */
	QAction *fitmenu7 = new QAction(_("Power"), this);
	fitmenu->addAction(fitmenu7);
	connect(fitmenu7, SIGNAL(triggered()), this, SLOT(makePwrReg()));
	
	/* TRANSLATORS: Adjective; gauss fitting */
	QAction *fitmenu8 = new QAction(_("Gaussian"), this);
	fitmenu->addAction(fitmenu8);
	connect(fitmenu8, SIGNAL(triggered()), this, SLOT(makeGaussReg()));

	QAction *chartmenu2 = new QAction(_("Find peaks"), this);
	chartmenu->addAction(chartmenu2);
	connect(chartmenu2, SIGNAL(triggered()), this, SLOT(findPeaks()));

	QMenu *exprtmenu = chartmenu->addMenu(_("Export"));

	const vector<export_info> exports = nApp::instance()->exports();
	for (unsigned int i = 0; i < exports.size(); ++i) {
		export_info info = exports[i];
		QAction *act = new QAction(info.name_abbrev, this);
		act->setData(QVariant(i));
		exprtmenu->addAction(act);
		connect(act, SIGNAL(triggered()), this, SLOT(exportChart()));
	}
	
	/************** W I N D O W S  M E N U **************/

	windowmenu = MainWindow::menuBar()->addMenu(_("&Windows"));
	createWindowMenu();
	
	/**************** H E L P   M E N U *****************/

	helpmenu = MainWindow::menuBar()->addMenu(_("&Help"));
	QAction *helpmenu1 = new QAction(_("Plugins"), this);
	helpmenu->addAction(helpmenu1);
	connect(helpmenu1, SIGNAL(triggered()), this, SLOT(settingsDialog()));

	QAction *helpmenu2 = new QAction(_("About"), this);
	helpmenu->addAction(helpmenu2);
	connect(helpmenu2, SIGNAL(triggered()), this, SLOT(about()));

	menuBar()->clear();
	menuBar()->addMenu(filemenu);
	menuBar()->addMenu(helpmenu);
}

void MainWindow::createWindowMenu()
{
	windowmenu->clear();

	if (workspace->activeWindow() != NULL) {
		/* TRANSLATORS: Next window */
		QAction *windowmenu1 = new QAction(_("Next"), this);
		windowmenu1->setShortcut(Qt::Key_F6);
		windowmenu->addAction(windowmenu1);
		connect(windowmenu1, SIGNAL(triggered()), workspace,
		        SLOT(activateNextWindow()));
	
		/* TRANSLATORS: Previous window */
		QAction *windowmenu2 = new QAction(_("Previous"), this);
		windowmenu2->setShortcut(Qt::Key_F5);
		windowmenu->addAction(windowmenu2);
		connect(windowmenu2, SIGNAL(triggered()), workspace,
		        SLOT(activatePreviousWindow()));
	
		/* TRANSLATORS: Rename window */
		QAction *windowmenu3 = new QAction(_("Rename"), this);
		windowmenu3->setShortcut(Qt::Key_F2);
		windowmenu->addAction(windowmenu3);
		connect(windowmenu3, SIGNAL(triggered()), this,
		        SLOT(renameWindow()));
	
		windowmenu->addSeparator();
	}

	/* TRANSLATORS: Cascade windows */
	QAction *windowmenu4 = new QAction(_("Cascade"), this);
	windowmenu->addAction(windowmenu4);
	connect(windowmenu4, SIGNAL(triggered()), workspace, SLOT(cascade()));

	/* TRANSLATORS: Tile windows */
	QAction *windowmenu5 = new QAction(_("Tile"), this);
	windowmenu->addAction(windowmenu5);
	connect(windowmenu5, SIGNAL(triggered()), workspace, SLOT(tile()));
	
	windowmenu->addSeparator();

	/* TRANSLATORS: Hide all windows */
	QAction *windowmenu6 = new QAction(_("Hide all"), this);
	windowmenu->addAction(windowmenu6);
	connect(windowmenu6, SIGNAL(triggered()), this,
	        SLOT(hideAllWindows()));
	
	/* TRANSLATORS: Close all windows */
	QAction *windowmenu7 = new QAction(_("Close all"), this);
	windowmenu->addAction(windowmenu7);
	connect(windowmenu7, SIGNAL(triggered()), workspace,
	        SLOT(closeAllWindows()));
	
	windowmenu->addSeparator();

	QActionGroup *group = new QActionGroup(this);
	group->setExclusive(true);
	for (list<WSWindow*>::iterator it = windowlist.begin();
	     it != windowlist.end(); ++it)
	{
		QAction *windowmenu4 = new QAction((*it)->windowTitle(),
		                                   group);
		windowmenu4->setCheckable(true);
		if (*it == workspace->activeWindow())
			windowmenu4->setChecked(true);
		group->addAction(windowmenu4);
		connect(windowmenu4, SIGNAL(triggered()), *it, SLOT(show()));
		connect(windowmenu4, SIGNAL(triggered()), *it,
		        SLOT(setFocus()));
		windowmenu->addAction(windowmenu4);
	}
	if (!group->actions().isEmpty())
		menuBar()->addMenu(windowmenu);
}

void MainWindow::updateMenus(QWidget *wdw)
{
	if (wdw) {
		menuBar()->clear();
		menuBar()->addMenu(filemenu);
		int type = ((WSWindow*) wdw)->type();
		if (type == wdw_spreadsheet) {           /* Spreadsheet */
			menuBar()->addMenu(sheetmenu);
			xytoolbar->setDisabled(true);
		} else if (type == wdw_chart) {          /* Chart */
			menuBar()->addMenu(chartmenu);
			xytoolbar->setEnabled(true);
			switch (((Chart*) wdw)->dataReaderMode()) {
			case 1:
				xytbar1->setChecked(true);
				xytbar2->setChecked(false);
				break;
			case 2:
				xytbar1->setChecked(false);
				xytbar2->setChecked(true);
				break;
			default:
				xytbar1->setChecked(false);
				xytbar2->setChecked(false);
			}
		} else if (type == wdw_notepad) {        /* Notepad */
			xytoolbar->setDisabled(true);
		}
		createWindowMenu();
		menuBar()->addMenu(helpmenu);
	} else {
		menuBar()->clear();
		menuBar()->addMenu(filemenu);
		createWindowMenu();
		menuBar()->addMenu(helpmenu);
		xytoolbar->setDisabled(true);
	}
}

void MainWindow::addWindow(QWidget *wdw)
{
	workspace->addWindow(wdw);
}

void MainWindow::setRowCount()
{
	((SpreadSheet*) workspace->activeWindow())->table()->setRows();
}

void MainWindow::setColCount()
{
	((SpreadSheet*) workspace->activeWindow())->table()->setCols();
}

void MainWindow::makeChartXY()
{
	((SpreadSheet*)
	 workspace->activeWindow())->table()->makeChart(elem_xy);
}

void MainWindow::makeChartXYDX()
{
	((SpreadSheet*)
	 workspace->activeWindow())->table()->makeChart(elem_xydx);
}

void MainWindow::makeChartXYDY()
{
	((SpreadSheet*)
	 workspace->activeWindow())->table()->makeChart(elem_xydy);
}

void MainWindow::makeChartXYDXDY()
{
	((SpreadSheet*)
	 workspace->activeWindow())->table()->makeChart(elem_xydxdy);
}

void MainWindow::chartSettings()
{
	((Chart*) workspace->activeWindow())->chartWidget()->settings();
}

void MainWindow::fitZoom()
{
	((Chart*) workspace->activeWindow())->chartWidget()->fitZoom();
}

void MainWindow::makeLinReg()
{
	Chart *chart = (Chart*) workspace->activeWindow();
	chart->chartWidget()->fit(fit_linear);
}

void MainWindow::makeQuadReg()
{
	Chart *chart = (Chart*) workspace->activeWindow();
	chart->chartWidget()->fit(fit_quad);
}

void MainWindow::makeCubicReg()
{
	Chart *chart = (Chart*) workspace->activeWindow();
	chart->chartWidget()->fit(fit_cubic);
}

void MainWindow::makeQuartReg()
{
	Chart *chart = (Chart*) workspace->activeWindow();
	chart->chartWidget()->fit(fit_quart);
}

void MainWindow::makePolyReg()
{
	Chart *chart = (Chart*) workspace->activeWindow();
	chart->chartWidget()->fit(fit_poly);
}

void MainWindow::makeExpReg()
{
	Chart *chart = (Chart*) workspace->activeWindow();
	chart->chartWidget()->fit(fit_exp);
}

void MainWindow::makeLnReg()
{
	Chart *chart = (Chart*) workspace->activeWindow();
	chart->chartWidget()->fit(fit_log);
}

void MainWindow::makePwrReg()
{
	Chart *chart = (Chart*) workspace->activeWindow();
	chart->chartWidget()->fit(fit_power);
}

void MainWindow::makeGaussReg()
{
	Chart *chart = (Chart*) workspace->activeWindow();
	chart->chartWidget()->fit(fit_gauss);
}

void MainWindow::findPeaks()
{
	((Chart*) workspace->activeWindow())->chartWidget()->findPeaks();
}

void MainWindow::exportChart()
{
	int export_id = ((QAction*) sender())->data().toInt();
	export_info info = nApp::instance()->exports()[export_id];
	
	QString fr = QFileDialog::getSaveFileName(this, info.name_full,
			Dialogs::currentDir,
			QString(info.name_abbrev) + " (*." + info.fext + ')');

	if (fr.isEmpty())
		return;

	Dialogs::setCurrentDir(fr);
	Export *exp;

	if (info.ask_dim) {
		int width, height;
		if (!Dialogs::getResolution(&width, &height))
			return;
		exp = info.instantiate((const char*) fr.toAscii(), width,
		                       height);
	} else {
		exp = info.instantiate((const char*) fr.toAscii(), IMG_WIDTH,
		                       IMG_HEIGHT);
	}

	XY *xychart = ((Chart*) workspace->activeWindow())->chart();
	xychart->calcMargins(exp);
	xychart->doExport(exp);
	xychart->calcMargins();
	delete exp;
}

void MainWindow::exportASCII()
{
	((SpreadSheet*) workspace->activeWindow())->exportASCII();
}

void MainWindow::exportLatexTab()
{
	((SpreadSheet*) workspace->activeWindow())->exportLatexTab();
}

void MainWindow::renameWindow()
{
	bool ok;
	QWidget *window = workspace->activeWindow();
	QString oldTitle = window->windowTitle();
	QString newTitle = QInputDialog::getText(this, _("Rename"),
	                   _("New name:"), QLineEdit::Normal, oldTitle, &ok);
	
	if (ok) {
		window->setWindowTitle(newTitle);
		updateMenus();
	}
}

void MainWindow::hideAllWindows()
{
	for (list<WSWindow*>::iterator it=windowlist.begin();
	     it!=windowlist.end(); ++it)
	{
		(*it)->hide();
	}
}

void MainWindow::createToolbar()
{
	toolbar = addToolBar(_("Toolbar"));
	QAction *tbar1 = new QAction(QIcon(filenew_xpm), _("New sheet"),
	                             toolbar);
	toolbar->addAction(tbar1);
	connect(tbar1, SIGNAL(triggered()), this, SLOT(fileNew()));

	QAction *tbar2 = new QAction(QIcon(fileimport_xpm),
	                             _("Import ASCII"), toolbar);
	toolbar->addAction(tbar2);
	connect(tbar2, SIGNAL(triggered()), this, SLOT(importASCII()));

	toolbar->addSeparator();
	
	QAction *tbar3 = new QAction(QIcon(fileopen_xpm), _("Open project"),
	                             toolbar);
	toolbar->addAction(tbar3);
	connect(tbar3, SIGNAL(triggered()), this, SLOT(fileOpen()));

	QAction *tbar4 = new QAction(QIcon(filesaveas_xpm),
	                                   _("Save project as"), toolbar);
	toolbar->addAction(tbar4);
	connect(tbar4, SIGNAL(triggered()), this, SLOT(fileSave()));
	
	toolbar->addSeparator();

	QAction *tbar5 = new QAction(QIcon(newnote_xpm), _("New note"),
	                             toolbar);
	toolbar->addAction(tbar5);
	connect(tbar5, SIGNAL(triggered()), this, SLOT(notepadNew()));

	QAction *tbar6 = new QAction(QIcon(help_xpm), _("About"), toolbar);
	toolbar->addAction(tbar6);
	connect(tbar6, SIGNAL(triggered()), this, SLOT(about()));

	xytoolbar = addToolBar(_("Chart toolbar"));
	xytbar1 = new QAction(QIcon(coords_xpm), _("Show coordinates"),
	                      xytoolbar);
	xytbar1->setCheckable(true);
	xytoolbar->addAction(xytbar1);
	connect(xytbar1, SIGNAL(triggered(bool)), this, SLOT(setCoords(bool)));

	xytbar2 = new QAction(QIcon(datamode_xpm), _("Data reader"),
	                      xytoolbar);
	xytbar2->setCheckable(true);
	xytoolbar->addAction(xytbar2);
	connect(xytbar2, SIGNAL(triggered(bool)), this,
	        SLOT(setDataMode(bool)));
	xytoolbar->setDisabled(true);
}

void MainWindow::fileNew()
{
	int rows,cols;
	if (!Dialogs::getRowsCols(&rows, &cols))
		return;

	vector< vector<double> > data;	
	for (int i = 0; i < cols; ++i)
		data.push_back(vector<double>(rows,0));

	SpreadSheet *spreadsheet = new SpreadSheet(data, this);
	workspace->addWindow(spreadsheet);
	spreadsheet->show();
}

void MainWindow::fileOpen()
{
	QString fname = QFileDialog::getOpenFileName(this,
	                                   _("Open project file..."),
	                                   Dialogs::currentDir,
	                                   _("nPlot project files (*.npf)"));
	if (fname.isEmpty())
		return;
	
	workspace->closeAllWindows();

	/* Setting locale to "C"; project files are not localized */
	setlocale(LC_NUMERIC, "C");
	SessionReader *reader = new LibXMLReader((const char*)
	                                         fname.toAscii());
	reader->processChildren();
	for (const char *name = reader->nextNode(); name;
	     name = reader->nextNode())
	{
		if (strcmp(name, "Vars") == 0) {
			SpreadSheet::counter =
			           reader->getPropertyInt("spreadsheetcount");
			Chart::counter = reader->getPropertyInt("chartcount");
			Notepad::counter =
			               reader->getPropertyInt("notepadcount");
		} else if (strcmp(name, "SpreadSheet") == 0) {
			SpreadSheet *spreadsheet = new SpreadSheet(reader,
			                                           this);
			workspace->addWindow(spreadsheet);
			spreadsheet->updateVisibility();
		} else if (strcmp(name, "Chart") == 0) {
			Chart *chart = new Chart(reader, this);
			workspace->addWindow(chart);
			chart->updateVisibility();
		} else if (strcmp(name, "Notepad") == 0) {
			Notepad *notepad = new Notepad(reader, this);
			workspace->addWindow(notepad);
			notepad->updateVisibility();
		}
	}
	delete reader;

	updateMenus(workspace->activeWindow());
}

void MainWindow::fileSave()
{
	QString fname = QFileDialog::getSaveFileName(this,
	                                   _("Save project as..."),
	                                   Dialogs::currentDir,
	                                   _("nPlot project files (*.npf)"));
	if (fname.isEmpty())
		return;

	/* Setting locale to "C" to avoid localized project files */
	setlocale(LC_NUMERIC, "C");
	SessionWriter *writer = new LibXMLWriter((const char*)
	                                         fname.toAscii());
	writer->startElement("Vars");
	writer->setProperty("spreadsheetcount", SpreadSheet::counter);
	writer->setProperty("chartcount", Chart::counter);
	writer->setProperty("notepadcount", Notepad::counter);
	writer->endElement();
	for (list<WSWindow*>::iterator it=windowlist.begin();
	     it!=windowlist.end(); ++it)
	{
		(*it)->save(writer);
	}
	delete writer;
}

void MainWindow::importASCII()
{
	double d;
	char   c;
	char  *rest;
	bool   comment = false;

	QString filename;
	ImportDialog importDialog(&filename);
	if (!importDialog.exec())
		return;

	ifstream infile((const char*) filename.toAscii());
	
	if (!infile.good()) {
		QMessageBox::information(this, _("Information"),
		                         _("Can't read file!"));
		return;
	}

	string buf;
	unsigned int n = nApp::settings->ncols;
	
	/* Setting locale to "C" to avoid localization issues */
	setlocale(LC_ALL, "C");

	/* Guessing n */
	if (n == 0) {
		n++;
		while (!infile.eof()) {
			c = infile.get();
			if (c == nApp::settings->commentSign) { 
				comment=true;
			} else {
				if (comment) {
					if (c == '\n' || c == '\r')
						comment = false;
				} else {
					if (c == '\n' || c == '\r')
						break;
					/* If found decimal separator,
					 * change it to period */
					if (c == nApp::settings->decimalSeparator)
						c = '.';
					if ((c >= '0' && c <= '9') || c == '+'
					    || c == '-' || c == '.' || c == 'e'
					    || c == 'E')
					{
						buf += c;
					} else if (buf != "") {
						d = strtod(buf.c_str(), &rest);
						if (strlen(rest) != buf.size())
							n++;
						buf = "";
					}
				}
			}
		}
		comment = false;
		buf = "";
		infile.seekg(0);
	}
	vector< vector<double> > data(n);

	unsigned int i = 0;
	while (!infile.eof()) {
		c = infile.get();
		if (c == nApp::settings->commentSign) {
			comment = true;
		} else {
			if (comment) {
				if (c == '\n' || c == '\r')
					comment = false;
			} else {
				if (c == nApp::settings->decimalSeparator)
					c = '.';
				if ((c >= '0' && c <= '9') || c == '+'
				    || c == '-'|| c == '.' || c == 'e'
				    || c == 'E')
				{
					buf += c;
				} else if (buf != "") {
					d = strtod(buf.c_str(), &rest);
					if (strlen(rest) != buf.size()) {
						data[i].push_back(d);
						i++;
						if (i == n)
							i = 0;
					}
					buf = "";
				}
			}
		}
	}
	for (unsigned int j = 1; j < n; ++j) {
		if (data[j].size() < data[0].size())
			data[j].push_back(0);
	}

	setlocale(LC_ALL, "");

	if (data[0].size() >= n) {
		SpreadSheet *spreadsheet = new SpreadSheet(data, this);
		workspace->addWindow(spreadsheet);
		spreadsheet->show();
		Dialogs::id_filename = filename;
		Dialogs::setCurrentDir(filename);
	} else {
		QMessageBox::information(this, _("Information"), 
		                        _("There is no data in this file!"));
	}
}

void MainWindow::notepadNew()
{
	Notepad *np = new Notepad(this);
	addWindow(np);
	np->show();
}

void MainWindow::about()
{
	HelpDialog *helpdialog = new HelpDialog(this);
	helpdialog->show();
}

void MainWindow::settingsDialog()
{
	Dialogs::settings();
}

void MainWindow::setCoords(bool b)
{
	if (b) {
		xytbar2->setChecked(false);
		((Chart*) workspace->activeWindow())->setDataReaderMode(1);
	} else if (!xytbar2->isChecked()) {
		((Chart*) workspace->activeWindow())->hideDataReader();
	}
}

void MainWindow::setDataMode(bool b)
{
	if (b) {
		xytbar1->setChecked(false);
		((Chart*) workspace->activeWindow())->setDataReaderMode(2);
	} else if (!xytbar1->isChecked()) {
		((Chart*) workspace->activeWindow())->hideDataReader();
	}
}

void MainWindow::closeEvent(QCloseEvent *event)
{
	int ret = QMessageBox::warning(this, _("Information"),
	                               _("Are you sure you want to quit?"),
	                               _("&OK"), _("&Cancel"), NULL, 1);

	if (ret == 0) {
		event->accept();
		qApp->quit();
	} else {
		event->ignore();
	}	
}

