/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui>
#include <list>
#include "dialogs.h"
#include "notepad.h"
#include "spreadsheet.h"
#include "wswindow.h"

using namespace std;

/**
 *   Main window class
 */
class MainWindow: public QMainWindow
{
Q_OBJECT
public:
	/**  A constructor */
	MainWindow();

	/**  A destructor */
	~MainWindow() {};
private:
	void createMenu();       /**< Creates all the menu actions */
	void createWindowMenu(); /**< Responsible for up-to-date window menu */
	void createToolbar();    /**< Creates toolbar actions */
public slots:
	/**
	 *   Adds a window to workspace.
	 *   \param window window to add
	 */
	void addWindow(QWidget *window);

	/**  Invokes appropriate dialog and creates a new spreadsheet*/
	void fileNew();

	/**  Invokes appropriate dialog opening a saved project file */
	void fileOpen();
	
	/**  Invokes appropriate dialog for saving a project file */
	void fileSave();

	/**  Invokes appropriate dialog importing ASCII data file */
	void importASCII();

	/**  Creates a new notepad */
	void notepadNew();

	/**  Invokes an about dialog */
	void about();

	void settingsDialog();

	/**
	 *   Set coords displaying for chart.
	 *   \param b if \c true, sets it on; off otherwise
	 */
	void setCoords(bool b);

	/**
	 *   Set data mode for chart.
	 *   \param b if \c true, it on; off otherwise
	 */
	void setDataMode(bool b);

	/**  Invokes a dialog for setting row count */
	void setRowCount();

	/**  Invokes a dialog for setting column count */
	void setColCount();

	/**  Invokes a dialog for making XY chart */
	void makeChartXY();

	/**  Invokes a dialog for making XY chart with X error-bars */
	void makeChartXYDX();

	/**  Invokes a dialog for making XY chart with Y error-bars */
	void makeChartXYDY();

	/**
	 *   Invokes a dialog for making XY chart with both X and Y
	 *   error-bars
	 */
	void makeChartXYDXDY();

	/**  Fits chart boundaries do data */
	void fitZoom();

	/**  Invokes a dialog for making a linear regression */
	void makeLinReg();

	/**  Invokes a dialog for making a quadratic regression */
	void makeQuadReg();

	/**  Invokes a dialog for making a cubic regression */
	void makeCubicReg();

	/**  Invokes a dialog for making a quart regression */
	void makeQuartReg();

	/**  Invokes a dialog for making a polynomial regression */
	void makePolyReg();

	/**  Invokes a dialog for making a exponential regression */
	void makeExpReg();

	/**  Invokes a dialog for making a logarithmic regression */
	void makeLnReg();

	/**  Invokes a dialog for making a power regression */
	void makePwrReg();

	/**  Invokes a dialog for making a gaussian regression */
	void makeGaussReg();

	/**  Invokes a dialog for peak seeking */
	void findPeaks();

	/**  Exports chart */
	void exportChart();

	/**  Invokes a dialog for exporting ASCII */
	void exportASCII();

	/**  Invokes a dialog for exporting LaTeX tabular environment */
	void exportLatexTab();

	/**  Invokes a chart setting dialog */
	void chartSettings();

	/** Invokes a dialog to rename active window */
	void renameWindow();

	/**  Hides all windows */
	void hideAllWindows();

	/**
	 *   Updates menu according to selected window
	 *   \param wdw selected window
	 */
	void updateMenus(QWidget *wdw = 0);
protected slots:
	/**  Event invoked when user presses close window button */
	void closeEvent(QCloseEvent*); 
public:
	static list<XY*> xylist;           /**< List of app XY widgets */
	static list<WSWindow*> windowlist; /**< List of all WS windows */
	static MainWindow *pointer;        /**< Pointer to self */
	/* FIXME: we really need it to be public? */
	QWorkspace *workspace;             /**< Workspace */
private:
	/* FIXME: check if we really need all this stuff */
	QMenu *filemenu;                   /**< File menu */
	QMenu *sheetmenu;                  /**< Spreadsheet menu */
	QMenu *mkchartmenu;                /**< Make chart menu */
	QMenu *chartmenu;                  /**< Chart menu */
	QMenu *windowmenu;                 /**< Window menu */
	QMenu *helpmenu;                   /**< Help menu */
	QToolBar *toolbar;                 /**< Main toolbar */
	QToolBar *xytoolbar;               /**< XY toolbar */
	QAction *xytbar1;                  /**< Show coordinates action */
	QAction *xytbar2;                  /**< Data reader action */
};

#endif /* MAINWINDOW_H */
