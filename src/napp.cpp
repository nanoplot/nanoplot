/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "napp.h"
#include "common.h"
#include "config.h"
#include "options.h"
#include <ltdl.h>
#include <iostream>
#include <string.h>
#include <stdio.h>

using namespace std;

Settings::Settings()
{
	Cfg *conf = Cfg::instance();
	string s;
	s = conf->getVar("decimalseparator");
	decimalSeparator = (s.size() != 0)? s.data()[0] : '.';
	s = conf->getVar("commentsign");
	commentSign = (s.size() != 0)? s.data()[0] : '#';
	s = conf->getVar("ncols");
	ncols = (s.size() != 0)? atoi(s.c_str()) : 0;
	if (conf->varExists("plugins")) {
		s = conf->getVar("plugins");
		string tmp;
		for (string::iterator it = s.begin(); it != s.end(); ++it) {
			if ((*it) == ' ' && !tmp.empty()) {
				plugins.push_back(tmp);
				tmp.clear();
				continue;
			}
			tmp += (*it);
		}
		if (!tmp.empty())
				plugins.push_back(tmp);
	} else {
		plugins = nApp::instance()->listPlugins();
	}
}

Settings::~Settings()
{
	Cfg *conf = Cfg::instance();
	string s;
	s = decimalSeparator;                   /* Save decimalseparator */
	conf->setVar("decimalseparator", s);
	s = commentSign;                        /* Save commentsign */
	conf->setVar("commentsign", s);
	char c[3];                              /* Save ncols */
	sprintf(c, "%d", ncols);
	s = c;
	conf->setVar("ncols", s);
	s.clear();
	s += '"';
	for (vector<string>::iterator it = plugins.begin();
	     it != plugins.end(); ++it)
	{
		s += *it + ' ';
	}
	s += '"';
	conf->setVar("plugins", s);
	conf->save();
	Cfg::destroy();
}

bool Settings::pluginIsOn(string name)
{
	for (vector<string>::iterator it = plugins.begin();
	     it != plugins.end(); ++it)
	{
		if (*it == name)
			return true;
	}
	return false;
}

nApp::nApp(int argc, char **argv)
{
	pointer = this;
	lt_dlinit();

	registerInternalTypes();

	settings = new Settings;

	loadAllPlugins();
}

nApp::~nApp()
{
	if (settings)
		delete settings;
	lt_dlexit();
}

Settings* nApp::settings = NULL;

nApp* nApp::pointer = NULL;

nApp* nApp::instance() { return pointer; }

int nApp::loadPlugin(const char *filename, void *data)
{
	lt_dlhandle plugin = lt_dlopenext(filename);
	if (plugin == NULL) {
		DMSG(("Failed to open plugin %s\n", filename));
		return 0;
	}

	lt_ptr fct_ptr = lt_dlsym(plugin, "info");
	if (fct_ptr != NULL) {
		plugin_info info = ((plugin_info (*) (void))
		                   fct_ptr)();
		instance()->registerPlugin(info);
		if (!settings->pluginIsOn(info.name_generic))
			return 0;
	}

	fct_ptr = lt_dlsym(plugin, "init");
	if (fct_ptr != NULL)
		((void (*) (void)) fct_ptr)();

	lt_ptr sym_ptr = lt_dlsym(plugin, "symbols");
	if (!sym_ptr) {
		DMSG(("No symbol table\n"));
		return 0;
	}

	exported_symbol *table = (exported_symbol*) sym_ptr;
	for (int i = 0; table[i].type != 0; ++i) {
		if (strcmp("datatype", table[i].type) == 0) {
			fct_ptr = lt_dlsym(plugin, table[i].symbol);
			if (!fct_ptr) {
				DMSG(("Failed to locate symbol %s\n",
				     table[i].symbol));
			} else {
				datatype_info di =
				       ((datatype_info (*) (void)) fct_ptr)();
				instance()->registerType(di);
				DMSG(("Type %s registered\n",
				      table[i].symbol));
			}
		} else if (strcmp("export", table[i].type) == 0) {
			fct_ptr = lt_dlsym(plugin, table[i].symbol);
			if (!fct_ptr) {
				DMSG(("Failed to locate symbol %s\n",
				     table[i].symbol));
			} else {
				export_info di =
				       ((export_info (*) (void)) fct_ptr)();
				instance()->registerExport(di);
				DMSG(("Type %s registered\n",
				      table[i].symbol));
			}
		} else {
			DMSG(("Unknown type: %s\n", table[i].type));
		}
	}
	return 0;
}

int nApp::pluginNames(const char *filename, void *data)
{
	lt_dlhandle plugin = lt_dlopenext(filename);
	if (plugin != NULL) {
		lt_ptr fct_ptr = lt_dlsym(plugin, "info");
		if (fct_ptr != NULL) {
			plugin_info info = ((plugin_info (*) (void))
			                   fct_ptr)();
			((vector<string>*) data)->push_back(info.name_generic);
		}
	}
	return 0;
}

vector<string> nApp::listPlugins()
{
	vector<string> ret;
	lt_dlforeachfile(datadir "plugins", (*pluginNames), &ret);
	return ret;
}

void nApp::loadAllPlugins()
{
	lt_dlforeachfile(datadir "plugins", (*loadPlugin), NULL);
}

void nApp::registerPlugin(plugin_info info)
{
	vplugins.push_back(info);
}

void nApp::registerExport(export_info info)
{
	vexports.push_back(info);
}

void nApp::registerType(datatype_info info)
{
	vtypes.push_back(info);
}

static Variant double_fromString(const char *str)
{
	return Variant(atof(str));
}

static void double_toString(Variant v, char *buf, int buf_len)
{
	snprintf(buf, buf_len, "%lg", v.toDouble());
}

static Variant int_fromString(const char *str)
{
	return Variant (atoi(str));
}

static void int_toString(Variant v, char *buf, int buf_len)
{
	snprintf(buf, buf_len, "%d", v.toInt());
}

void nApp::registerInternalTypes()
{
	datatype_info di = {&double_fromString, &double_toString, type_double,
	                    "double", "double"};
	registerType(di);

	datatype_info ii = {&int_fromString, &int_toString, type_int, "int",
	                    "int"};
	registerType(ii);
}
