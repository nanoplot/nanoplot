/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NAPP_H
#define NAPP_H

#include "nplotnamespace.h"
#include "plugin.h"
#include <vector>

using namespace std;
	
/**
 *   Class providing application-wide settings read from config file.
 */
class Settings
{
public:
	/**
	 *   A constructor.
	 *   Reads the config file and gets saved settings.
	 */
	Settings();

	/**
	 *   A destructor.
	 *   Writes the config file with current settings.
	 */
	~Settings();

	bool pluginIsOn(string name);
public:
	char decimalSeparator;    /**< Decimal separator */
	char commentSign;         /**< Comment sign */
	int ncols;                /**< Number of columns */
	vector<string> plugins;
};

class nApp
{
public:
	nApp(int argc, char **argv);
	~nApp();
	static nApp* instance();
	vector<string> listPlugins();
	void registerPlugin(plugin_info info);
	const vector<plugin_info> plugins()  { return vplugins; };
	void registerExport(export_info info);
	const vector<export_info>& exports() { return vexports; };
	void registerType(datatype_info info);
	const vector<datatype_info>& types() { return vtypes; };
	static Settings *settings;         /**< Application-wide settings */
private:
	static int loadPlugin(const char *filename, void *data);
	static int pluginNames(const char *filename, void *data);
	void registerInternalTypes();
	void loadAllPlugins();
	static nApp *pointer;
	vector<plugin_info> vplugins;
	vector<export_info> vexports;
	vector<datatype_info> vtypes;
};

#endif /* NAPP_H */
