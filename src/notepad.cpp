/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2010, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "options.h"
#include <gettext.h>
#include <glib.h>
#include "notepad.h"
#include "config.h"
#include "nplotnamespace.h"
#include "images/notepad.xpm"
#include "images/lock.xpm"
#include "images/unlock.xpm"

#define _(String) (gettext(String))

using namespace std;

Notepad::Notepad(QWidget *parent): WSWindow(parent)
{
	gchar *title = g_strdup_printf(_("Notepad %d"), counter++);
	setWindowTitle(title);
	Init();

	g_free(title);
}

Notepad::Notepad(const char *note, QWidget *parent): WSWindow(parent)
{
	Init();

	gchar *title = g_strdup_printf(_("Notepad %d"), counter++);
	setWindowTitle(title);
	makeNote(note);
	setReadOnly(true);
	lockBtn->setChecked(true);

	g_free(title);
}

Notepad::Notepad(SessionReader *reader, QWidget *parent): WSWindow(parent)
{
	Init();

	setWindowTitle(reader->getProperty("title"));
	hidden = reader->getPropertyBool("hidden");

	reader->processChildren();
	
	for (const char *name = reader->nextNode(); name;
	     name = reader->nextNode()) {
		if (strcmp(name, "content") == 0)
			textEdit.setPlainText(reader->getValue());
	}
}

void Notepad::Init()
{
	setMinimumWidth(250);
	setMinimumHeight(200);
	setWindowIcon(QIcon(notepad_xpm));

	setCentralWidget(&textEdit);
	lockBtn = new LockButton(this);
	statusBar()->setSizeGripEnabled(false);
	statusBar()->addPermanentWidget(lockBtn);
	connect(lockBtn, SIGNAL(toggled(bool)), this, SLOT(setReadOnly(bool)));
}

int Notepad::type() const
{
	return nplot::wdw_notepad;
}

void Notepad::save(SessionWriter *writer) const
{
	writer->startElement("Notepad");
	writer->setProperty("title", (const char*) windowTitle().toAscii());
	writer->setProperty("hidden", isHidden());
	writer->startElement("content");
	writer->setValue((const char*) textEdit.toPlainText().toAscii());
	writer->endElement();
	writer->endElement();
}

int Notepad::counter = 1;

void Notepad::makeNote(const char *note)
{
	textEdit.setHtml(note);
}

void Notepad::makeNote(const QString note)
{
	textEdit.setHtml(note);
}

void Notepad::setReadOnly(bool b)
{
	textEdit.setReadOnly(b);
}

LockButton::LockButton(QWidget *parent): QAbstractButton(parent)
{
	setFixedWidth(16);
	setFixedHeight(16);
	setCheckable(true);
	connect(this, SIGNAL(toggled(bool)), this, SLOT(setChecked(bool)));
	setToolTip(_("lock"));
}

void LockButton::setChecked(bool b)
{
	setToolTip(b ? _("unlock") : _("lock"));
	QAbstractButton::setChecked(b);
}

void LockButton::paintEvent(QPaintEvent*)
{
	QPainter canvas;
	QImage img = isChecked() ? QImage(lock_xpm) : QImage(unlock_xpm);
	canvas.begin(this);
	canvas.drawImage(0, 0, img);	
	canvas.end();
}

