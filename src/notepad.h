/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NOTEPAD_H
#define NOTEPAD_H

#include <QtGui>
#include <sstream>
#include <string>
#include "sessionreader.h"
#include "wswindow.h"

class LockButton;

/**
 * Notepad window.
 */
class Notepad: public WSWindow 
{
Q_OBJECT
public:
	/**
	 *   A constructor.
	 *   Creates empty notepad.
	 *   \param parent Pointer to parent widget
	 */
	Notepad(QWidget *parent = 0);

	/**
	 *   An alternative constructor.
	 *   Creates notepad with desired content.
	 *   \param content Text to be placed in a notepad
	 *   \param parent Pointer to parent widget
	 */
	Notepad(const char *content, QWidget *parent = 0);

	/**
	 *   A constructor creating notepad using information from a session
	 *   file.
	 *   \param reader session reader
	 *   \param parent Pointer to parent widget
	 */
	Notepad(SessionReader *reader, QWidget *parent = 0);

	/**  A destructor */
	~Notepad() {};

	int type() const;
	
	/**
	 *   Save notepad.
	 *   Method used for saving information to session file.
	 *   \param writer session writer
	 */
	void save(SessionWriter *writer) const;

	static int counter;            /**< Holds the counter of notepads */
public slots:
	/**
	 *   Makes a note in notepad.
	 *   \param note Note to be placed in notepad
	 */
	void makeNote(const char *note);

	/**
	 *   Makes a note using QString.
	 *   \param note Note to be placed in notepad
	 */
	void makeNote(const QString note);

	/**
	 *   Sets whether notepad shold be read only.
	 *   \param b \c true sets notepad read only, \c false makes it writable
	 */
	void setReadOnly(bool b);
private:
	void Init();                   /**< Initializes the notepad */
	QTextEdit textEdit;            /**< Text edit */
	LockButton *lockBtn;           /**< Lock/unlock button */
};

/**
 *   Lock button widget
 */
class LockButton: public QAbstractButton
{
Q_OBJECT
public:
	/**
	 *   A constructor
	 *   \param parent Pointer to parent widget
	 */
	LockButton(QWidget *parent = 0);

	/**  A destructor */
	~LockButton() {};
public slots:
	/**
	 *   Sets widget checked (or unchecked)
	 *   \param b If \c true, the widget will be checked; unchecked otherwise
	 */
	void setChecked(bool b);
protected slots:
	void paintEvent(QPaintEvent*);  /**< Called when repaint is needed */
};

#endif /* NOTEPAD_H */
