/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NPLOTNAMESPACE_H
#define NPLOTNAMESPACE_H

/**
 *   Namespace encapsulating all the enums.
 */
namespace nplot
{
	/**
	 *   Config parser state enum.
	 */
	enum config_parser_state {
		cps_normal,        /**< Parser in normal mode */
		cps_safemode,      /**< Parser in safe mode (inside quotes) */
		/**  Parser in escape mode (next char should be unescaped) */
		cps_escape,
		/**  Parser in skip line mode (current line will  be omitted) */
		cps_skipline 
	};

	/**
	 *   Window type enum.
	 */
	enum wdw_type {
		wdw_unknown = 0,   /**< Unknown window */
		wdw_spreadsheet,   /**< Spreadsheet window */
		wdw_chart,         /**< Chart window */
		wdw_notepad        /**< Notepad window */
	};
	
	/**
	 *   ChartElement type enum.
	 */
	enum elem_type {
		elem_unknown = 0,  /**< Unknown element */
		elem_xy = 1,       /**< XY series */
		elem_xydx = 3,     /**< XY series with X-error bars */
		elem_xydy = 5,     /**< XY series with Y-error bars */
		elem_xydxdy = 7,   /**< XY series with both X nad Y-error bars */
		elem_func = 8      /**< Function */
	};
	
	enum data_type {
		type_unknown = -1,
		type_double = 0,
		type_int = 1,
	};

	/**
	 *   NPL directives enum.
	 */
	enum npl {
		npl_end,           /**< Ends NPL procedure */
		npl_push,          /**< Push element on stack */
		npl_pop,           /**< Pop element from the stack */
		npl_dup,           /**< Duplicate element on top of stack */
		npl_add,           /**< Add two elements on top of stack */
		npl_sub,           /**< Substract two elements on top of stack */
		npl_mul,           /**< Multiply two elements on top of stack */
		npl_div,           /**< Divide two elements on top of stack */
	/**  Rise the element on top of stack to the power of next element */
		npl_pow,
		npl_exp,           /**< Exp function */
		npl_ln,            /**< Ln function */
		npl_varx           /**< Push the X variable on stack */
	};

	/**
	 *   NPL error enum.
	 */
	enum npl_err {
		err_domain = 1,    /**< Domain error - operation not permited */
		err_syntax         /**< Syntax error */
	};

	/**
	 *   Fitting types.
	 */
	enum fit_type {
		fit_linear,        /**< Linear */
		fit_quad,          /**< Quadratic */
		fit_cubic,         /**< Cubic */
		fit_quart,         /**< Quart */
		fit_poly,          /**< Polynomial */
		fit_exp,           /**< Exponential */
		fit_log,           /**< Logarithmic */
		fit_power,         /**< Power */
		fit_gauss          /**< Gaussian */
	};

	/**
	 *   Fitting errors.
	 */
	enum fit_err {
		ferr_ok = 0,       /**< No error */
		ferr_data = -1,    /**< Data set to small */
		ferr_domain = -2   /**< Domain error */
	};

	/** Point shape enum */
	enum point_shape {
		shp_unknown = -1,  /**< Unknown symbol */
		shp_x,             /**< x */
		shp_plus,          /**< + */
		shp_circle,        /**< Circle */
		shp_fcircle,       /**< Filled circle */
		shp_square,        /**< Square */
		shp_fsquare,       /**< Filled square */
		shp_diamond,       /**< Diamond */
		shp_fdiamond       /**< Filled diamond */
	};

	/**  Line dash enum */
	enum line_dash {
		ld_solid = 1,         /**< Solid */
		ld_dash = 2,          /**< Dashed */
		ld_dot = 3,           /**< Dotted */
		ld_dash_dot = 4,      /**< Dash-dot */
		ld_dash_dot_dot = 5   /**< Dash-dot-dot */
	};

};

#endif /* NPLOTNAMESPACE_H */

