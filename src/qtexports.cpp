/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "qtexports.h"
#include "options.h"
#include "nplotnamespace.h"
#include <sstream>
#include <cmath>

using namespace std;
using namespace nplot;

QtExport::QtExport(QPainter *_canvas): canvas(_canvas)
{
}

void QtExport::begin(QWidget *_widget)
{
	widget = _widget;
	canvas->begin(widget);
	canvas->setPen(Qt::black);
}

void QtExport::end()
{
	canvas->end();
	widget = 0;
}

void QtExport::drawPoint(double x, double y, point_shape type, int size)
{
	if (!widget)
		return;

	QPainterPath *path;
	switch (type) {
	case shp_x:
		drawLine(x - size, y - size, x + size, y + size);
		drawLine(x - size, y + size, x + size, y - size);
		break;
	case shp_plus:
		drawLine(x - size, y, x + size, y);
		drawLine(x, y - size, x, y + size);
		break;
	case shp_circle: 
		path = new QPainterPath;
		path->addEllipse(x - size, y - size, 2 * size, 2 * size);
		canvas->setRenderHint(QPainter::Antialiasing, true);
		canvas->strokePath(*path, canvas->pen());
		delete path;
		break;
	case shp_fcircle:
		path = new QPainterPath;
		path->addEllipse(x - size, y - size, 2 * size, 2 * size);
		canvas->setRenderHint(QPainter::Antialiasing, true);
		canvas->fillPath(*path, canvas->brush());
		delete path;
		break;
	case shp_square:
		path = new QPainterPath;
		path->moveTo(x - size, y - size);
		path->lineTo(x + size, y - size);
		path->lineTo(x + size, y + size);
		path->lineTo(x - size, y + size);
		path->closeSubpath();
		canvas->strokePath(*path, canvas->pen());
		delete path;
		break;
	case shp_fsquare:
		path = new QPainterPath;
		path->moveTo(x - size, y - size);
		path->lineTo(x + size, y - size);
		path->lineTo(x + size, y + size);
		path->lineTo(x - size, y + size);
		path->closeSubpath();
		canvas->fillPath(*path, canvas->brush());
		delete path;
		break;
	case shp_diamond:
		path = new QPainterPath;
		path->moveTo(x, y + size);
		path->lineTo(x + size, y);
		path->lineTo(x, y - size);
		path->lineTo(x - size, y);
		path->closeSubpath();
		canvas->strokePath(*path, canvas->pen());
		delete path;
		break;
	case shp_fdiamond:
		path = new QPainterPath;
		path->moveTo(x, y + size);
		path->lineTo(x + size, y);
		path->lineTo(x, y - size);
		path->lineTo(x - size, y);
		path->closeSubpath();
		canvas->fillPath(*path, canvas->brush());
		delete path;
	default:
		break;
	}
}

void QtExport::drawLine(double x1, double y1,
                        double x2, double y2,
                        double thick, line_dash ldash)
{
	canvas->setRenderHint(QPainter::Antialiasing, true);
	QPainterPath path;
	path.moveTo(x1, y1);
	path.lineTo(x2, y2);
	QPen pen(canvas->brush(), thick, (Qt::PenStyle) ldash, Qt::SquareCap,
	         Qt::BevelJoin);
	canvas->strokePath(path, pen);
}

double QtExport::drawText(double x, double y, const char *text)
{
	if (!textMeasure) {
		canvas->setRenderHint(QPainter::Antialiasing, true);
		canvas->save();
		canvas->translate(x, y);
		canvas->drawText(0, 0, QString(text));
		canvas->restore();
	}
	QFontMetrics fm = QFontMetrics(currentFont);
	return fm.width(QString(text)) + 1;
}

void QtExport::setFont(font_face face, double size)
{
	QFont f;
	switch (face) {
	case times:
		f.setFamily("times new roman, utopia");
		break;
	case math:
		f.setFamily("cmmi10");
		break;
	case symbol:
		f.setFamily("cmsy10");
		break;
	case symbol2:
		f.setFamily("msbm10");
		break;
	}
	f.setFixedPitch(true);
	f.setPointSizeF(size);
	currentFont = f;
	if (canvas->isActive())
		canvas->setFont(f);
}

void QtExport::setColor(double _r, double _g, double _b)
{
	QColor c;
	c.setRgbF(_r, _g, _b);
	canvas->setPen(QPen(c));
	canvas->setBrush(QBrush(c));
}

double QtExport::stringWidth(const char *text)
{
	QFontMetrics fm = QFontMetrics(currentFont);
	return fm.width(QString(text));
}

double QtExport::getFontAscent()
{
	QFontMetrics fm = QFontMetrics(currentFont);
	return fm.ascent();
}

double QtExport::getFontDescent()
{
	QFontMetrics fm = QFontMetrics(currentFont);
	return fm.descent();
}

double QtExport::getFontHeight()
{
	QFontMetrics fm = QFontMetrics(currentFont);
	return fm.height();
}

void QtExport::translate(double x, double y)
{
	canvas->translate(x, y);
}

void QtExport::rotate(double angle)
{
	canvas->rotate(angle);
}

void QtExport::moveto(double x, double y)
{
	path = QPainterPath();
	path.moveTo(x, y);
}

void QtExport::lineto(double x, double y)
{
	path.lineTo(x, y);
}

void QtExport::stroke(double thick, line_dash ldash)
{
	int n = path.elementCount();
	QPainterPath path2;
	QPainterPath::Element el1;
	QPainterPath::Element el2;
	for (int i = 1; i < n; ++i) {			/* Throwing out extra elements */
		el1 = path.elementAt(i - 1);
		el2 = path.elementAt(i);
		if ((el1.x < 0 || el1.x > width() || el1.y < 0
		     || el1.y > height())
		    && (el2.x < 0 || el2.x > width() || el2.y < 0
		        || el2.y > height()))
		{
			continue;
		}
		if (path2.elementCount() == 0)
			path2.moveTo(el1.x, el1.y);
		path2.lineTo(el2.x, el2.y);
	}
	QPen pen(canvas->brush(), thick, (Qt::PenStyle) ldash, Qt::SquareCap,
	         Qt::MiterJoin);
	canvas->strokePath(path2, pen);
}

int QtExport::width()
{
	if (widget)
		return widget->width();
	else	
		return 0;
}

int QtExport::height()
{
	if (widget)
		return widget->height();
	else
		return 0;
}

void QtExport::setClipRect(double x1, double y1, double x2, double y2)
{
	QPainterPath p;
	p.moveTo(x1, y1);
	p.lineTo(x2, y1);
	p.lineTo(x2, y2);
	p.lineTo(x1, y2);
	p.closeSubpath();
	canvas->setClipPath(p);
}

NativeExport::NativeExport(QPainter *canvas, bool scaling): QtExport(canvas)
{
	this->scaling = scaling;
}

void NativeExport::begin(QWidget *_widget)
{
	widget = _widget;
	canvas->begin(widget);
	canvas->setRenderHint(QPainter::Antialiasing, true);
	canvas->setRenderHint(QPainter::TextAntialiasing, true);
	canvas->setRenderHint(QPainter::SmoothPixmapTransform, true);
	canvas->setPen(Qt::black);
	if (scaling) {
		canvas->scale((double) widget->width() / width(),
		              (double) widget->height() / height());
	}
}

int NativeExport::width() 
{
	if (scaling || widget==NULL)
		return IMG_WIDTH;
	else
		return widget->width();
}

int NativeExport::height()
{
	if (scaling || widget == NULL)
		return IMG_HEIGHT;
	else
		return widget->height();
}

NativeCairoExport::NativeCairoExport()
{
	w = 0;
	h = 0;
	painter = new QPainter;
	surface = cairo_image_surface_create(CAIRO_FORMAT_RGB24, w, h);
	canvas  = cairo_create(surface);
	
	cairo_set_line_cap(canvas, CAIRO_LINE_CAP_SQUARE);
	cairo_select_font_face(canvas, "Serif", CAIRO_FONT_SLANT_NORMAL,
	                       CAIRO_FONT_WEIGHT_NORMAL);
	
	cairo_set_source_rgb(canvas, 1 ,1 ,1);
	cairo_rectangle(canvas, 0, 0, w, h);
	cairo_fill(canvas);
	cairo_set_source_rgb(canvas, 0, 0, 0);
}

NativeCairoExport::~NativeCairoExport()
{
	if (canvas) {
		cairo_show_page(canvas);
		cairo_destroy(canvas);
	}
	if (surface)
		cairo_surface_destroy(surface);
	if (painter)
		delete painter;
}

void NativeCairoExport::begin(QWidget *widget)
{
	painter->begin(widget);
	w = widget->width();
	h = widget->height();

	if (canvas)
		cairo_destroy(canvas);
	if (surface)
		cairo_surface_destroy(surface);
	
	surface = cairo_image_surface_create(CAIRO_FORMAT_RGB24, w, h);
	canvas  = cairo_create(surface);
	
	cairo_set_line_cap(canvas, CAIRO_LINE_CAP_SQUARE);
	cairo_select_font_face(canvas, "Serif", CAIRO_FONT_SLANT_NORMAL,
	                       CAIRO_FONT_WEIGHT_NORMAL);
	
	cairo_set_source_rgb(canvas, 1,1,1);
	cairo_rectangle(canvas, 0, 0, w, h);
	cairo_fill(canvas);
	cairo_set_source_rgb(canvas, 0,0,0);
	cairo_scale(canvas, (double) w / IMG_WIDTH, (double) h / IMG_HEIGHT);
}

void NativeCairoExport::end()
{
	cairo_show_page(canvas);
	int width  = cairo_image_surface_get_width(surface);
	int height = cairo_image_surface_get_height(surface);
	QImage image = QImage(cairo_image_surface_get_data(surface), width,
	                      height, QImage::Format_RGB32);
	painter->drawImage(0, 0, image);
	painter->end();
}

void NativeCairoExport::redrawFromBuffer(QWidget *widget)
{
	painter->begin(widget);
	end();
}

int NativeCairoExport::width()  { return IMG_WIDTH; }

int NativeCairoExport::height() { return IMG_HEIGHT; }

