/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QTEXPORTS_H
#define QTEXPORTS_H

#include "nplotnamespace.h"
#include "cairoexport.h"
#include "export.h"
#include <QtGui>
#include <string>

using namespace std;
using namespace nplot;

/**
 *   Base class for Qt-related exports.
 */
class QtExport: public Export
{
public:
	/**
	 *   A constructor.
	 *   \param canvas QPainter canvas to draw on.
	 */
	QtExport(QPainter *canvas);

	/**
	 *   A destructor.
	 */
	virtual ~QtExport() {};

	/**
	 *   Begins a new drawing process on a given widget.
	 *   \param widget a Qt widget to draw on
	 */
	virtual void begin(QWidget *widget);

	/** Ends the draw process. */
	virtual void end();
	void drawPoint(double x, double y, point_shape type, int size);
	void drawLine(double x1, double y1, double x2, double y2,
	              double thick = 1, line_dash ldash = ld_solid);
	double drawText(double x, double y, const char* text);
	void setColor(double r, double g, double b);
	void translate(double x, double y);
	void rotate(double angle);
	void moveto(double x, double y);
	void lineto(double x, double y);
	void stroke(double thick, line_dash ldash);
	virtual int width();
	virtual int height();
	void setFont(font_face face, double size);
	double stringWidth(const char *text);
	double getFontAscent();
	double getFontDescent();
	double getFontHeight();
	void setClipRect(double x1, double y1, double x2, double y2);
protected:
	QPainter *canvas;            /**< QPainter drawing context*/
	QWidget  *widget;            /**< Widget we are painting on */
private:
	QPainterPath path;           /**< A path for use with moveto/lineto */
	QFont currentFont;           /**< Current font */
};

/**
 *   Export graphics to Qt widget.
 */
class NativeExport: public QtExport
{
public:
	/**
	 *   A constructor.
	 *   NOTE: if \e scaling is on, \e width and \e height are returning
	 *   \c IMG_WIDTH and \c IMG_HEIGHT respectively, so it makes sense
	 *   only when drawing a chart (but we're doing it via cairo now).
	 *   \param canvas QPainter canvas to draw on
	 *   \param scaling determines whether or not, content should 
	 *   be scaled to the widget size
	 */
	NativeExport(QPainter *canvas, bool scaling=false);

	/**  A destructor */
	~NativeExport() {};
	void begin(QWidget *widget);
	int width();
	int height();
private:
	bool scaling;       /**< Scaling on/off */
};

/**
 *   Export graphics to Qt widget via cairo
 */
class NativeCairoExport: public CairoExport
{
public:
	/**
	 *   A constructor.
	 */
	NativeCairoExport();

	/**  A destructor. */
	~NativeCairoExport();
	
	/**
	 *   Begins a new drawing process on a given widget.
	 *   \param widget a Qt widget to draw on
	 */
	void begin(QWidget *widget);
	
	/**  Ends the draw process */
	void end();
	
	/**
	 *   Redraws previously rendered image.
	 *   \param widget a Qt widget to draw on
	 */
	void redrawFromBuffer(QWidget *widget);
	int width();
	int height();
private:
	QPainter *painter;        /**< Qt drawing context */
	int w;                    /**< Width of the widget we're drawing on */
	int h;                    /**< Height of the widget we're drawing on */
	cairo_surface_t *surface; /**< Cairo image surface */
};

#endif /* QTEXPORTS_H */
