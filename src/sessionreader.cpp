/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sessionreader.h"
#include <sstream>
#include <iostream>
#include <string.h>

using namespace std;

/************************ S E S S I O N R E A D E R *************************/

int SessionReader::getPropertyInt(const char *name)
{
	const char *prop = getProperty(name);
	if (prop)
		return atoi(prop);
	return 0;
}

bool SessionReader::getPropertyBool(const char *name)
{
	return !strcmp(getProperty(name), "true");
}

double SessionReader::getPropertyDouble(const char *name)
{
	const char *prop = getProperty(name);
	if (prop)
		return atof(prop);
	return 0;
}

/************************* L I B X M L R E A D E R **************************/

LibXMLReader::LibXMLReader(const char *fname): SessionReader()
{
	xmlNodePtr root_element;
	doc = xmlReadFile(fname, NULL, 0);
	root_element = xmlDocGetRootElement(doc);
	nodes.push(root_element);
}

LibXMLReader::~LibXMLReader()
{
	/* Free the document */
	xmlFreeDoc(doc);

	/* Free the parsers' global variables */
	xmlCleanupParser();

}

const char *LibXMLReader::nextNode()
{
	if (!nodes.top())
		return NULL;

	xmlNodePtr node = nodes.top()->next;
	while (node && node->type != XML_ELEMENT_NODE)
		node = node->next;
	nodes.pop();
	if (!node)
		return NULL;
	nodes.push(node);
	return (const char*) node->name;
}

bool LibXMLReader::processChildren()
{
	if (!nodes.top())
		return false;

	xmlNodePtr child = nodes.top()->children;
	if (!child)
		return false;
	nodes.push(child);
	return true;
}

const char *LibXMLReader::getProperty(const char *name)
{
	return (const char*) xmlGetProp(nodes.top(), BAD_CAST name);
}

const char *LibXMLReader::getValue()
{
	const char *content;
	processChildren();
	content = (const char*) nodes.top()->content;
	nodes.pop();
	return content;
}
