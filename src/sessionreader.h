/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2007, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SESSIONREADER_H
#define SESSIONREADER_H

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <stack>
#include <string>

using namespace std;

/**
 *   Common interface for session readers.
 */
class SessionReader
{
public:
	/**  A constructor */
	SessionReader() {};

	/**  A destructor */
	virtual ~SessionReader() {};

	virtual const char *nextNode() = 0;
	virtual bool processChildren() = 0;
	virtual const char *getProperty(const char *name) = 0;
	int getPropertyInt(const char *name);
	bool getPropertyBool(const char *name);
	double getPropertyDouble(const char *name);

	virtual const char *getValue() = 0;
};

class LibXMLReader: public SessionReader
{
public:
	/**
	 *   A constructor
	 *   \param fname session file name
	 */
	LibXMLReader(const char *fname);

	/**  A destructor */
	~LibXMLReader();

	const char *nextNode();
	bool processChildren();
	const char *getProperty(const char *name);
	const char *getValue();
private:
	xmlDoc *doc;
	stack<xmlNodePtr> nodes;
};

#endif /* SESSIONREADER_H */
