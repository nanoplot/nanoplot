/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sessionwriter.h"
#include "options.h"
#include <sstream>

using namespace std;

/************************ S E S S I O N W R I T E R *************************/

void SessionWriter::setProperty(const char *name, int value)
{
	stringstream buf;
	buf << value;
	setProperty(name, buf.str().c_str());
}

void SessionWriter::setProperty(const char *name, bool value)
{
	setProperty(name, value? "true" : "false");
}

void SessionWriter::setProperty(const char *name, double value)
{
	stringstream buf;
	buf << value;
	setProperty(name, buf.str().c_str());
}

/************************* L I B X M L W R I T E R **************************/

LibXMLWriter::LibXMLWriter(const char *fname): SessionWriter()
{
	doc = xmlNewDoc(BAD_CAST "1.0");
	filename = fname;

	xmlNodePtr root_node = xmlNewNode(NULL, BAD_CAST "nanoplot");
	xmlNewProp(root_node, BAD_CAST "version", BAD_CAST VERSION);
	xmlDocSetRootElement(doc, root_node);
	nodes.push(root_node);
}

LibXMLWriter::~LibXMLWriter()
{
	/* Saving document to a file */
	xmlSaveFormatFileEnc(filename.c_str(), doc, "UTF-8", 1);

        /* Free the document */
	xmlFreeDoc(doc);

	/* Free the parsers' global variables */
	xmlCleanupParser();
}

void LibXMLWriter::startElement(const char *name)
{
	xmlNodePtr node = xmlNewNode(NULL, BAD_CAST name);
	nodes.push(node);
}

void LibXMLWriter::endElement()
{
	xmlNodePtr node = nodes.top();
	nodes.pop();
	if (node)
		xmlAddChild(nodes.top(), node);
}

void LibXMLWriter::setProperty(const char *name, const char *value)
{
	xmlNewProp(nodes.top(), BAD_CAST name, BAD_CAST value);
}

void LibXMLWriter::setValue(const char *text)
{
	xmlNodePtr node = xmlNewText(BAD_CAST text);
	xmlAddChild(nodes.top(), node);
}

