/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2007, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SESSIONWRITER_H
#define SESSIONWRITER_H

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <stack>
#include <string>

using namespace std;

/**
 *   Common interface for session writers.
 */
class SessionWriter
{
public:
	/**  A constructor */
	SessionWriter() {};

	/**  A destructor */
	virtual ~SessionWriter() {};

	/**  
	 *   Start a new element
	 *   \param name name of the node
	 */
	virtual void startElement(const char *name) = 0;

	/**
	 *   Close the element and pop it from stack.
	 */
	virtual void endElement() = 0;

	/**  
	 *   Set a property
	 *   \param name name of a property
	 *   \param value value to set
	 */
	virtual void setProperty(const char *name, const char *value) = 0;
	void setProperty(const char *name, int value);
	void setProperty(const char *name, bool value);
	void setProperty(const char *name, double value);

	/**
	 *   Set the value of the element
	 *   \param text text to be writen as a value of the element
	 */
	virtual void setValue(const char *text) = 0;
};

class LibXMLWriter: public SessionWriter
{
public:
	/**
	 *   A constructor
	 *   \param fname session file name
	 */
	LibXMLWriter(const char *fname);

	/**  A destructor */
	~LibXMLWriter();

	void startElement(const char *name);
	void endElement();
	void setProperty(const char *name, const char *value);
	void setValue(const char *text);
private:
	string            filename;
	xmlDocPtr         doc;
	stack<xmlNodePtr> nodes;
};

#endif /* SESSIONWRITER_H */
