/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2010, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "options.h"
#include <gettext.h>
#include "settingsdialog.h"
#include "napp.h"
#include "mainwindow.h"

#define _(String) (gettext(String))

SettingsDialog::SettingsDialog(QWidget *parent)
 : QDialog(parent)
{
	QVBoxLayout *mainLayout = new QVBoxLayout;
	QHBoxLayout *butLayout  = new QHBoxLayout;

	list  = new QListWidget;
	pages = new QStackedWidget;
	pages->addWidget(new QWidget);

	vector<string> plugins = nApp::settings->plugins;
	vector<plugin_info> infos = nApp::instance()->plugins();

	for (unsigned int i = 0; i < infos.size(); ++i) {
		QListWidgetItem *item = new QListWidgetItem(infos[i].name);
		pages->addWidget(new QLabel(infos[i].provides));
		bool exists = false;
		for (vector<string>::iterator it = plugins.begin();
		     it != plugins.end(); ++it)
		{
			if (*it != infos[i].name_generic)
				continue;
			exists = true;
			break;
		}
		if (exists)
			item->setData(Qt::CheckStateRole, Qt::Checked);
		else 
			item->setData(Qt::CheckStateRole, Qt::Unchecked);
		list->addItem(item);
	}

	connect(list, SIGNAL(itemChanged(QListWidgetItem*)),
	        this, SLOT(changed(QListWidgetItem*)));

	connect(list, SIGNAL(currentRowChanged(int)), this, 
	        SLOT(listRowChanged(int)));

	QPushButton *okButton = new QPushButton(_("&OK"));
	okButton->setDefault(true);
	connect(okButton, SIGNAL(clicked()), this, SLOT(accept()));

	QPushButton *cancelButton = new QPushButton(_("&Cancel"));
	connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));

	butLayout->addWidget(okButton);
	butLayout->addWidget(cancelButton);

	mainLayout->addWidget(list);
	mainLayout->addWidget(pages);
	mainLayout->addLayout(butLayout);
	setLayout(mainLayout);

	setWindowTitle(_("Plugins"));
	vchanged = false;
}

void SettingsDialog::listRowChanged(int row)
{
	if (row < 0)
		pages->setCurrentIndex(0);
	else
		pages->setCurrentIndex(++row);
}

void SettingsDialog::accept()
{
	if (vchanged) {
		QAbstractItemModel *model = list->model();
		int rows = model->rowCount();

		vector<string> plugins;
		vector<plugin_info> infos = nApp::instance()->plugins();

		for (int i = 0; i < rows; ++i) {
			QModelIndex index = model->index(i, 0);
			if (index.data(Qt::CheckStateRole) == Qt::Unchecked)
				continue;
			plugins.push_back(infos[i].name_generic);
		}
		nApp::settings->plugins = plugins;
		QMessageBox::information(this, _("Information"),
		                         _("You must restart nPlot for "
		                           "changes to take effect."));
	}
	QDialog::accept();
}

void SettingsDialog::changed(QListWidgetItem*)
{
	vchanged = true;
}

