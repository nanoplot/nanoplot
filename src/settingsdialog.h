/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QtGui>

class SettingsDialog: public QDialog
{
Q_OBJECT
public:
	SettingsDialog(QWidget *parent = 0);
	~SettingsDialog() {};
public slots:
	void accept();
	void changed(QListWidgetItem*); 
	void listRowChanged(int);
private:
	QListWidget *list;
	QStackedWidget *pages;
	bool vchanged;
};

#endif /* SETTINGSDIALOG_H */
