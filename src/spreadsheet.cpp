/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2010, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "options.h"
#include <fstream>
#include <gettext.h>
#include <glib.h>
#include <iostream>
#include <sstream>
#include <vector>
#include "spreadsheet.h"
#include "config.h"
#include "napp.h"
#include "nplotnamespace.h"
#include "images/spread.xpm"

#define _(String) (gettext(String))

using namespace std;

Sheet::Sheet(vector< vector<double> > cont, QWidget *parent)
 : QTableWidget(parent)
{
	array.setCols(cont.size());
	array.setRows(cont[0].size());

	setColumnCount(array.cols());
	setRowCount(array.rows());
	
	for (unsigned int i = 0; i < cont.size(); ++i) {
		array.setColumnType(i, type_double);
		for (unsigned int j = 0; j < cont[0].size(); ++j)
			array[i][j] = cont[i][j];
	}

	Init();
}

Sheet::Sheet(Array cont, QWidget *parent)
 : QTableWidget(parent)
{
	array = cont;

	setColumnCount(array.cols());
	setRowCount(array.rows());
	
	Init();
}

Sheet::Sheet(int cols, int rows, QWidget *parent): QTableWidget(parent)
{
	setColumnCount(cols);
	setRowCount(rows);

	array.setCols(cols);
	array.setRows(rows);

	Init();
}

Sheet::Sheet(SessionReader *reader, QWidget *parent): QTableWidget(parent)
{
	array.load(reader);

	setColumnCount(array.cols());
	setRowCount(array.rows());

	Init();
}

void Sheet::Init()
{
	int rows = rowCount();
	int cols = columnCount();

	array.setCols(cols);
	array.setRows(rows);

	/*
	 *   Vertical header context menu setup
	 */
	vheaderPos = 0;

	QHeaderView *vHeader = verticalHeader();
	connect(vHeader, SIGNAL(customContextMenuRequested(QPoint)), this, 
	        SLOT(vheaderContextReq(QPoint)));
	
	vheaderContextMenu = new QMenu(this);

	QAction *goToRow = new QAction(_("Go to row"), vheaderContextMenu);
	connect(goToRow, SIGNAL(triggered()), this, SLOT(goToRow()));
	QAction *setRows = new QAction(_("Rows..."), this);
	connect(setRows, SIGNAL(triggered()), this, SLOT(setRows()));
	QAction *remRows = new QAction(_("Remove rows..."), this);
	connect(remRows, SIGNAL(triggered()), this, SLOT(remRows()));
	QAction *insRows = new QAction(_("Insert rows..."), this);
	connect(insRows, SIGNAL(triggered()), this, SLOT(insRows()));
	QAction *hc_separator1 = new QAction(this);
	hc_separator1->setSeparator(true);
	QAction *setBegin = new QAction(_("Set as begin"),
	                                vheaderContextMenu);
	connect(setBegin, SIGNAL(triggered()), this, SLOT(setBegin()));
	QAction *setEnd = new QAction(_("Set as end"), vheaderContextMenu);
	connect(setEnd, SIGNAL(triggered()), this, SLOT(setEnd()));
	vheaderContextMenu->addAction(goToRow);
	vheaderContextMenu->addAction(setRows);
	vheaderContextMenu->addAction(remRows);
	vheaderContextMenu->addAction(insRows);
	vheaderContextMenu->addAction(hc_separator1);
	vheaderContextMenu->addAction(setBegin);
	vheaderContextMenu->addAction(setEnd);
	
	vHeader->setContextMenuPolicy(Qt::CustomContextMenu);

	/*
	 *   Horizontal header context menu setup
	 */
	hheaderPos = 0;

	QHeaderView *hHeader = horizontalHeader();
	connect(hHeader, SIGNAL(customContextMenuRequested(QPoint)), this, 
	        SLOT(hheaderContextReq(QPoint)));
	
	hheaderContextMenu = new QMenu(this);
	
	QMenu *typeMenu = hheaderContextMenu->addMenu(_("Type"));

	vector<datatype_info> types = nApp::instance()->types(); 
	for (unsigned int i = 0; i < types.size(); ++i) {
		datatype_info info = types[i];
		QAction *act = new QAction(info.name, this);
		act->setData(QVariant(i));
		typeMenu->addAction(act);
		connect(act, SIGNAL(triggered()), this, SLOT(setType()));
	}

	QAction *sortAsc = new QAction(_("Sort ascending"), this);
	connect(sortAsc, SIGNAL(triggered()), this, SLOT(sortAscending()));
	QAction *sortDesc = new QAction(_("Sort descending"), this);
	connect(sortDesc, SIGNAL(triggered()), this, SLOT(sortDescending()));
	hheaderContextMenu->addAction(sortAsc);
	hheaderContextMenu->addAction(sortDesc);

	hHeader->setContextMenuPolicy(Qt::CustomContextMenu);

	for (int i = 0; i < cols; ++i) {
		setHorizontalHeaderItem(i, new QTableWidgetItem());
		for (int j = 0; j < rows; ++j) {
			string celltext = array.toString(i, j);
			setItem(j, i, new QTableWidgetItem(celltext.c_str()));
			item(j, i)->setTextAlignment(Qt::AlignRight
			                             | Qt::AlignVCenter);
		}
		setupColumn(i);
	}
	setFont(QFont("Times-Roman", 13));

	/* Context menu actions */

	QAction *cellCopy = new QAction(_("Copy"), this);
	cellCopy->setShortcut(Qt::CTRL|Qt::Key_C);
	connect(cellCopy, SIGNAL(triggered()), this, SLOT(copySelCells()));

	QAction *cellPaste = new QAction(_("Paste"), this);
	cellPaste->setShortcut(Qt::CTRL|Qt::Key_V);
	connect(cellPaste, SIGNAL(triggered()), this, SLOT(pasteCells()));

	/* TRANSLATORS: Clear (set to zero) selected cells */
	QAction *cellClear = new QAction(_("Clear"), this);
	cellClear->setShortcut(Qt::Key_Delete);
	connect(cellClear, SIGNAL(triggered()), this, SLOT(clearSelCells()));
	
	QAction *separator1 = new QAction(this);
	separator1->setSeparator(true);

	addAction(cellCopy);
	addAction(cellPaste);
	addAction(separator1);
	addAction(cellClear);
	setContextMenuPolicy(Qt::ActionsContextMenu);

	connect(this, SIGNAL(itemChanged(QTableWidgetItem*)), this, 
	        SLOT(element(QTableWidgetItem*)));
}

void Sheet::setupColumn(int n)
{
	if (n > array.cols())
		return;

	int type = array.columnType(n);
	QString caption = QString((char) ('A' + n));
	if (type != type_unknown) {
		datatype_info info = nApp::instance()->types()[type];
		caption += " [";
		caption += info.name;
		caption += ']';
	}
	horizontalHeaderItem(n)->setText(caption);

	blockSignals(true);
	for (int i = 0; i < array.rows(); ++i)
		item(i, n)->setText(array.toString(n, i).c_str());
	blockSignals(false);
}

void Sheet::save(SessionWriter *writer) const
{
	array.save(writer);
}

int Sheet::realWidth() const
{
	int w = verticalHeader()->width();
	for (int i = 0; i < columnCount(); ++i)
		w += columnWidth(i);
	return w;
}

void Sheet::setRows()
{
	bool ok;
	int r = QInputDialog::getInteger(this, _("Information"), _("Rows:"),
	                                 rowCount(), 2, MAX_ROWS, 1, &ok);
	if (!ok || r == rowCount())
		return;

	array.setRows(r);

	QTableWidgetItem *cell;
	int nrows = rowCount();
	int i, j;
	
	setRowCount(r);
	if (r < nrows)
		return;
	for (j = nrows; j < r; ++j) {
		for (i = 0; i < columnCount(); ++i) {
			cell = new QTableWidgetItem;
			setItem(j, i, cell);
			item(j, i)->setTextAlignment(Qt::AlignRight
			                             | Qt::AlignVCenter);
		}
	}
}

void Sheet::setCols()
{
	bool ok;
	int c = QInputDialog::getInteger(this, _("Information"),
	                                 _("Columns:"), columnCount(), 1, 26,
	                                 1, &ok);
	if (!ok || c == columnCount())
		return;
	
	array.setCols(c);

	QTableWidgetItem *headeritem;
	QTableWidgetItem *cell;
	int ncols = columnCount();
	int i, j;

	setColumnCount(c);
	if (c < ncols)
		return;
	
	for (j = ncols; j < c; ++j) {
		headeritem = new QTableWidgetItem();
		setHorizontalHeaderItem(j, headeritem);
		for (i = 0; i < rowCount(); ++i) {
			cell = new QTableWidgetItem;
			setItem(i, j, cell);
			item(i, j)->setTextAlignment(Qt::AlignRight
			                             | Qt::AlignVCenter);
		}
		setupColumn(j);
	}
}

void Sheet::vheaderContextReq(QPoint pos)
{
	QHeaderView *vHeader = verticalHeader();
	vheaderPos = vHeader->logicalIndexAt(pos);

	vheaderContextMenu->popup(vHeader->mapToGlobal(pos));
}

void Sheet::hheaderContextReq(QPoint pos)
{
	QHeaderView *hHeader = horizontalHeader();
	hheaderPos = hHeader->logicalIndexAt(pos);

	hheaderContextMenu->popup(hHeader->mapToGlobal(pos));
}

void Sheet::goToRow()
{
	bool ok;
	int r = QInputDialog::getInteger(this, _("Information"), _("Row:"), 
	                                 vheaderPos + 1, 1, rowCount(), 1,
	                                 &ok);
	if (!ok)
		return;

	scrollToItem(item(r - 1, 0), QAbstractItemView::PositionAtTop);
}

void Sheet::remRows()
{
	int nrows = rowCount();

	QDialog *remRowsDialog = new QDialog;
	remRowsDialog->setWindowTitle(_("Remove rows"));

	QVBoxLayout *mainLayout = new QVBoxLayout;
	QGridLayout *gridLayout = new QGridLayout;
	QHBoxLayout *butLayout = new QHBoxLayout;

	QLabel *fromLabel = new QLabel(_("From:"));
	QSpinBox *fromSpin = new QSpinBox;
	fromSpin->setValue(vheaderPos+1);
	fromSpin->setMinimum(1);
	fromSpin->setMaximum(nrows);
	
	QLabel *toLabel = new QLabel(_("To:"));
	QSpinBox *toSpin = new QSpinBox;
	toSpin->setValue(vheaderPos+1);
	toSpin->setMinimum(1);
	toSpin->setMaximum(nrows);

	QPushButton *okButton = new QPushButton(_("OK"));
	connect(okButton, SIGNAL(clicked()), remRowsDialog, SLOT(accept()));
	QPushButton *cancelButton = new QPushButton(_("Cancel"));
	connect(cancelButton, SIGNAL(clicked()), remRowsDialog, SLOT(reject()));
	
	gridLayout->addWidget(fromLabel, 0, 0);
	gridLayout->addWidget(fromSpin, 0, 1);
	gridLayout->addWidget(toLabel, 1, 0);
	gridLayout->addWidget(toSpin, 1, 1);

	butLayout->addWidget(okButton);
	butLayout->addWidget(cancelButton);

	mainLayout->addLayout(gridLayout);
	mainLayout->addLayout(butLayout);
	remRowsDialog->setLayout(mainLayout);

	if (!remRowsDialog->exec())
		return;

	int from = fromSpin->value();
	int to;
	
	if (toSpin->value() < from) {
		to = from;                   /* Pick the greater one as "to" */
		from = toSpin->value();
	} else {
		to = toSpin->value();
	}

	if (from == 1 && to == nrows)
		from++;

	from--;
	to--;

	for (int i = to; i >= from; --i)
		removeRow(i);

	for (int i = 0; i < array.cols(); ++i) {
		for (int j = 0; j < nrows - to; ++j)
			array[i][from + j] = array[i][to + j];
	}
	array.setRows(nrows - (to - from + 1));
}

void Sheet::insRows()
{
	bool ok;
	int r = QInputDialog::getInteger(this, _("Information"), 
	                           /* TRANSLATORS: Number of rows to insert */
	                                 _("Row count to insert:"), 1, 1,
	                                 MAX_ROWS - rowCount(), 1, &ok);
	if (!ok)
		return;

	array.setRows(array.rows() + r);
	for (int i = 0; i < array.cols(); ++i) {
		for (int j = 0; j < r; ++j)
			array[i][vheaderPos + 1 + j] =
				array[i][vheaderPos + r + j];
	}

	int ncols = columnCount();
	QTableWidgetItem *cell;
	for (int i = vheaderPos + 1; i <= vheaderPos + r; ++i) {
		insertRow(i);
		for (int j = 0; j < ncols; ++j) {
			cell = new QTableWidgetItem;
			setItem(i, j, cell);
			item(i, j)->setTextAlignment(Qt::AlignRight
			                             | Qt::AlignVCenter);
		}
	}
}

void Sheet::setBegin()
{
	for (int i = vheaderPos - 1; i >= 0; --i)
		removeRow(i);

	for (int i = 0; i < array.cols(); ++i) {
		for (int j = 0; j < array.rows() - vheaderPos; ++j)
			array[i][j] = array[i][vheaderPos + j];
	}
	array.setRows(array.rows() - vheaderPos);
}

void Sheet::setEnd()
{
	int nrows = rowCount();
	for (int i = nrows - 1; i > vheaderPos; --i)
		removeRow(i);

	array.setRows(vheaderPos + 1);
}

void Sheet::copySelCells()
{
	QString tmp;
	/* True means that we started appending a new row, so at the end we
	 * will have to remove unneeded tab and add a LF. */
	bool ap = false;

	int nrows = rowCount();
	int ncols = columnCount();

	qApp->clipboard()->ownsClipboard();
	for (int i = 0; i < nrows; ++i) {       /* Searching for selected cells */
		ap = false;
		for (int j = 0; j < ncols; ++j) {
			if (isItemSelected(item(i, j))) {
				tmp.append(item(i, j)->text());
				tmp.append("\t");
				ap = true;
			}
		}
		if (ap) {
			tmp.chop(1);                         /* Removing tab */
			tmp.append("\n");                    /* Appending LF */
		}
	}
	qApp->clipboard()->setText((const QString&) tmp); 
}

void Sheet::pasteCells()
{
	char c;
	char *rest;
	QString tmp;
	QString celltext;
	double d;
	string buf;
	int i,j = 0;
	int nrows = rowCount();
	int ncols = columnCount();
	bool celldone;
	
	if (qApp->clipboard()->text().isEmpty())
		return;
	
	tmp.append(qApp->clipboard()->text());
	QString::iterator it = tmp.begin();

	for (i = 0; i < nrows; ++i) {
		for (j = 0; j < ncols; ++j) {
			if (!isItemSelected(item(i, j)))
				continue;
			celldone = false;
			while (it < tmp.end() && !celldone) {
				c = (char) it->toAscii();
				it++;
				if (c == ',') c = '.';
				if ((c >= '0' && c <= '9')
				    || c == '+' || c == '-' || c == '.'
				    || c == 'e' || c == 'E') 
				{
					buf += c;
				} else if (buf != "") {
					d = strtod(buf.c_str(), &rest);
					if (strlen(rest) != buf.size()) {
						celltext = QString("%1").arg(d);
						item(i, j)->setText(celltext);
						celldone = true;
					}
					buf = "";
				}
			}
			/* Last element from buf */
			if (it == tmp.end() && buf != "" && !celldone) {
				d = strtod(buf.c_str(), &rest);
				if (strlen(rest) != buf.size()) {
					celltext = QString("%1").arg(d);
					item(i, j)->setText(celltext);
					celldone = true;
				}
				buf = "";
			}
		}
	}
}

void Sheet::clearSelCells()
{
	foreach(QTableWidgetItem *item, selectedItems())
		item->setText("");
}

void Sheet::sortAscending()
{
	array.sortAscending(hheaderPos);

	for (int i = 0; i < array.cols(); ++i) {
		for (int j = 0; j < array.rows(); ++j)
			item(j, i)->setText(array.toString(i, j).c_str());
	}
}

void Sheet::sortDescending()
{
	array.sortDescending(hheaderPos);

	for (int i = 0; i < array.cols(); ++i) {
		for (int j = 0; j < array.rows(); ++j)
			item(j, i)->setText(array.toString(i, j).c_str());
	}
}

void Sheet::setType()
{
	int type = (int) ((QAction*) sender())->data().toInt();
	array.setColumnType(hheaderPos, type);
	setupColumn(hheaderPos);
}

void Sheet::element(QTableWidgetItem *item)
{
	if (!item)
		return;

	int r = row(item);
	int c = column(item);

	/* Setting LC_NUMERIC to "C"; we want '.' as a decimal separator */
	setlocale(LC_NUMERIC, "C");
	if (r < rowCount() && c < columnCount()) {
		array.fromString(c, r, (const char*) item->text().toAscii());
		blockSignals(true);
		item->setText(array.toString(c, r).c_str());
		blockSignals(false);
	}
}
		
void Sheet::makeChart(int charttype)
{
	int ncols = columnCount();

	int x, y, dx = 0, dy = 0, type;
	XY *xy = NULL;
	if (charttype == elem_xy) {
		if (!Dialogs::makeChartXY(ncols, &xy, &x, &y, &type))
			return;
	} else if (charttype == elem_xydx) {
		if (!Dialogs::makeChartXYD(ncols, charttype, &xy, &x, &y, &dx,
		    &type)) 
			return;
	} else if (charttype == elem_xydy) {
		if (!Dialogs::makeChartXYD(ncols, charttype, &xy, &x, &y, &dy,
		    &type)) 
			return;
	} else if (charttype == elem_xydxdy) {
		if (!Dialogs::makeChartXYD2(ncols, &xy, &x, &y, &dx, &dy,
		    &type))
			return;
	}

	bool symb, line;
	switch (type) {
	case 0:
		symb = true;
		line = false;
		break;
	case 1:
		symb = false; 
		line = true;
		break;
	default:
		symb = true;
		line = true;
	}

	int n = 2;
	int c[4];
	c[0] = x - 1;
	c[1] = y - 1;

	if ((charttype & elem_xydx) != elem_xy)
		c[n++] = dx;
	if ((charttype & elem_xydy) != elem_xy)
		c[n++] = dy;

	Array _data = array.columns(n, c);
	if (!_data.good()) {
		Dialogs::error(_("Error: Selected columns are empty!"));
		return;
	}

	if (xy == NULL) {
		Chart *chart = new Chart(MainWindow::pointer);
		chart->addDataXY(_data, charttype, symb, line);
		MainWindow::pointer->workspace->addWindow(chart);
		chart->show();
	} else {
		int n = xy->numElements() + 1;
		gchar *dataname = g_strdup_printf(_("data%d"), n);
		xy->addDataXY(dataname, _data, charttype, symb, line);
		g_free(dataname);
		QWidgetList l = MainWindow::pointer->workspace->windowList();
		for (QList<QWidget*>::iterator it = l.begin();
		     it != l.end(); ++it)
		{
			if (((WSWindow*) *it)->type() != nplot::wdw_chart)
				continue;
			Chart *wdw = (Chart*) *it;
			if (wdw->chart() == xy)
				wdw->chartWidget()->update();
		}
	}
}

void Sheet::exportAscii(QString filename) const
{
	array.exportAscii(filename.toAscii());
}

void Sheet::exportLatexTabular(QString filename) const
{
	array.exportLatexTabular(filename.toAscii());
}

vector<double> Sheet::getDataColumn(int col) const
{
	return array.columnDouble(col);
}

Array Sheet::getData() const
{
	return array;
}

/************** S P R E A D S H E E T ****************/

SpreadSheet::SpreadSheet(vector< vector<double> > cont, QWidget *parent)
 : WSWindow(parent)
{
	Init();
	setWindowTitle(_("Spreadsheet") + QString(" %1").arg(counter++));
	
	sheet = new Sheet(cont, this);
	resize(sheet->realWidth() + 20, 300);
	setCentralWidget(sheet);
}

SpreadSheet::SpreadSheet(SessionReader *reader, QWidget *parent)
 : WSWindow(parent)
{
	Init();

	setWindowTitle(reader->getProperty("title"));
	hidden = reader->getPropertyBool("hidden");
	reader->processChildren();
	
	for (const char *name = reader->nextNode(); name;
	     name = reader->nextNode()) {
		if (strcmp(name, "Array") == 0) {
			sheet = new Sheet(reader, this);
			resize(sheet->realWidth() + 20, 300);
			setCentralWidget(sheet);
		}
	}
	
}

void SpreadSheet::Init()
{
	setMinimumWidth(250);
	setMinimumHeight(300);
	setWindowIcon(QIcon(spread_xpm));
}

int SpreadSheet::type() const
{
	return nplot::wdw_spreadsheet;
}

void SpreadSheet::save(SessionWriter *writer) const
{
	writer->startElement("SpreadSheet");
	writer->setProperty("title", (const char*) windowTitle().toAscii());
	writer->setProperty("hidden", isHidden());
	sheet->save(writer);
	writer->endElement();
}

Sheet* SpreadSheet::table() const
{
	return sheet;
}

int SpreadSheet::counter = 1;

void SpreadSheet::exportASCII()
{
	QString fr = QFileDialog::getSaveFileName(this, _("Export ASCII"), 
	                              Dialogs::currentDir,
	                              _("txt files (*.txt);;all files (*)"));
	if (fr.isEmpty())
		return;
	sheet->exportAscii(fr);
	Dialogs::setCurrentDir(fr);
}

void SpreadSheet::exportLatexTab()
{
	QString fr = QFileDialog::getSaveFileName(this, 
	                            _("Export to LaTeX tabular environment"),
	                            Dialogs::currentDir, _("TeX (*.tex)"));
	if (fr.isEmpty())
		return;
	sheet->exportLatexTabular(fr);
	Dialogs::setCurrentDir(fr);
}

