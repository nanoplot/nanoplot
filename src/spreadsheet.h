/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPREADSHEET_H
#define SPREADSHEET_H

#include <QtGui>
#include <vector>
#include <sstream>
#include <string>
#include "array.h"
#include "chart.h"
#include "dialogs.h"
#include "mainwindow.h"
#include "sessionreader.h"
#include "wswindow.h"

#define MAX_ROWS INT_MAX

class Chart;
class MainWindow;
class SpreadSheet;

using namespace std;

/**
 *   Class providing table widget functionality.
 */
class Sheet: public QTableWidget
{
Q_OBJECT
public:
	/**
	 *   A constructor.
	 *   Creates a table with content determined by given parameters
	 *   \param cont Std::vector columns (also vectors) of table
	 *   \param parent Pointer to parent widget
	 */
	Sheet(vector< vector<double> > cont, QWidget *parent = 0);

	Sheet(Array cont, QWidget *parent = 0);
	/**
	 *   Alternative constructor.
	 *   Creates an empty table (filled with zeros)
	 *   \param cols Number of columns
	 *   \param rows Number of rows
	 *   \param parent Pointer to parent widget
	 */
	Sheet(int cols, int rows, QWidget *parent = 0);

	/**
	 *   A constructor creating table using information from a session
	 *   file.
	 *   \param cols Number of columns
	 *   \param rows Number of rows
	 *   \param reader session reader
	 *   \param parent Pointer to parent widget
	 */
	Sheet(SessionReader *reader, QWidget *parent = 0);

	/**  A destructor. */
	~Sheet() {};

	/**
	 *   Save table.
	 *   Method used for saving information to the session file.
	 *   \param writer session writer
	 */
	void save(SessionWriter *writer) const;
public slots:
	int realWidth() const; /**< Returns sum of widths of all columns */

	/**  Invokes a dialog for setting number of rows */
	void setRows(); 

	/**  Invokes a dialog for setting number of columns */
	void setCols();
	void goToRow();        /**< Invokes a dialog for jumping to given row */
	void remRows();        /**< Invokes a dialog for removing rows */
	void insRows();        /**< Invokes a dialog for inserting rows */
	void setBegin();       /**< Invokes a dialog for setting begining row */
	void setEnd();         /**< Invokes a dialog for setting ending row */
	void copySelCells();   /**< Copys selected cells to clipboard */
	void pasteCells();     /**< Pastes selected cells from clipboard */
	void clearSelCells();  /**< Clears selected cells */
	void sortAscending();  /**< Sorts ascending relative to given row */
	void sortDescending(); /**< Sorts descending relative to given row */
	void setType();

	/**
	 *   Invokes appropriate dialog and creates a chart.
	 *   \param type Type of chart (nplot::elem_type)
	 */
	void makeChart(int type);

	/**
	 *   Exports data from the table to ascii.
	 *   \param filename Filename
	 */
	void exportAscii(QString filename) const;

	/**
	 *   Exports data from the table to LaTeX tabular environment. 
	 *   \param filename Filename
	 */
	void exportLatexTabular(QString filename) const;

	/**
	 *   Returns a data column.
	 *   \param col Column number for extraction
	 *   \return Std::vector containing data from desired column
	 */
	vector<double> getDataColumn(int col) const;

	/**
	 *   Gives all data from the table.
	 *   \return Std::vector of columns (also vector)
	 */
	Array getData() const;

	/**
	 *   Pops up a vertical header context menu.
	 *   \param pos Mouse pointer position to determine a row
	 */
	void vheaderContextReq(QPoint pos);

	/**
	 *   Pops up a horizontal header context menu.
	 *   \param pos Mouse pointer position to determine a column 
	 */
	void hheaderContextReq(QPoint pos);
protected slots:
	/**
	 *   Sets up a table element.
	 *   Reads content of the given cell, converts it to double and stores
	 *   it in corresponding place in memory.
	 *   \param item Table item
	 */
	void element(QTableWidgetItem *item);
private:
	void Init();                    /**< Initializes widget */

	void setupColumn(int n);

	Array array;

	/** Holds pointer to vertical header context menu */
	QMenu *vheaderContextMenu;

	/** Holds pointer to horizontal header context menu */
	QMenu *hheaderContextMenu;

	/** Holds vertical header position (row number) */
	int vheaderPos;

	/** Holds horizontal header position (clumn number) */
	int hheaderPos;
};

/**
 *   Spreadsheet window.
 */
class SpreadSheet: public WSWindow 
{
Q_OBJECT
public:
	/**
	 *   A constructor.
	 *   Creates a window with the Sheet widget
	 *   \param cont Std::vector columns (also vectors) for the table
	 *   \param parent Pointer to parent widget
	 */
	SpreadSheet(vector< vector<double> > cont,  QWidget *parent = 0);

	/**
	 *   A constructor creating spreadsheet window using information from
	 *   a session file.
	 *   \param reader session reader
	 *   \param parent Pointer to parent widget
	 */
	SpreadSheet(SessionReader *reader, QWidget *parent = 0);

	/**  A destructor. */
	~SpreadSheet() {};
	int type() const;

	/**
	 *   Save spreadsheet.
	 *   Method used for saving information to a session file.
	 *   \param writer session writer
	 */
	void save(SessionWriter *writer) const;

	Sheet* table() const;       /**< Returns pointer to the Sheet widget */
	static int counter;         /**< Holds the counter of spreadsheets */
public slots:
	void exportASCII();   /**< Invokes a dialog for exporting Ascii */

	/** Invokes a dialog for exporting LaTeX tabular environment */
	void exportLatexTab();
private:
	void Init();                /**< Intializes needed variables */
	Sheet *sheet;               /**< Holds the pointer to Sheet widget */
};
#endif /* SPREADSHEET_H */

