/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tex.h"
#include "options.h"
#include "nplotnamespace.h"
#include <sstream>
#include <string.h>

using namespace std;
using namespace nplot;

TeX::TeX(Export *_exp)
 : exp(_exp)
{
}

double TeX::textInterpret(double x, double y, const char *text,
                             double size, int angle)
{
	double tx = x;
	double ty = y;
	if (angle != 0) {
		exp->translate(x, y);
		exp->rotate((double) angle);
		x = 0;
		y = 0;
	}
	
	double w = TeXInterpret(x, y, &text, size);
	
	if (angle != 0) {
		exp->rotate(-(double) angle);
		exp->translate(-tx, -ty);
	}

	return w;
}

/* 
 *   TODO: This should be changed to use Pango; now kerning is owful
 *   (strictly speaking, it doesn't even exist).
 *   Maybe use bison for TeX interpretation?
 */
double TeX::TeXInterpret(double x, double y, const char **text, double size)
{
	const char *ptr = *text;
	double  rx = 0;          /* Will contain width of the rendered text */
	double *dx = &rx;        /* Can point to rx, supx or subx */
	double  y0 = y;          /* Base line */
	double  size0 = size;    /* Base size */
	double  supy = y - 0.4 * exp->getFontAscent();
	double  suby = y + 0.5 * exp->getFontDescent();
	double  supx = 0;
	double  subx = 0;
	char buf[128];
	int i = 0;
	do {                        /* Tricky */
		switch (*ptr) {
		case '{':           /* Render group */
			*dx += TeXInterpret(x + *dx, y, &++ptr, size);
			break;
		case '}':           /* End of our group */
			if (supx > rx || subx > rx)
				rx = (supx > subx) ? supx : subx;
			*text = ptr;
			return rx;
		case '\\':          /* Symbol started */
			*dx += insertSymbol(x + *dx, y, &ptr, size);
			break;
		case '^':           /* Render superscript */
			if (supx < rx)
				supx = rx;
			dx = &supx;
			y = supy;
			size = 0.6 * size0;
			continue;
		case '_':           /* Render subscript */
			if (subx < rx)
				subx = rx;
			dx = &subx;
			y = suby;
			size = 0.6 * size0;
			continue;
		default: 
			buf[i++] = *ptr;
			if (*ptr > 0 || i == 128) {
				exp->setFont(Export::times, size);
				buf[i] = '\0';
				*dx += exp->drawText(x + *dx, y, buf);
				i = 0;
			}
		}
		if ((supx > rx || subx > rx) && ptr[1] != '^' && ptr[1] != '_')
		{
			rx = (supx > subx)? supx : subx;
			dx = &rx;
			supx = subx = 0;
			y = y0;
			size = size0;
		}
	} while (*ptr++ != 0);

	/* Clearing buffer */
	if (i > 0) {
		exp->setFont(Export::times, size);
		buf[i] = '\0';
		*dx += exp->drawText(x + *dx, y, buf);
	}

	/* Cleaning up sup- and subscripts */
	if (supx > rx || subx > rx)
		rx = (supx > subx)? supx : subx;
	*text = ptr;
	return rx;
}

double TeX::textWidth(const char *text, double size)
{
	exp->setTextMeasure(true);
	double result = textInterpret(0, 0, text, size, 0);
	exp->setTextMeasure(false);
	return result;
}

string TeX::sci2pwr(double d)
{
	string out;
	bool pwr = false;
	stringstream stream;
	stream << d;
	char c = stream.get();
	while (!stream.eof()) {
		if (c == 'E' || c == 'e') {
			out += "\\cdot10^{";
			pwr = true;
			c = stream.get();
			if (c == '-') {
				out += c;
				c = stream.get();
			}
			if (c == '+')
				c = stream.get();
			while (c == '0')
				c = stream.get();
		} else {
			out += c;
			c = stream.get();
		}
	}
	if (pwr)
		out += '}';
	
	return out;
}

double TeX::insertSymbol(double x, double y, const char **text, double size)
{
	const char *ptr = *text;
	const int buf_lenght = 64;
	char buf[buf_lenght];
	char c;
	int  i = 1;

	buf[0] = *ptr;
	while (i < buf_lenght - 1 && (((c = *++ptr) >= 'a' && c <= 'z')
	                              || (c >= 'A' && c <= 'Z')))
	{
	       buf[i++] = c;
	}
	if (i == 1 && (*ptr == '{' || *ptr == '}'))
		buf[i++] = *ptr++;
	buf[i] = 0;
	*text = --ptr;
	if (ptr[1] == ' ') {
		(*text)++;
		if (i == 1) {
			exp->setFont(Export::times, size);
			return 2 * exp->stringWidth(",");
		}
	}
	i = 0;
	while (symbol_array[i].symb != 0) {
		symbol_struct s = symbol_array[i++];
		if (strcmp(buf, s.symb) != 0)
			continue;
		exp->setFont((Export::font_face) s.font, size);
		return exp->drawText(x, y, s.code) + 1;
	}
	exp->setFont(Export::times, size);
	return exp->drawText(x, y, "?");
}

