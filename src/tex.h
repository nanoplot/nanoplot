/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TEX_H
#define TEX_H

#include "export.h"
#include "nplotnamespace.h"
#include "texsymbols.h"
#include <string>

using namespace std;
using namespace nplot;

class TeX
{
public:
	/**  Constructor */
	TeX(Export *exp);

	/**  Destructor */
	virtual ~TeX() {};

	/**
	 *   Preprocess the data for TeXInterpret.
	 *   Translate and rotate viewport.
	 *   \param x, y coordinates of text 
	 *   \param text text to be interpreted
	 *   \param size font size
	 *   \param angle rotation angle (deg)
	 */
	double textInterpret(double x, double y, const char *text, double size, 
	                     int angle = 0);
	
	/**
	 *   Interpret TeXed text.
	 *   Group-oriented recursive TeX interpreting (very basic).
	 *   \param x, y coordinates of text 
	 *   \param text iterator of interpreted text
	 *   \param size font size
	 */
	double TeXInterpret(double x, double y, const char **text, double size);
	
	/**
	 *   Determine the width of given text.
	 *   \param text text to be processed
	 *   \param size font size
	 *   \return width of given text
	 */
	double textWidth(const char *text, double size);

	/**
	 *   Make a TeXified string from numerical value.
	 *   \param d given value
	 *   \return TeXified string
	 */
	string sci2pwr(double d);

	/**
	 *   Inserts a TeX symbol.
	 *   \param x, y coordinates of the symbol  
	 *   \param text iterator of interpreted text
	 *   \param size font size
	 */
	double insertSymbol(double x, double y, const char **text, double size);

protected:
	Export *exp;
};

#endif /* TEX_H */
