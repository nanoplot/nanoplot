/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TEXSYMBOLS_H
#define TEXSYMBOLS_H

typedef const struct {
	const char *code;       /* UTF8 code */
	const char *symb;       /* TeX symbol */
	int font;               /* font set; 1-times, 2-cmmi10, 3-cmsy10,
	                         * 4-msbm10 */
} symbol_struct;

symbol_struct symbol_array[] = 
{
	{"\xc2\xae", "\\alpha", 2},
	{"\xc2\xaf", "\\beta", 2},
	{"\xc2\xb0", "\\gamma", 2},
	{"\xc2\xb1", "\\delta", 2},
	{"\xc2\xb2", "\\epsilon", 2},
	{"\x22", "\\varepsilon", 2},
	{"\xc2\xb3", "\\zeta", 2},
	{"\xc2\xb4", "\\eta", 2},
	{"\xc2\xb5", "\\theta", 2},
	{"\x23", "\\vartheta", 2},
	{"\xc2\xb6", "\\iota", 2},
	{"\xe2\x88\x99", "\\kappa", 2},
	{"\xc2\xb8", "\\lambda", 2},
	{"\xc2\xb9", "\\mu", 2},
	{"\xc2\xba", "\\nu", 2},
	{"\xc2\xbb", "\\xi", 2},
	{"o", "\\o", 2},
	{"\xc2\xbc", "\\pi", 2},
	{"\x24", "\\varpi", 2},
	{"\xc2\xbd", "\\rho", 2},
	{"\x25", "\\varrho", 2},
	{"\xc2\xbe", "\\sigma", 2},
	{"\x26", "\\varsigma", 2},
	{"\xc2\xbf", "\\tau", 2},
	{"\xc3\x80", "\\upsilon", 2},
	{"\xc3\x81", "\\phi", 2},
	{"\x27", "\\varphi", 2},
	{"\xc3\x82", "\\chi", 2},
	{"\xc3\x83", "\\psi", 2},
	{"\x21", "\\omega", 2},
	{"\xc2\xa1", "\\Gamma", 2},
	{"\xc2\xa2", "\\Delta", 2},
	{"\xc2\xa3", "\\Theta", 2},
	{"\xc2\xa4", "\\Lambda", 2},
	{"\xc2\xa5", "\\Xi", 2},
	{"\xc2\xa6", "\\Pi", 2},
	{"\xc2\xa7", "\\Sigma", 2},
	{"\xc2\xa8", "\\Upsilon", 2},
	{"\xc2\xa9", "\\Phi", 2},
	{"\xc2\xaa", "\\Psi", 2},
	{"\xc2\xad", "\\Omega", 2},
	{"\x7d", "\\hslash", 4},
	{"\x7e", "\\hbar", 4},
	{"\xc2\xa3", "\\times", 3},
	{"\xc2\xa2", "\\cdot", 3},
	{"\xc2\xb1", "\\circ", 3},
	{"\\", "\\backslash", 1},
	{"{", "\\{", 1},
	{"}", "\\}", 1},
	{"\x68", "\\langle", 3},
	{"\x69", "\\rangle", 3},
	{"\x40", "\\partial", 2},
	{0,0,0}	
};

#endif /* TEXSUMBOLS_H */
