/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2010, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "options.h"
#include <gettext.h>
#include "wswindow.h"
#include "mainwindow.h"
#include "nplotnamespace.h"

#define _(String) (gettext(String))

using namespace nplot;

WSWindow::WSWindow(QWidget *parent): QMainWindow(parent)
{
	setAttribute(Qt::WA_DeleteOnClose);
	MainWindow::windowlist.push_back(this);
}

int WSWindow::type() const
{
	return wdw_unknown;
}

void WSWindow::updateVisibility()
{
	if (!hidden)
		show();
	else
		hide();
}

void WSWindow::closeEvent(QCloseEvent *event)
{
	if (sender() == NULL) {
		MainWindow::windowlist.remove(this);
		event->accept();
		return;
	}
	QMenu *closeMenu = new QMenu(this);

	QAction *close  = new QAction(_("Close"), closeMenu);
	QAction *hide   = new QAction(_("Hide"), closeMenu);
	QAction *cancel = new QAction(_("Cancel"), closeMenu);
	
	closeMenu->addAction(close);
	closeMenu->addAction(hide);
	closeMenu->addAction(cancel);

	QAction *ret = closeMenu->exec(QCursor::pos());
	
	if (ret == close) {
		MainWindow::windowlist.remove(this);
		event->accept();
	} else {
		event->ignore();
		if (ret == hide)
			this->hide();
	}

	delete closeMenu;
}
