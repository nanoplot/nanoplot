/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WSWINDOW_H
#define WSWINDOW_H

#include <QtGui>
#include <sstream>
#include "sessionwriter.h"

using namespace std;

/**
 *   Workspace window class.
 */
class WSWindow: public QMainWindow
{
Q_OBJECT
public:
	/**
	 *   A constructor.
	 *   \param parent Pointer to parent widget
	 */
	WSWindow(QWidget *parent);

	/**  A destructor. */
	virtual ~WSWindow() {};
	virtual int type() const;        /**< Returns type of a given window */
	
	/**
	 *   Save window.
	 *   Method used to saving information to a session file.
	 *   \param writer session writer
	 */
	virtual void save(SessionWriter *writer) const = 0;

	/**
	 *   Update visibility.
	 */
	virtual void updateVisibility();
protected slots:
	/**< Called when window is about to be closed */
	void closeEvent(QCloseEvent*); 
protected:
	bool hidden;
};

#endif /* WSWINDOW_H */
