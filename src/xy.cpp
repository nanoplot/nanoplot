/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2010, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cmath>
#include "xy.h"
#include "analysis.h"
#include "config.h"
#include "funcxy.h"

using namespace nplot;

XY::XY(int _width, int _height, Export *exp)
{
	elements.clear();
	
	nativeExport = exp;
	vwidth  = _width;
	vheight = _height;
	scalefx = (double) vwidth / nativeExport->width();
	scalefy = (double) vheight / nativeExport->height();
	
	markerX  = 0;
	markerY  = 0;
	marking  = false;
	modified = true;
	
	MainWindow::xylist.push_back(this);

	legend = new Legend(this);
	xAxis  = new XAxis2D(this);
	yAxis  = new YAxis2D(this);

	legend->setHidden(false);

	calcMargins(exp);
}

XY::~XY()
{
	MainWindow::xylist.remove(this);
	delete legend;
	delete xAxis;
	delete yAxis;
	for (unsigned int i = 0; i < elements.size(); ++i)
		delete elements[i];
}

void XY::load(SessionReader *reader)
{
	reader->processChildren();
	
	for (const char *name = reader->nextNode(); name;
	     name = reader->nextNode()) {
		if (strcmp(name, "DataSeriesXY") == 0) {
			DataSeriesXY *element = new DataSeriesXY(reader, this);
			elements.push_back(element);
		} else if (strcmp(name, "FuncXY") == 0) {
			FuncXY *element = new FuncXY(reader, this);
			elements.push_back(element);
		} else if (strcmp(name, "Legend") == 0) {
			legend->load(reader);
		} else if (strcmp(name, "XAxis2D") == 0) {
			xAxis->load(reader);
		} else if (strcmp(name, "YAxis2D") == 0) {
			yAxis->load(reader);
		}
	}
	modified = true;
}

void XY::save(SessionWriter *writer)
{
	writer->startElement("XY");
	for (vector<ChartElement*>::iterator it = elements.begin();
	     it != elements.end(); ++it)
	{
		(*it)->save(writer);
	}
	legend->save(writer);
	xAxis->save(writer);
	yAxis->save(writer);
	writer->endElement();
}

void XY::addDataXY(string name, Array data, int type,
                   bool symbols, bool line)
{
	DataSeriesXY *element = new DataSeriesXY(name.c_str(), data, type,
	                                         this);
	element->setColor(nextColor());
	element->setSymbolsEnabled(symbols);
	element->setConnectedEnabled(line);
	elements.push_back(element);
	
	modified = true;
}

void XY::calcMargins(Export *exp)
{
	if (exp == 0)
		exp = nativeExport;
	btmmgn   = (int) xAxis->height(exp) + 5; 
	leftmgn  = (int) yAxis->width(exp) + 5;
	topmgn   = (int) (0.5 * exp->getFontAscent()) + 5;
	rightmgn = (int) xAxis->rightMargin(exp) + 5;
}

void XY::fitZoom()
{
	if (elements.size() == 0)
		return;
	
	double xmajorinc = 0.1;
	double ymajorinc = 0.1;
	double minx = 0;
	double maxx = 0;
	double miny = 0;
	double maxy = 0;
	
	for (vector<ChartElement*>::iterator it = elements.begin();
	     it != elements.end(); ++it)
	{
		if (((*it)->type() & nplot::elem_xy) == 0)
			continue;
		minx = ((DataSeriesXY*)*it)->x()[0];
		miny = ((DataSeriesXY*)*it)->y()[0];
		maxx = minx;
		maxy = miny;
		break;
	}

	for (vector<ChartElement*>::iterator it = elements.begin();
	     it != elements.end(); ++it)
	{
		if (((*it)->type() & nplot::elem_xy) == 0)
			continue;
		vector<double> x = ((DataSeriesXY*) *it)->x();
		vector<double> y = ((DataSeriesXY*) *it)->y();
		for (unsigned int i = 1; i < x.size(); ++i) {
			/* Setting size of visualisation window */
			if (minx > x[i])
				minx = x[i];
			if (miny > y[i])
				miny = y[i];
			if (maxx < x[i])
				maxx = x[i];
			if (maxy < y[i])
				maxy = y[i];
		}
	}

	if (minx == maxx)
		maxx += 1;
	if (miny == maxy)
		maxy += 1;
		
	double exp1, exp2;
	double w1, w2;
	exp1 = floor(log10(abs(maxx - minx)));
	exp2 = floor(log10(abs(maxy - miny)));
	w1 = pow(10, exp1);
	w2 = pow(10, exp2);
	xmajorinc = w1 * 0.5 * ceil(0.2 * (maxx - minx) / w1);
	ymajorinc = w2 * 0.5 * ceil(0.2 * (maxy - miny) / w2);

	minx = xmajorinc * floor(minx / xmajorinc);
	maxx = xmajorinc * ceil(maxx / xmajorinc);
		
	miny = ymajorinc * floor(miny / ymajorinc);
	maxy = ymajorinc * ceil(maxy / ymajorinc);
	
	xAxis->setMin(minx);
	xAxis->setMax(maxx);
	xAxis->setMajorInc(xmajorinc);
	yAxis->setMin(miny);
	yAxis->setMax(maxy);
	yAxis->setMajorInc(ymajorinc);
	
	modified = true;
}

const char* XY::closestDataPoint(double x, double y)
{
	int closest = 0;
	string out  = "";
	double w    = trX(xAxis->max()) - trX(xAxis->min());
	double h    = trY(yAxis->max()) - trY(yAxis->min());
	double dx   = 500 / (w * w);
	double dy   = 500 / (h * h);
	double mind = 1;
	double cx   = 0;
	double cy   = 0;
	DataSeriesXY* data = 0;
	x = trX(x);
	y = trY(y);
	for (vector<ChartElement*>::iterator it=elements.begin();
			it<elements.end(); ++it)
	{
		if (((*it)->type() & nplot::elem_xy) == 0)
			continue;
		vector<double> px = ((DataSeriesXY*) (*it))->x();
		vector<double> py = ((DataSeriesXY*) (*it))->y();
		if (!px.empty()) {
			for (unsigned int i = 0; i < px.size(); ++i) {
				double tx = trX(px[i]) - x;
				double ty = trY(py[i]) - y;
				double d = tx * tx * dx + ty * ty * dy;
				if (d < mind) {
					mind = d;
					closest = i;
					out = (*it)->name.c_str();
					data = (DataSeriesXY*) *it;
					cx = px[i];
					cy = py[i];
				}
			}
		}
	}
	if (out == "") {
		if (marking) {
			marking = false;
			modified = true;
		}
		return NULL;
	} else {
		stringstream s;
		setMarker(cx, cy);
		s << out << '[' << closest + 1
		  << "]: X=" << xAxis->toString(cx)
		  << "; Y=" << yAxis->toString(cy);
		return s.str().c_str();
	}
}

double XY::trX(double x)
{
	return xAxis->trX(x);
}

double XY::atrX(double x)
{
	return xAxis->atrX(x);
}

double XY::trY(double y)
{
	return yAxis->trY(y);
}

double XY::atrY(double y)
{
	return yAxis->atrY(y);
}

double XY::wdwX(double x, double w)
{
	double maxx = xAxis->max();
	double minx = xAxis->min();
	return leftmgn + (w - leftmgn - rightmgn) * (trX(x) - trX(minx))
	       / (trX(maxx) - trX(minx));
}

double XY::wdwY(double y, double h)
{
	double maxy = yAxis->max();
	double miny = yAxis->min();
	return h - btmmgn - (h - topmgn - btmmgn) * (trY(y) - trY(miny))
	       / (trY(maxy) - trY(miny));
}

double XY::wdw2realX(int x)
{
	double maxx = xAxis->max();
	double minx = xAxis->min();
	double w = width() - scalefx * (leftmgn + rightmgn);
	return atrX((x - scalefx * leftmgn) * (trX(maxx) - trX(minx))
	            / w + trX(minx));
}

double XY::wdw2realY(int y)
{
	double maxy = yAxis->max();
	double miny = yAxis->min();
	double h = height() - scalefy * (topmgn + btmmgn);
	return atrY((h + scalefy * topmgn - y) * (trY(maxy) - trY(miny))
	            / h + trY(miny));
}

void XY::zoom(int x1, int x2, int y1, int y2, double scale)
{
	double maxx = xAxis->max();
	double minx = xAxis->min();
	double maxy = yAxis->max();
	double miny = yAxis->min();
	double w = width() - scalefx * (leftmgn + rightmgn);
	double h = height() - scalefy * (topmgn + btmmgn);

	double wx = atrX((x1 - scalefx * leftmgn) * (trX(maxx) - trX(minx))
	                 / w + trX(minx));
	double wy = atrY((h + scalefy * topmgn - y1) * (trY(maxy) - trY(miny))
	                 / h + trY(miny));
	double wx2 = atrX((x2 - scalefx * leftmgn) * (trX(maxx)
		           - trX(minx)) / w + trX(minx));
	double wy2 = atrY((h + scalefy * topmgn - y2) * (trY(maxy)
		           - trY(miny)) / h + trY(miny));
	double dx = scale * (trX(wx2) - trX(wx));
	double dy = scale * (trY(wy2) - trY(wy));
	
	xAxis->setMin(atrX(trX(minx) + dx));
	xAxis->setMax(atrX(trX(maxx) - dx));
	yAxis->setMin(atrY(trY(miny) + dy));
	yAxis->setMax(atrY(trY(maxy) - dy));

	modified = true;
}

void XY::scroll(int x1, int x2, int y1, int y2, double scale)
{
	double maxx = xAxis->max();
	double minx = xAxis->min();
	double maxy = yAxis->max();
	double miny = yAxis->min();
	double w = width() - scalefx * (leftmgn + rightmgn);
	double h = height() - scalefy * (topmgn + btmmgn);

	double wx = atrX((x1 - scalefx * leftmgn) * (trX(maxx) - trX(minx))
	                 / w + trX(minx));
	double wy = atrY((h + scalefy * topmgn - y1) * (trY(maxy) - trY(miny))
	                 / h + trY(miny));
	double wx2 = atrX((x2 - scalefx * leftmgn) * (trX(maxx)
		           - trX(minx)) / w + trX(minx));
	double wy2 = atrY((h + scalefy * topmgn - y2) * (trY(maxy)
		           - trY(miny)) / h + trY(miny));
	double dx = scale * (trX(wx2) - trX(wx));
	double dy = scale * (trY(wy2) - trY(wy));
	
	xAxis->setMin(atrX(trX(minx) - dx));
	xAxis->setMax(atrX(trX(maxx) - dx));
	yAxis->setMin(atrY(trY(miny) - dy));
	yAxis->setMax(atrY(trY(maxy) - dy));

	modified = true;
}

int XY::objectAt(int x, int y)
{
	if ((y >= height() - scalefy * btmmgn) && (x > scalefx * leftmgn))
		return 1;
	if ((x <= scalefx * leftmgn) && (y < height() - scalefy * btmmgn))
		return 2;
	return 0;
}

const Color& XY::nextColor()
{
	Color &c = Color::first();
	if (!elements.empty()) {
		ChartElement *top = elements.back();
		if ((top->type() & elem_xy) != 0)
			c = ((DataSeriesXY*) top)->color().next();
		else if ((top->type() & elem_func) != 0)
			c = ((FuncXY*) top)->color().next();
	}
	return c;
}

void XY::setMarker(double x, double y)
{
	markerX  = x;
	markerY  = y;
	marking  = true;
	modified = true;
}

void XY::drawPoint(double x, double y, point_shape shape, int size, int lblstype,
                   Export *exp)
{
	/* FIXME: Make this enum. Thats what it is for! */
	/*
	 *   shape = 0 - x
	 *   shape = 1 - +
	 *   shape = 2 - circle
	 *   shape = 3 - filled circle
	 *   shape = 4 - square
	 *   shape = 5 - filled square
	 *   shape = 6 - diamond
	 *   shape = 7 - filled diamond
	 */
	
	double maxx = xAxis->max();
	double minx = xAxis->min();
	double maxy = yAxis->max();
	double miny = yAxis->min();
	
	if (minx < maxx && (x < minx || x > maxx))
		return;
	if (miny < maxy && (y < miny || y > maxy))
		return;
	if (minx > maxx && (x > minx || x < maxx))
		return;
	if (miny > maxy && (y > miny || y < maxy))
		return;

	if (exp == 0)
		exp = nativeExport;

	double rx = leftmgn + (exp->width() - leftmgn - rightmgn)
	            * (trX(x) - trX(minx)) / (trX(maxx) - trX(minx));
	double ry = exp->height() - btmmgn - (exp->height() - topmgn - btmmgn)
			* (trY(y) - trY(miny)) / (trY(maxy) - trY(miny));
	exp->drawPoint(rx, ry, shape, size);
	exp->setFont(Export::times, 12);

	stringstream s;
	switch (lblstype) {
	case 0:
		s << x;
		break;
	case 1:
		s << y;
		break;
	case 2:
		s << x << ';' << y;
		break;
	}
	exp->drawText(rx, ry - size - 3, s.str().c_str());
}

void XY::drawMarker(double mx, double my)
{
	if (!marking)
		return;

	double x = wdwX(mx, nativeExport->width());
	double y = wdwY(my, nativeExport->height());
	double h = 8 / scalefx;
	double v = 8 / scalefy;
	nativeExport->setColor(1, 0, 0);
	nativeExport->moveto(x - h, y + v);
	nativeExport->lineto(x + h, y + v);
	nativeExport->lineto(x + h, y - v);
	nativeExport->lineto(x - h, y - v);
	nativeExport->lineto(x - h, y + v);
	nativeExport->stroke(1);
	nativeExport->moveto(x, y + v + v);
	nativeExport->lineto(x, y - v - v);
	nativeExport->stroke(1);
	nativeExport->moveto(x - h - h, y);
	nativeExport->lineto(x + h + h, y);
	nativeExport->stroke(1);
}

void XY::doExport(Export *exp)
{
	calcMargins();
	
	double maxx = xAxis->max();
	double minx = xAxis->min();
	double maxy = yAxis->max();
	double miny = yAxis->min();
	double w    = exp->width();
	double h    = exp->height();
	exp->setClipRect(wdwX(minx, w), wdwY(miny, h), wdwX(maxx, w),
	                 wdwY(maxy, h));
	
	xAxis->drawGrid(exp);
	yAxis->drawGrid(exp);

	for (vector<ChartElement*>::reverse_iterator it = elements.rbegin();
	     it != elements.rend(); ++it)
	{
		(*it)->repaint(exp);
	}

	exp->setClipRect(0, 0, w, h);
	xAxis->repaint(exp);
	yAxis->repaint(exp);

	exp->setColor(0, 0, 0);
	exp->drawLine(leftmgn, topmgn, w - rightmgn, topmgn);
	exp->drawLine(w - rightmgn, topmgn, w - rightmgn, h - btmmgn);

	legend->repaint(exp);
}

void XY::repaint()
{
	doExport(nativeExport);
	drawMarker(markerX, markerY);
	modified = false;
}

void XY::fit(nplot::fit_type type)
{
	if (elements.size() == 0)
		return;

	bool ok = false;
	for (vector<ChartElement*>::iterator it = elements.begin(); 
	     it != elements.end(); ++it)
	{
		if (((*it)->type() & nplot::elem_xy) == 0)
			continue;
		ok = true;
		break;
	}
	if (!ok)
		return;

	int from, to, ds, degree;
	RangeDialog rd(&elements, &ds, &from, &to,
	               (type == nplot::fit_poly)? &degree : NULL);
	if (!rd.exec())
		return;

	int n = to - from;
	vector<double> x;
	vector<double> y;
	for (vector<ChartElement*>::iterator it = elements.begin();
	     it != elements.end(); ++it)
	{
		if (((*it)->type() & nplot::elem_xy) == 0)
			continue;
		if (ds-- != 0)
			continue;

		x = ((DataSeriesXY*) (*it))->x();
		y = ((DataSeriesXY*) (*it))->y();
		for (int i = 0, j = from; j < to; ++i, ++j) {
			x[i] = x[j];
			y[i] = y[j];
		}
		x.resize(n);
		y.resize(n);
	}

	string info;
	vector<double> f;
	FuncXY *func = NULL;
	nplot::fit_err err;
	switch (type) {
	case nplot::fit_linear:
		err = Analysis::linearFitting(x, y, &f, &info);
		if (err == nplot::ferr_ok)
			func = new FuncXY("aX+b", f, 1, this);
		break;
	case nplot::fit_quad:
		err = Analysis::quadFitting(x, y, &f, &info);
		if (err == nplot::ferr_ok)
			func = new FuncXY("aX^2+bX+c", f, 100, this);
		break;
	case nplot::fit_cubic:
		err = Analysis::cubicFitting(x, y, &f, &info);
		if (err == nplot::ferr_ok)
			func = new FuncXY("aX^3+bX^2+cX+d", f, 100, this);
		break;
	case nplot::fit_quart:
		err = Analysis::quartFitting(x, y, &f, &info);
		if (err == nplot::ferr_ok)
			func = new FuncXY("aX^4+bX^3+cX^2+dX+e", f, 100, this);
		break;
	case nplot::fit_poly:
		err = Analysis::polyFitting(x, y, degree, &f, &info);
		if (err == nplot::ferr_ok)
			func = new FuncXY("polynomial", f, 100, this);
		break;
	case nplot::fit_exp:
		err = Analysis::expFitting(x, y, &f, &info);
		if (err == nplot::ferr_ok)
			func = new FuncXY("ab^x", f, 100, this);
		break;
	case nplot::fit_log:
		err = Analysis::logFitting(x, y, &f, &info);
		if (err == nplot::ferr_ok)
			func = new FuncXY("a+blnx", f, 100, this);
		break;
	case nplot::fit_power:
		err = Analysis::powerFitting(x, y, &f, &info);
		if (err == nplot::ferr_ok)
			func = new FuncXY("ax^b", f, 100, this);
		break;
	case nplot::fit_gauss:
		err = Analysis::gaussFitting(x, y, &f, &info);
		if (err == nplot::ferr_ok)
			func = new FuncXY("aexp(b(x-c)^2)", f, 100, this);
		break;
	default:
		cerr << "Not implemented yet" << endl;
		return;
	}

	if (err != nplot::ferr_ok) {
		Dialogs::errorMessage(err);
		return;
	}

	func->setColor(nextColor());
	Notepad *np = new Notepad(info.c_str(), MainWindow::pointer);
	MainWindow::pointer->addWindow(np);
	np->show();
	elements.push_back(func);
	modified = true;
}

void XY::findPeaks()
{
	if (elements.size() == 0)
		return;

	bool ok = false;
	for (vector<ChartElement*>::iterator it = elements.begin();
	     it != elements.end(); ++it)
	{
		if (((*it)->type() & nplot::elem_xy) == 0)
			continue;
		ok = true;
		break;
	}
	if (!ok)
		return;

	string name;
	int ds, type;
	double threshold;
	if (!Dialogs::findPeaks(elements, &name, &ds, &type, &threshold))
		return;

	vector<double> x;
	vector<double> y;
	for (vector<ChartElement*>::iterator it = elements.begin();
			it != elements.end(); ++it)
	{
		if (((*it)->type() & nplot::elem_xy) == 0)
			continue;
		if (ds-- != 0)
			continue;
		x = ((DataSeriesXY*) (*it))->x();
		y = ((DataSeriesXY*) (*it))->y();
	}

	vector< vector<double> > peaks;

	Analysis::findPeaks(x, y, threshold, type, &peaks);
	
	if (!peaks.empty()) {
		Array array(2, peaks[0].size());
		array.setColumnType(0, type_double);
		array.setColumnType(1, type_double);
		for (int i = 0; i < array.cols(); ++i) {
			for (int j = 1; j < array.rows(); ++j)
				array[i][j] = peaks[i][j];
		}
		DataSeriesXY *element = new DataSeriesXY(name.c_str(), array,
		                                         nplot::elem_xy, this);
		element->setColor(nextColor());
		element->setSymbolShape(shp_x);
		element->setSymbolSize(4);
		elements.push_back(element);
		modified = true;
	}
}

void XY::setHeight(int v)
{
	if (vheight == v)
		return;
	
	vheight  = v;
	modified = true;
	scalefy  = (double) v / nativeExport->height();
}

void XY::setWidth(int v)
{
	if (vwidth == v)
		return;
	
	vwidth   = v;
	modified = true;
	scalefx  = (double) v / nativeExport->width();
}

