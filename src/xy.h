/*
 *   nPlot, a minimalistic data analysis application
 *   Copyright (C) 2006-2009, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of nPlot.
 *
 *   nPlot is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   nPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with nPlot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef XY_H
#define XY_H

#include <vector>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include "array.h"
#include "axes.h"
#include "chart.h"
#include "chartelement.h"
#include "export.h"
#include "legend.h"
#include "nplotnamespace.h"
#include "types.h"

class ChartElement;
class Legend;
class XAxis2D;
class YAxis2D;

/**
 *   XY plotting object
 */
class XY
{
public:
	/**
	 *   A constructor.
	 *   \param width initial width
	 *   \param height initial height
	 *   \param exp Export object to use for drawing
	 */
	XY(int width, int height, Export *exp);

	/**  A destructor */
	~XY();

	/**
	 *   Load state of the widget.
	 *   Method used to load the information from a project file.
	 *   \param reader session reader
	 */
	void load(SessionReader *reader);

	/**
	 *   Save state of a widget.
	 *   Method used for saving information to a project file.
	 *   \param writer session writer
	 */
	void save(SessionWriter *writer);

	/**
	 *   Add XY data to the widget.
	 *   \param name name of the dataset to add
	 *   \param data Array of data
	 *   \param type data type
	 *   \param symbols if \c true, turns symbols on; turns them off 
	 *   otherwise
	 *   \param line if \c true, turns continous plotting on; turns it off
	 *   otherwise
	 */
	void addDataXY(string name, Array data, int type, bool symbols,
	               bool line);

	/**
	 *   Recalculate margins of a plot.
	 *   \param exp export used (if not specified or \c NULL, uses 
	 *   Export object given in constructor)
	 */
	void calcMargins(Export *exp = 0);
	
	/**
	 *   Redraw the contents on a given export.
	 *   \param *exp export to draw on
	 */
	void doExport(Export *exp);

	/**
	 *   Used to redraw contents on a widget.
	 *   Redraws also markers, etc.
	 */
	void repaint();
	void findPeaks();            /**< Find peaks */

	/**
	 *   Fits a given curve to the data
	 *   \param type type of fitting
	 */
	void fit(nplot::fit_type type);

	void fitZoom();              /**< Zoom to fit all the data sets */

	/**  Returns height of the canvas. */
	int height() const          { return vheight; }

	/**  Returns \c true if something was modified, \c false otherwise. */
	bool isModified() const     { return modified; }

	/**
	 *   Get a name of the widget.
	 *   \return name of the widget
	 */
	const char* name() const    { return vname.c_str(); }

	/**
	 *   Get number of chart elements.
	 *   \return number of elements
	 */
	int numElements() const     { return elements.size(); }

	/**  Returns width of the canvas. */
	int width() const           { return vwidth; }

	/**
	 *   Sets height of the canvas.
	 *   \param v value of height to set
	 */
	void setHeight(int v);

	/**
	 *   Set a name of the widget.
	 *   \param name new name
	 */
	void setName(string name)   { vname = name; }

	/**  Sets modified flag. */
	void setModified()          { modified = true; }

	/**
	 *   Sets width of the canvas.
	 *   \param v value of width to set
	 */
	void setWidth(int v);

	/**
	 *   Sets the coordinates of the marker on a plot.
	 *   \param x,y coordinates to be set
	 */
	void setMarker(double x, double y);

	/**
	 *   Get closest data point.
	 *   Method used in DataReader.
	 *   \param x,y coordinates
	 *   \return string containing relevant information to print 
	 *   in DataReader
	 */
	const char* closestDataPoint(double x, double y);

	/* FIXME: we probably could use exports insteed of this;
	 * DataSeriesXY still uses this method */
	/**
	 *   Draws a point.
	 *   \param x,y coordinates
	 *   \param shape shape
	 *   \param size size
	 *   \param lbltype label type
	 *   \param exp export used
	 */
	void drawPoint(double x, double y, nplot::point_shape shape, 
	               int size, int lbltype,  Export *exp = 0);

	/**
	 *   Transform X according to X scale
	 *   \param x value to be transformed
	 *   \return transformed X
	 */
	double trX(double x);

	/**
	 *   Anti-transform X according to X scale
	 *   Do inverse transformation of X.
	 *   \param x value to be transformed
	 *   \return transformed X
	 */
	double atrX(double x);

	/**
	 *   Transform Y according to Y scale
	 *   \param y value to be transformed
	 *   \return transformed Y
	 */
	double trY(double y);
	
	/**
	 *   Anti-transform Y according to Y scale
	 *   Do inverse transformation of Y.
	 *   \param y value to be transformed
	 *   \return transformed Y
	 */
	double atrY(double y);
	
	/**
	 *   Transforms X from the visualization window coordinates to pixel
	 *   coordinates.
	 *   \param x X to transform
	 *   \param w width of the visualization window in pixels
	 *   \return transformed X
	 */
	double wdwX(double x, double w);

	/**
	 *   Transforms Y from the visualization window coordinates to pixel
	 *   coordinates.
	 *   \param y Y to transform
	 *   \param h height of the visualization window in pixels
	 *   \return transformed Y
	 */
	double wdwY(double y, double h);

	/**
	 *   Converts pixel coordinate X to real X.
	 *   \param x pixel coordinate X
	 *   \return converted X value 
	 */
	double wdw2realX(int x);

	/**
	 *   Converts pixel coordinate Y to real Y.
	 *   \param x pixel coordinate Y
	 *   \return converted Y value 
	 */
	double wdw2realY(int y);

	/**
	 *   Zoom with vector.
	 *   \param x1 X coord of the origin of vector
	 *   \param x2 X coord of the end of the vector
	 *   \param y1 Y coord of the origin of the vector
	 *   \param y2 Y coord of the end of the vector
	 *   \param scale scale of zoom
	 */
	void zoom(int x1, int x2, int y1, int y2, double scale = 1.);

	/**
	 *   Scroll with vector.
	 *   \param x1 X coord of the origin of vector
	 *   \param x2 X coord of the end of the vector
	 *   \param y1 Y coord of the origin of the vector
	 *   \param y2 Y coord of the end of the vector
	 *   \param scale scale of scroll
	 */
	void scroll(int x1, int x2, int y1, int y2, double scale = 1.);
	
	/* FIXME: use some sort of enum for type of object */
	/**
	 *   Returns "type" of the object in position \c x, \c y.
	 *   \param x X coordinate
	 *   \param y Y coordinate
	 *   \return "type" of the object
	 */
	int objectAt(int x, int y);
private:

	/**
	 *   Draw a marker with given export.
	 *   Procedure draws only with the native export, hence - on a widget.
	 *   \param x,y coordinates of the marker
	 */
	void drawMarker(double x, double y);

	const Color& nextColor();
public:	
	vector<ChartElement*> elements; /**< Chart elements */
	int      leftmgn;               /**< Left margin */
	int      rightmgn;              /**< Right margin */
	int      topmgn;                /**< Top margin */
	int      btmmgn;                /**< Bottom margin */
	double   scalefx;               /**< X scale factor */
	double   scalefy;               /**< Y scale factor */
	Legend  *legend;                /**< Legend */
	XAxis2D *xAxis;                 /**< X axis */
	YAxis2D *yAxis;                 /**< Y axis */
private:
	int      vwidth;
	int      vheight;
	double   markerX;
	double   markerY;
	bool     marking;
	bool     modified;
	string   vname;
	Export  *nativeExport;
};

#endif /* XY_H */
